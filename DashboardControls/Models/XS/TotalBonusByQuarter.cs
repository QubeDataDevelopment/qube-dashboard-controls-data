﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Models.XS
{
    public class TotalBonusByQuarter : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>{
            { "Q1", new Objects.XS.TotalBonusByQuarter() },
            { "Q2", new Objects.XS.TotalBonusByQuarter() },
            { "Q3", new Objects.XS.TotalBonusByQuarter() },
            { "Q4", new Objects.XS.TotalBonusByQuarter() },
            { "YTD", new Objects.XS.TotalBonusByQuarter() }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>
        {
            { "Q1", 1 },
            { "Q2", 2 },
            { "Q3", 3 },
            { "Q4", 4 },
            { "YTD", 5 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>> {
            { "English", new List<string> { "Apr-Jun", "Jul-Sep", "Oct-Dec", "Jan-Mar", "YTD" } },
            { "German", new List<string> { "Apr-Jun", "Jul-Sep", "Okt-Dez", "Jan-Mär", "YTD" } },
            { "French", new List<string> { "Avr-Juin", "Juil-Sept", "Oct-Déc", "Jan-Mars", "YTD" } }
        };

        public TotalBonusByQuarter(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            string sql = @"
                SELECT
	                d.id dealer_id,
	                coalesce(d.dealergroup_id, 0) dealergroup_id,
	                coalesce(quarter_id, 0) quarter_id,
	                coalesce(pb_bonus, 0) pb_bonus,
	                coalesce(fg_bonus, 0) fg_bonus,
	                coalesce(av_bonus, 0) av_bonus
                from v_LiveDealers d
                left join (
	                select
		                fg.dealergroup_id,
		                fg.year_id,
		                fg.quarter_id,
		                pb.payment_real pb_bonus,
		                fg.fg_bonus,
		                av.bonus_today av_bonus
	                from FG_PerformanceSummary fg
	                left join (
		                SELECT qv.*
		                FROM QV_Performance qv
		                INNER JOIN dbo.PB_Bonuscontrol bc ON bc.calendaryear = qv.year_id AND bc.quarter_id = qv.quarter_id
		                LEFT JOIN dbo.PB_Targets pbt ON pbt.control_id = bc.id  AND pbt.dealergroup_id = qv.dealergroup_id 
		                WHERE qv.year_id >= 2020 AND pbt.dealergroup_id IS NOT null
				                --((qv.year_id < 2019 AND qv.quarter_id IN (1,2,3,4))
				                --OR
				                --(qv.year_id = 2019 AND ((qv.quarter_id = 4 AND pbt.dealergroup_id IS NOT NULL) OR qv.quarter_id IN (1,2,3)))
				                --OR
				                --())
	                ) av on av.dealergroup_id = fg.dealergroup_id and av.year_id = fg.year_id and av.quarter_id = fg.quarter_id
	                left join (
		                select
			                pb.dealergroup_id,
			                pb.payment_real,
			                dp.FinancialQuarter,
			                CAST(LEFT(dp.FinancialYear,4) AS INT)  AS FinancialYear
		                from PB_Performance_DealerGroup pb 
		                inner join dataperiods dp on dp.id = pb.period_from
	                ) pb on pb.dealergroup_id = fg.dealergroup_id and pb.FinancialQuarter = fg.quarter_id and pb.FinancialYear = fg.year_id 
                    WHERE fg.dealergroup_id IN (SELECT dealergroup_id FROM dbo.PB_Targets WHERE targetvalue > 0 AND control_id = fg.PB_bonuscontrol_id)
                ) q on q.dealergroup_id = d.dealergroup_id
                where year_id = @Year";

            return MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                new SqlParameter("@Year", SqlDbType.Int) { Value = int.Parse(Period.FinancialYear.Substring(0,4)) }
            }).AsEnumerable()
            .Where(w => w.Field<int>("quarter_id") != 0)
            .GroupBy(g => g.Field<int>("quarter_id"))
            .ToDictionary(
                x => "Q" + x.Key,
                x => x.GroupBy(g => new { dealer_id = g.Field<int>("dealer_id"), dealergroup_id = g.Field<int>("dealergroup_id") })
                    .ToDictionary(x2 => x2.Key.dealer_id, x2 => new JavaScriptSerializer().Serialize(
                        new Objects.XS.TotalBonusByQuarter
                        {
                            Name = "Q" + x.Key,
                            dealergroup_id = x2.Key.dealergroup_id,
                            PB_Bonus = x2.Sum(s => s.Field<decimal>("pb_bonus")),
                            FG_Bonus = x2.Sum(s => s.Field<decimal>("fg_bonus")),
                            AV_Bonus = x2.Sum(s => s.Field<decimal>("av_bonus"))
                        })
                    )
            );
        }
    }
}
