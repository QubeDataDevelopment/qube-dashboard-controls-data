﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public interface IDashboardControl<T>
    {
        Dictionary<string, T> Values { get; }
        Dictionary<string, T> ValuesLY { get; }
        Dictionary<string, int> Quartiles { get; }
        Dictionary<string, int> Weights { get; }
        Dictionary<string, T> ZoneValues { get; }
        Dictionary<string, T> NatValues { get; }
        Dictionary<string, List<string>> Text { get; }
        Dictionary<string, T> Template { get; }
    }
}
