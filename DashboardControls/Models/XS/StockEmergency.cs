﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.XS
{
    public class StockEmergency : DashboardModel<decimal>
    {
        public override Dictionary<string, decimal> Template => new Dictionary<string, decimal>() { { "Score", 0m } };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() { { "Score", 1 } };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>> { };

        public StockEmergency(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string SOrders = "select\n"
               + "	d.id as dealer_id,\n"
               + "	coalesce(S_Orders, 0) S_Orders\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	SELECT\n"
               + "		dealer_id,\n"
               + "		SUM(Quantity) as S_Orders\n"
               + "	FROM Web_ValidDealerPurchases\n"
               + "	WHERE DataPeriod_Id between @From and @To\n"
               + "		and Level4_Code in (" + GetL4Codes() + ")\n"
               + "		and Stock_Emergency_Code = 'S'\n"
               + "		and Gen_Loc_Code = 'G'\n"
               + "	group by Dealer_Id\n"
               + ") se on se.Dealer_Id = d.id";

            string Orders = "select\n"
               + "	d.id as dealer_id,\n"
               + "	coalesce(Orders, 0) Orders\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	SELECT\n"
               + "		dealer_id,\n"
               + "		SUM(Quantity) as Orders\n"
               + "	FROM Web_ValidDealerPurchases\n"
               + "	WHERE DataPeriod_Id between @From and @To\n"
               + "		and Level4_Code in (" + GetL4Codes() + ")\n"
               + "		and Gen_Loc_Code = 'G'\n"
               + "	group by Dealer_Id\n"
               + ") se on se.Dealer_Id = d.id";

            res.Add("S_Orders", MSSQLHelper.GetDictionary<int,decimal>(SOrders,
                new SqlParameter[]
                {
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                }
            ).ToDictionary(x => x.Key, x => x.Value.ToString()));

            res.Add("Orders", MSSQLHelper.GetDictionary<int,decimal>(Orders,
                new SqlParameter[]
                {
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                }
            ).ToDictionary(x => x.Key, x => x.Value.ToString()));

            return res;
        }

        private string GetL4Codes()
        {
            //Store this in the DB somewhere??
            switch (Measure.Enum)
            {
                case DashboardMeasure.StockEmergency_RoutineMaintenance:
                    return "'ROM'";
                case DashboardMeasure.StockEmergency_ExtendedMaintenance:
                    return "'EXM'";
                case DashboardMeasure.StockEmergency_MechanicalRepair:
                    return "'MEC'";
                case DashboardMeasure.StockEmergency_Damage:
                    return "'DAM'";
                case DashboardMeasure.StockEmergency_Accessories:
                    return "'ACC'";
                case DashboardMeasure.StockEmergency_Total:
                    return "'ACC','DAM','EXM','MEC','ROM'";
                default:
                    throw new ArgumentOutOfRangeException("Invalid measure");
            }
        }
    }
}
