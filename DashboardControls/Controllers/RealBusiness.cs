﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Controllers
{
    public class RealBusiness : Controller
    {
        public RealBusiness(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override object Execute(bool lastYear = false)
        {
            Dictionary<string, decimal> res = new Dictionary<string, decimal>();

            if (!Data.ContainsKey("YTD")) return res;

            decimal YTD = Data["YTD"].Sum(s => decimal.Parse(s.Value));
            decimal PYTD = Data["PYTD"].Sum(s => decimal.Parse(s.Value));
            decimal Target = Data["Target"].Sum(s => decimal.Parse(s.Value));

            res.Add("YTD" + (lastYear ? "LY" : ""), YTD);
            res.Add("Change" + (lastYear ? "LY" : ""), (PYTD > 0 ? (YTD - PYTD) / PYTD : 0m));
            res.Add("Objective" + (lastYear ? "LY" : ""), (Target > 0 ? YTD / Target : 0m));

            return res;
        }

        public override object ExecuteDealer(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, decimal>> res = new Dictionary<string, Dictionary<int, decimal>>();

            res.Add("YTD" + (lastYear ? "LY" : ""), Data["YTD"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)));
            res.Add("PYTD" + (lastYear ? "LY" : ""), Data["PYTD"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)));
            res.Add("Target" + (lastYear ? "LY" : ""), Data["Target"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)));
            res.Add("Change" + (lastYear ? "LY" : ""), Data["YTD"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value))
                .Join(Data["PYTD"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)), j1 => j1.Key, j2 => j2.Key, (j1, j2) => new { dealer_id = j1.Key, YTD = j1.Value, PYTD = j2.Value })
                .ToDictionary(x => x.dealer_id, x => (x.PYTD > 0 ? (x.YTD - x.PYTD) / x.PYTD : 0m))
            );
            res.Add("Objective" + (lastYear ? "LY" : ""), Data["YTD"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value))
                .Join(Data["Target"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)), j1 => j1.Key, j2 => j2.Key, (j1, j2) => new { dealer_id = j1.Key, YTD = j1.Value, Target = j2.Value })
                .ToDictionary(x => x.dealer_id, x => (x.Target > 0 ? x.YTD / x.Target : 0m))
            );

            return res;
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
