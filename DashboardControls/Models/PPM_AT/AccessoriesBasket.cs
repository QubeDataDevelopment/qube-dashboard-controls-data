﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.PPM_AT
{
    public class AccessoriesBasket : DashboardModel<decimal>
    {
        public override Dictionary<string, decimal> Template => new Dictionary<string, decimal>() {
            { "Performance", 0m },
            { "Value", 0m },
            { "YTD", 0m }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>()
        {
            { "Performance", 1 },
            { "Value", 2 },
            { "YTD", 3 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>();

        public AccessoriesBasket(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();
            DateTime fYStartDate = MSSQLHelper.ExecuteScalar<DateTime>("SELECT StartDate from DataPeriods where id=@FYStartPeriod", new SqlParameter[] { new SqlParameter("@FYStartPeriod", SqlDbType.Int) { Value = Period.FYearStartPeriod } });
            DateTime qtrStartDate = MSSQLHelper.ExecuteScalar<DateTime>("SELECT StartDate from DataPeriods where id=@QtrStartPeriod",new SqlParameter[] {new SqlParameter("@QtrStartPeriod", SqlDbType.Int) { Value = Period.Id - ((Period.FinancialMonth - 1) % 3) }});
            DateTime qtrEndDate = MSSQLHelper.ExecuteScalar<DateTime>("SELECT EndDate from DataPeriods where id=@QtrEndPeriod", new SqlParameter[] { new SqlParameter("@QtrEndPeriod", SqlDbType.Int) { Value = Period.Id } });
            DateTime maxDate = MSSQLHelper.ExecuteScalar<DateTime>("SELECT dbo.MaxPurchaseDate()", null);
            DateTime ncsMaxDate = MSSQLHelper.ExecuteScalar<DateTime>("SELECT isnull(MAX(RetailDate), '1900-01-01') FROM VehicleSales_Summary WHERE RetailDate BETWEEN @StartDate AND @EndDate", new SqlParameter[] {
                new SqlParameter("@StartDate", SqlDbType.DateTime) { Value = qtrStartDate },
                new SqlParameter("@EndDate", SqlDbType.DateTime) { Value = qtrEndDate }
            });

            if (ncsMaxDate < qtrEndDate)
            {
                qtrEndDate = ncsMaxDate;
                maxDate = ncsMaxDate;
            }

            string sql =
                @"SELECT
	                d.id dealer_id,
	                d.DealerGroup_Id,
	                coalesce(ThisYear, 0) ThisYear,
	                coalesce(LastYear, 0) LastYear,
	                coalesce(ThisQuarter, 0) ThisQuarter,
	                ISNULL(ty.VehicleSales, 0) ThisYearNCS,
	                (CASE WHEN ty.VehicleSales > 0 THEN dbo.DivideTwoNumbers_PC(ThisQuarter,ty.VehicleSales) ELSE 0 END) Ratio
                from v_LiveDealers d
                left join (
	                SELECT 
		                a.DealerGroup_Id,
		                SUM(CASE WHEN a.Purchase_Date BETWEEN @FYStartDate AND @MaxDate THEN a.DealerCost ELSE 0 END) AS ThisYear, 
		                SUM(CASE WHEN a.Purchase_Date BETWEEN DATEADD(YEAR, -1, @FYStartDate) AND DATEADD(YEAR, -1, @MaxDate)  THEN a.DealerCost ELSE 0 END) AS LastYear,
		                SUM(CASE WHEN a.Purchase_Date BETWEEN @QtrStartDate and @MaxDate then a.DealerCost ELSE 0 END) as ThisQuarter
	                FROM Web_ValidDealerPurchases a
	                INNER JOIN BonusBasketItems b ON a.Level2_Code = b.ProductGroup AND b.Control_Id = (select top 1 id from bonusbasketcontrol where @EndPeriod between period_from and period_to and basketname = 'Accessories NCS Basket')
	                inner join dbo.CurrentDealerGroupSelectionTableForBonus('M', '6', 6) dg on dg.DealerGroup_ID = a.DealerGroup_Id
	                WHERE a.Purchase_Date BETWEEN DATEADD(YEAR, -1, @FYStartDate) AND @MaxDate
	                GROUP BY a.DealerGroup_Id
                ) q on q.DealerGroup_Id = d.DealerGroup_Id 
                LEFT JOIN
                (
	                SELECT dg.id DealerGroupId, ISNULL(SUM(VehicleSales),0) VehicleSales FROM v_LiveDealers d 
	                INNER JOIN dbo.DealerGroups dg ON dg.id=d.dealergroup_id 
	                LEFT JOIN 
	                (
		                SELECT vs.dealer_id, vs.retaildate RetailDate, ISNULL(vehicles_sold, 0) AS VehicleSales
		                FROM VehicleSales_Summary vs
		                WHERE 
			                vs.Dealer_Id IN (SELECT dealer_id FROM CurrentDealerSelectionTable('M', '6')) AND 
			                vs.RetailDate BETWEEN @QtrStartDate AND @MaxDate AND
			                (vs.OrderClassCode IN (SELECT ClassCode FROM VehicleSalesClassifications WHERE Channel1Translate_Id IN (SELECT DISTINCT Channel1Translate_Id FROM VehicleSalesClassifications)) or 1=1)
	                ) a ON a.dealer_id = d.id 
	                GROUP BY dg.id  
                ) ty ON ty.DealerGroupId = d.dealergroup_id ";

            string maxDateSql =
                @"select
	                d.id dealer_id,
	                coalesce(MaxDate, '19000101') MaxDate
                from v_LiveDealers d
                left join (
	                select
		                dealergroup_id,
		                convert(decimal, format(ISNULL(MAX(purchase_date),'1900-01-01'), 'yyyyMMdd')) MaxDate
	                from Web_ValidDealerPurchases
	                where DataPeriod_Id <= @PeriodId
	                group by DealerGroup_Id
                ) a on a.DealerGroup_Id = d.dealergroup_id";

            EnumerableRowCollection<DataRow> data = MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                new SqlParameter("@FYStartDate", SqlDbType.DateTime) { Value = fYStartDate },
                new SqlParameter("@MaxDate", SqlDbType.DateTime) { Value = maxDate },
                new SqlParameter("@QtrStartDate", SqlDbType.DateTime) { Value = qtrStartDate },
                new SqlParameter("@EndPeriod", SqlDbType.Int) { Value = Period.Id }
            }).AsEnumerable();

            EnumerableRowCollection<DataRow> maxDateData = MSSQLHelper.GetDataTable(maxDateSql, new SqlParameter[] {
                new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
            }).AsEnumerable();

            res.Add("dealergroup", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<int>("DealerGroup_Id").ToString()));
            res.Add("ThisYear", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("ThisYear").ToString()));
            res.Add("LastYear", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("LastYear").ToString()));
            res.Add("ThisQuarter", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("ThisQuarter").ToString()));
            res.Add("MaxDate", maxDateData.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("MaxDate").ToString()));
            res.Add("ThisYearNCS", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<int>("ThisYearNCS").ToString()));
            res.Add("Ratio", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<double>("Ratio").ToString()));

            return res;
        }
    }
}
