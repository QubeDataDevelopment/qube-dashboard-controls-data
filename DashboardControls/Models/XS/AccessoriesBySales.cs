﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Models.XS
{
    public class AccessoriesBySales : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>() {
            { "YTD", new Objects.XS.AccessoriesBySales() },
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>()
        {
            { "YTD", 1 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>() {
            { "English", new List<string>{ "YTD FY##YYCY##", "YTD FY##YYLY##", "Zone", "National" } },
            { "German", new List<string>{ "YTD FY##YYCY##", "YTD FY##YYLY##", "Gebiet", "BUND" } },
            { "French", new List<string>{ "YTD FY##YYCY##", "YTD FY##YYLY##", "Zone", "National" } },
        };

        public AccessoriesBySales(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            if (lastYear)
                Period = new DataPeriod(Period.Id - 12);

            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string accSql = "select\n"
               + "	d.id dealer_id,\n"
               + "	coalesce(DealerCost, 0) DealerCost\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id,\n"
               + "		sum(DealerCost) DealerCost\n"
               + "	from Web_ValidDealerPurchases\n"
               + "	where DataPeriod_Id between @From and @To\n"
               + "		and Level4_Code = 'ACC'\n"
               + "	group by dealer_id\n"
               + ") r on r.dealer_id = d.id";

            string vehSql = "select\n"
               + "	d.id dealer_id,\n"
               + "	coalesce(vehicles_sold, 0) vehicles_sold\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id,\n"
               + "		sum(vehicles_sold) vehicles_sold\n"
               + "	from VehicleSales_Summary\n"
               + "	where dataperiod_id between @From and @To\n"
               + "	group by dealer_id\n"
               + ") r on r.dealer_id = d.id";

            Dictionary<int, Objects.XS.AccessoriesBySales> resO;

            resO = MSSQLHelper.GetDictionary<int, decimal>(accSql,
                    new SqlParameter[]
                    {
                        new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                        new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                    }
                ).AsEnumerable().Join(
                    MSSQLHelper.GetDictionary<int, int>(vehSql,
                        new SqlParameter[]
                        {
                                new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                                new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id }
                        }
                    ).AsEnumerable(),
                    t1 => t1.Key,
                    t2 => t2.Key,
                    (t1, t2) => new { t1, t2 }
                ).GroupBy(g => g.t1.Key)
                .Select(s => new
                {
                    dealer_id = s.Key,
                    values = new Objects.XS.AccessoriesBySales()
                    {
                        Acc = s.Sum(su => su.t1.Value),
                        Veh = s.Sum(su => su.t2.Value)
                    }
                }).ToDictionary(x => x.dealer_id, x => x.values);

            res.Add("Values" + (lastYear ? "LY" : ""), resO.ToDictionary(x => x.Key, x => new JavaScriptSerializer().Serialize(x.Value)));

            return res;
        }
    }
}
