﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Models.XS
{
    public class Turnover_PPUYear : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>() {
            { "YTD", new Objects.XS.Turnover_PPUYear() }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>()
        {
            { "YTD", 1 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>() {
            { "English", new List<string>{ "YTD FY##YYCY##", "YTD FY##YYLY##", "Zone", "National" } },
            { "German", new List<string>{ "YTD FY##YYCY##", "YTD FY##YYLY##", "Gebiet", "BUND" } },
            { "French", new List<string>{ "YTD FY##YYCY##", "YTD FY##YYLY##", "Zone", "National" } }
        };

        public Turnover_PPUYear(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            if (lastYear)
                Period = new DataPeriod(Period.Id - 12);

            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string TOSql = "SELECT \n"
               + "	d.Id dealer_id,\n"
               + "	coalesce(total_this, 0) total_this\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id, SUM(real_value) as total_this\n"
               + "	from  RealBusiness  \n"
               + "	where dataperiod_id between @From and @To\n"
               + "		and level4_code in ('ROM','EXM','MEC','DAM')\n"
               + "	group by dealer_id\n"
               + ") tuo on tuo.dealer_id = d.id";

            string ParcSql = "select\n"
               + "	d.id dealer_id,\n"
               + "	coalesce(parc, 0) parc\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id, dbo.dividetwonumbers(sum(parc_value),12) * (1+(@To-@From)) as parc\n"
               + "	from v_CarParcTenYear t1\n"
               + "	where finyear = @FinYear\n"
               + "		and parc_year between 1 and 10\n"
               + "	group by dealer_id\n"
               + ") parc on parc.dealer_id = d.id";

            Dictionary<int, Objects.XS.Turnover_PPUYear> resO;

            resO = MSSQLHelper.GetDictionary<int, decimal>(TOSql,
                    new SqlParameter[]
                    {
                        new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                        new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                    }
                ).AsEnumerable().Join(
                    MSSQLHelper.GetDictionary<int, decimal>(ParcSql,
                        new SqlParameter[]
                        {
                                new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                                new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                                new SqlParameter("@FinYear", SqlDbType.VarChar) { Value = Period.FinancialYear },
                        }
                    ).AsEnumerable(),
                    t1 => t1.Key,
                    t2 => t2.Key,
                    (t1, t2) => new { t1, t2 }
                ).GroupBy(g => g.t1.Key)
                .Select(s => new
                {
                    dealer_id = s.Key,
                    values = new Objects.XS.Turnover_PPUYear()
                    {
                        TO = s.Sum(su => su.t1.Value),
                        Parc = s.Sum(su => su.t2.Value)
                    }
                }).ToDictionary(x => x.dealer_id, x => x.values);

            res.Add("Values" + (lastYear ? "LY" : ""), resO.ToDictionary(x => x.Key, x => new JavaScriptSerializer().Serialize(x.Value)));

            return res;
        }
    }
}
