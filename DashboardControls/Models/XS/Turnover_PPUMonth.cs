﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.XS
{
    public class Turnover_PPUMonth : DashboardModel<decimal?>
    {
        public override Dictionary<string, decimal?> Template => new Dictionary<string, decimal?>() { { "APR", null }, { "MAY", null }, { "JUN", null }, { "JUL", null }, { "AUG", null }, { "SEP", null }, { "OCT", null }, { "NOV", null }, { "DEC", null }, { "JAN", null }, { "FEB", null }, { "MAR", null } };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() { { "APR", 1 }, { "MAY", 2 }, { "JUN", 3 }, { "JUL", 4 }, { "AUG", 5 }, { "SEP", 6 }, { "OCT", 7 }, { "NOV", 8 }, { "DEC", 9 }, { "JAN", 10 }, { "FEB", 11 }, { "MAR", 12 } };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>> {
                    { "English", new List<string>() { "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR" } },
                    { "German", new List<string>() { "APR", "MAI", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEZ", "JÄN", "FEB", "MÄR" } },
                    { "French", new List< string>() { "AVR", "MAI", "JUIN", "JUIL", "AOÛT", "SEPT", "OCT", "NOV", "DÉC", "JANV", "FÉV", "MARS" } },
                };
        private List<string> Measures => new List<string>() { "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR" };

        public Turnover_PPUMonth(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string TOSql = "select\n"
               + "	d.id as dealer_id,\n"
               + "	coalesce(ppu.total_this, 0) total_this\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id, \n"
               + "		SUM(real_value) as total_this\n"
               + "	from  RealBusiness s\n"
               + "	where dataperiod_id between @FYearStart and @To\n"
               + "		and level4_code in ('DAM','EXM','MEC','ROM')\n"
               + "		and dataperiod_id <= @Month\n"
               + "	group by dealer_id\n"
               + ") ppu on ppu.dealer_id = d.id";

            string ParcSql = "select\n"
               + "	d.id dealer_id,\n"
               + "	coalesce(parc, 0) parc\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id, dbo.dividetwonumbers(sum(parc_value),12) * @Months as parc\n"
               + "	from v_CarParcTenYear t1\n"
               + "	where finyear = @FinYear\n"
               + "		and parc_year between 1 and 10\n"
               + "	group by dealer_id\n"
               + ") parc on parc.dealer_id = d.id";

            foreach (string measure in Measures)
            {
                int month = Period.FYearStartPeriod + Measures.IndexOf(measure);

                res.Add("TO_" + measure, MSSQLHelper.GetDictionary<int, decimal?>(TOSql,
                    new SqlParameter[]
                    {
                        new SqlParameter("@FYearStart", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                        new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                        new SqlParameter("@Month", SqlDbType.Int) { Value = (month > Period.Id ? Period.FYearStartPeriod - 1 : month) },
                    }
                ).ToDictionary(x => x.Key, x => x.Value.ToString()));

                res.Add("Parc_" + measure, MSSQLHelper.GetDictionary<int, decimal?>(ParcSql,
                    new SqlParameter[]
                    {
                        new SqlParameter("@FYearStart", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                        new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                        new SqlParameter("@FinYear", SqlDbType.VarChar) { Value = Period.FinancialYear },
                        new SqlParameter("@Months", SqlDbType.Int) { Value = Measures.IndexOf(measure) + 1 },
                    }
                ).ToDictionary(x => x.Key, x => x.Value.ToString()));
            }

            return res;
        }
    }
}
