﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.PPM_AT
{
    public class PartsBasket : DashboardModel<decimal>
    {
        public override Dictionary<string, decimal> Template => new Dictionary<string, decimal>() {
            { "Performance", 0m },
            { "Value", 0m },
            { "YTD", 0m }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>()
        {
            { "Performance", 1 },
            { "Value", 2 },
            { "YTD", 3 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>();

        public PartsBasket(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string sql = 
                @"select
	                d.id dealer_id,
                    d.dealergroup_id,
	                coalesce(ThisYear, 0) ThisYear,
	                coalesce(LastYear, 0) LastYear,
                    coalesce(ThisQuarter, 0) ThisQuarter
                from v_LiveDealers d
                left join (
	                SELECT 
		                a.DealerGroup_Id,
		                SUM(CASE WHEN a.DataPeriod_Id BETWEEN @StartPeriod AND @EndPeriod THEN a.DealerCost ELSE 0 END) AS ThisYear, 
		                SUM(CASE WHEN a.DataPeriod_Id BETWEEN (@StartPeriod-12) AND (@EndPeriod-12) AND a.Purchase_Date <= @MaxDateLastYear THEN a.DealerCost ELSE 0 END) AS LastYear,
                        SUM(CASE WHEN a.DataPeriod_Id BETWEEN @QuarterStartPeriod and @EndPeriod then a.DealerCost ELSE 0 END) as ThisQuarter
	                FROM Web_ValidDealerPurchases a
	                INNER JOIN BonusBasketItems b ON a.Level2_Code = b.ProductGroup AND b.Control_Id = (select top 1 id from bonusbasketcontrol where @EndPeriod between period_from and period_to and basketname = 'Parts Basket')
	                inner join dbo.CurrentDealerGroupSelectionTableForBonus('M', '" + Settings.GetString("MarketID") + @"', " + Settings.GetString("MarketID") + @") dg on dg.DealerGroup_ID = a.DealerGroup_Id
	                WHERE a.DataPeriod_Id BETWEEN (@StartPeriod-12) AND @EndPeriod
	                GROUP BY a.DealerGroup_Id
                ) q on q.DealerGroup_Id = d.dealergroup_id";

            string maxDateSql =
                @"select
	                d.id dealer_id,
	                coalesce(MaxDate, '19000101') MaxDate
                from v_LiveDealers d
                left join (
	                select
		                dealergroup_id,
		                convert(decimal, format(ISNULL(MAX(purchase_date),'1900-01-01'), 'yyyyMMdd')) MaxDate
	                from Web_ValidDealerPurchases
	                where DataPeriod_Id <= @PeriodId
	                group by DealerGroup_Id
                ) a on a.DealerGroup_Id = d.dealergroup_id";

            EnumerableRowCollection<DataRow> data = MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                new SqlParameter("@StartPeriod", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                new SqlParameter("@QuarterStartPeriod", SqlDbType.Int) { Value = Period.Id - ((Period.FinancialMonth - 1) % 3) },
                new SqlParameter("@EndPeriod", SqlDbType.Int) { Value = Period.Id },
                new SqlParameter("@MaxDateLastYear", SqlDbType.Date) { Value = Period.EndDate.AddYears(-1) }
            }).AsEnumerable();

            EnumerableRowCollection<DataRow> maxDateData = MSSQLHelper.GetDataTable(maxDateSql, new SqlParameter[] {
                new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
            }).AsEnumerable();

            res.Add("dealergroup", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<int>("DealerGroup_Id").ToString()));
            res.Add("ThisYear", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("ThisYear").ToString()));
            res.Add("LastYear", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("LastYear").ToString()));
            res.Add("ThisQuarter", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("ThisQuarter").ToString()));
            res.Add("MaxDate", maxDateData.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("MaxDate").ToString()));

            return res;
        }
    }
}
