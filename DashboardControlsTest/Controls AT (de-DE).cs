﻿using System;
using System.Globalization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QubeDashboardControls;
using QubeUtils;

namespace DashboardControlsTest
{
    [TestClass]
    public class ControlsAT_deDE
    {
        [TestMethod]
        public void RealBusiness()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(152);

            DataPill pill1 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "6");
            DataPill pill2 = new DataPill(DashboardMeasure.RealBusiness_ExtendedMaintenance, "M", "6");
            DataPill pill3 = new DataPill(DashboardMeasure.RealBusiness_MechanicalRepair, "M", "6");
            DataPill pill4 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "6");
            DataPill pill5 = new DataPill(DashboardMeasure.RealBusiness_Damage, "M", "6");
            DataPill pill6 = new DataPill(DashboardMeasure.RealBusiness_Total, "M", "6");
        }

        [TestMethod]
        public void PPU_Month()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            LineGraph ppuMonth = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "6");
        }

        [TestMethod]
        public void PPU_Category()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            BarGraph ppuCat = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "5");
        }

        [TestMethod]
        public void StockVsEmergency()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(153);

            Dial dial1 = new Dial(DashboardMeasure.StockEmergency_RoutineMaintenance, "M", "6");
            Dial dial2 = new Dial(DashboardMeasure.StockEmergency_ExtendedMaintenance, "M", "6");
            Dial dial3 = new Dial(DashboardMeasure.StockEmergency_MechanicalRepair, "M", "6");
            Dial dial4 = new Dial(DashboardMeasure.StockEmergency_Damage, "M", "6");
            Dial dial5 = new Dial(DashboardMeasure.StockEmergency_Accessories, "M", "6");
            Dial dial6 = new Dial(DashboardMeasure.StockEmergency_Total, "M", "6");
        }

        [TestMethod]
        public void TurnoverByType_FB()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(157);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "6", period);
        }

        [TestMethod]
        public void TurnoverByType_RB()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(154);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "6");
        }

        [TestMethod]
        public void TurnoverByYear()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(154);

            BarGraph barGraph1 = new BarGraph(DashboardMeasure.Turnover_PPUYear, "R", "1", period);
        }

        [TestMethod]
        public void RealBusinessAA()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(157);

            BarGraph lineGraph1 = new BarGraph(DashboardMeasure.RealBusinessAA_Accessories, "M", "6", period);
            BarGraph lineGraph2 = new BarGraph(DashboardMeasure.RealBusinessAA_Damage, "M", "6", period);
            BarGraph lineGraph3 = new BarGraph(DashboardMeasure.RealBusinessAA_ExtendedMaintenance, "M", "6", period);
            BarGraph lineGraph4 = new BarGraph(DashboardMeasure.RealBusinessAA_MechanicalRepair, "M", "6", period);
            BarGraph lineGraph5 = new BarGraph(DashboardMeasure.RealBusinessAA_RoutineMaintenance, "M", "6", period);
            BarGraph lineGraph6 = new BarGraph(DashboardMeasure.RealBusinessAA_Total, "M", "6", period);
        }

        [TestMethod]
        public void AccessoriesBySales()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            BarGraph barGraph1 = new BarGraph(DashboardMeasure.AccessoriesBySales, "M", "6", new DataPeriod(156));
        }

        [TestMethod]
        public void TurnoverByType()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "6");
            DataGrid grid2 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "6");
        }

        [TestMethod]
        public void Total_MainDashboard()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(157);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "6", period);
            DataGrid grid2 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "6", period);

            BarGraph barGraph1 = new BarGraph(DashboardMeasure.RealBusinessAA_Accessories, "M", "6", period);
            BarGraph barGraph2 = new BarGraph(DashboardMeasure.RealBusinessAA_Damage, "M", "6", period);
            BarGraph barGraph3 = new BarGraph(DashboardMeasure.RealBusinessAA_ExtendedMaintenance, "M", "6", period);
            BarGraph barGraph4 = new BarGraph(DashboardMeasure.RealBusinessAA_MechanicalRepair, "M", "6", period);
            BarGraph barGraph5 = new BarGraph(DashboardMeasure.RealBusinessAA_RoutineMaintenance, "M", "6", period);
            BarGraph barGraph6 = new BarGraph(DashboardMeasure.RealBusinessAA_Total, "M", "6", period);

            BarGraph barGraph7 = new BarGraph(DashboardMeasure.Turnover_PPUYear, "M", "6", period);
            BarGraph barGraph8 = new BarGraph(DashboardMeasure.AccessoriesBySales, "M", "6", period);
        }

        [TestMethod]
        public void EquipmentRate()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            Dial dial7 = new Dial(DashboardMeasure.EquipmentRate_OilFilters, "M", "6");
            Dial dial8 = new Dial(DashboardMeasure.EquipmentRate_AirFilters, "M", "6");
            Dial dial9 = new Dial(DashboardMeasure.EquipmentRate_PollenFilters, "M", "6");
            Dial dial10 = new Dial(DashboardMeasure.EquipmentRate_FuelFilters, "M", "6");
            Dial dial11 = new Dial(DashboardMeasure.EquipmentRate_SparkPlugs, "M", "6");
            Dial dial12 = new Dial(DashboardMeasure.EquipmentRate_WiperBlades, "M", "6");
            Dial dial13 = new Dial(DashboardMeasure.EquipmentRate_BrakePads, "M", "6");
            Dial dial14 = new Dial(DashboardMeasure.EquipmentRate_BrakeDiscs, "M", "6");
            Dial dial15 = new Dial(DashboardMeasure.EquipmentRate_TimingBelts, "M", "6");
            Dial dial16 = new Dial(DashboardMeasure.EquipmentRate_Batteries, "M", "6");
        }

        //[TestMethod]
        //public void BadMeasure()
        //{
        //    DataPill pill1 = new DataPill("BAD", "M", "6");
        //}

        [TestMethod]
        public void Total()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPill pill1 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "6");
            DataPill pill2 = new DataPill(DashboardMeasure.RealBusiness_ExtendedMaintenance, "M", "6");
            DataPill pill3 = new DataPill(DashboardMeasure.RealBusiness_MechanicalRepair, "M", "6");
            DataPill pill4 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "6");
            DataPill pill5 = new DataPill(DashboardMeasure.RealBusiness_Damage, "M", "6");
            DataPill pill6 = new DataPill(DashboardMeasure.RealBusiness_Total, "M", "6");

            Dial dial1 = new Dial(DashboardMeasure.StockEmergency_RoutineMaintenance, "M", "6");
            Dial dial2 = new Dial(DashboardMeasure.StockEmergency_ExtendedMaintenance, "M", "6");
            Dial dial3 = new Dial(DashboardMeasure.StockEmergency_MechanicalRepair, "M", "6");
            Dial dial4 = new Dial(DashboardMeasure.StockEmergency_Damage, "M", "6");
            Dial dial5 = new Dial(DashboardMeasure.StockEmergency_Accessories, "M", "6");
            Dial dial6 = new Dial(DashboardMeasure.StockEmergency_Total, "M", "6");

            LineGraph ppuMonth = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "6");
            BarGraph ppuCat = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "6");

            Dial dial7 = new Dial(DashboardMeasure.EquipmentRate_OilFilters, "M", "6");
            Dial dial8 = new Dial(DashboardMeasure.EquipmentRate_AirFilters, "M", "6");
            Dial dial9 = new Dial(DashboardMeasure.EquipmentRate_PollenFilters, "M", "6");
            Dial dial10 = new Dial(DashboardMeasure.EquipmentRate_FuelFilters, "M", "6");
            Dial dial11 = new Dial(DashboardMeasure.EquipmentRate_SparkPlugs, "M", "6");
            Dial dial12 = new Dial(DashboardMeasure.EquipmentRate_WiperBlades, "M", "6");
            Dial dial13 = new Dial(DashboardMeasure.EquipmentRate_BrakePads, "M", "6");
            Dial dial14 = new Dial(DashboardMeasure.EquipmentRate_BrakeDiscs, "M", "6");
            Dial dial15 = new Dial(DashboardMeasure.EquipmentRate_TimingBelts, "M", "6");
            Dial dial16 = new Dial(DashboardMeasure.EquipmentRate_Batteries, "M", "6");

        }

        [TestMethod]
        public void PPUMonth_Match_PPUCat()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(151);

            LineGraph ppuCat = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "6", period);
            BarGraph ppuMonth = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "6", period);

            //Must change this when change period id
            Assert.AreEqual(ppuCat.NatValues["JUL"], ppuMonth.NatValues["TOT"]);
        }

        [TestMethod]
        public void PartsBasket()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(159);

            DashboardControl<decimal> pb = new DashboardControl<decimal>(DashboardMeasure.PartsBasket, "G", "6001", period);
        }

        [TestMethod]
        public void AccessoriesBasket()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(159);

            DashboardControl<decimal> pb = new DashboardControl<decimal>(DashboardMeasure.AccessoriesBasket, "G", "6002", period);
        }

        [TestMethod]
        public void LifeCycleBasket()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(159);

            DashboardControl<decimal> pb = new DashboardControl<decimal>(DashboardMeasure.LifeCycleBasket, "G", "6001", period);
        }

        [TestMethod]
        public void WinterTyresBasket()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(159);

            DashboardControl<decimal> pb = new DashboardControl<decimal>(DashboardMeasure.WinterTyresBasket, "G", "6001", period);
        }

        [TestMethod]
        public void BonusSummary()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
            DataPeriod period = new DataPeriod(159);

            DataGrid pb = new DataGrid(DashboardMeasure.BonusSummary, "M", "6", period);
        }
    }
}
