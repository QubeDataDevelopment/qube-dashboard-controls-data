﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls
{
    public class DataGrid : DashboardControl<DashboardObject>
    {
        public DataGrid(DashboardMeasure measure, string selectionLevel, string selectionId, DataPeriod period = null) : base(measure, selectionLevel, selectionId, period) { }

        protected override bool LastYear => false;
        protected override PeriodSetting PeriodSettings => new PeriodSetting();
        protected override bool AllowValidation => true;

        protected override Dictionary<string, DashboardObject> CalculateData(ControlLevel level = ControlLevel.Default, bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (level == ControlLevel.National ? Model.NationalValues : (level == ControlLevel.Zone ? Model.ZoneValues : (lastYear ? Model.SelectionValuesLY : Model.SelectionValues)));

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return (Dictionary<string, DashboardObject>) res.Execute(lastYear);
        }

        protected override Dictionary<string, Dictionary<int, DashboardObject>> CalculateDealerData(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        protected override Dictionary<string, int> GetQuartiles()
        {
            throw new NotImplementedException();
        }
    }
}
