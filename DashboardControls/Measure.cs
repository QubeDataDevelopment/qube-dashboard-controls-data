﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public class Measure
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DashboardMeasure Enum { get; set; }
        public bool IsPeriodic { get; set; }
        public bool IsCrossSite { get; set; }
        public bool IsGroup { get; set; }
        public bool HasQuartiles { get; set; }
    }

    public static class MeasureFactory
    {
        public static Measure Factory(DashboardMeasure measure)
        {
            DataTable measureInfo = MSSQLHelper.GetDataTable("SELECT Name, IsPeriodic, IsCrossSite, IsGroup, HasQuartiles FROM DashboardMeasure WHERE Id = @Id", new SqlParameter[] { new SqlParameter("@Id", SqlDbType.Int) { Value = (int) measure } });

            if (measureInfo.Rows.Count == 0) throw new ArgumentException("Invalid measure");

            return new Measure()
            {
                Id = (int)measure,
                Name = measureInfo.Rows[0].Field<string>("Name"),
                Enum = measure,
                IsPeriodic = measureInfo.Rows[0].Field<bool>("IsPeriodic"),
                IsCrossSite = measureInfo.Rows[0].Field<bool>("IsCrossSite"),
                IsGroup = measureInfo.Rows[0].Field<bool>("IsGroup"),
                HasQuartiles = measureInfo.Rows[0].Field<bool>("HasQuartiles"),
            };
        }
    }
}
