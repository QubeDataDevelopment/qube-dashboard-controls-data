﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class Turnover_PPUYear : DashboardObject
    {
        public decimal TO { get; set; }
        public decimal Parc { get; set; }
        public decimal YTD { get; set; }
    }
}
