﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Controllers
{
    public class EquipmentRate : Controller
    {
        public EquipmentRate(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override object Execute(bool lastYear = false)
        {
            Dictionary<string, decimal> res = new Dictionary<string, decimal>();

            if (!Data.ContainsKey("Units")) return res;

            decimal Units = Data["Units"].Sum(s => decimal.Parse(s.Value));
            decimal Parc = Data["Parc"].Sum(s => decimal.Parse(s.Value));

            res.Add("Score" + (lastYear ? "LY" : ""), (Parc > 0 ? Units / Parc : 0m));

            return res;
        }

        public override object ExecuteDealer(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, decimal>> res = new Dictionary<string, Dictionary<int, decimal>>();

            res.Add("Units" + (lastYear ? "LY" : ""), Data["Units"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)));
            res.Add("Parc" + (lastYear ? "LY" : ""), Data["Parc"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)));
            res.Add("Score" + (lastYear ? "LY" : ""), Data["Units"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value))
                .Join(Data["Parc"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)), j1 => j1.Key, j2 => j2.Key, (j1, j2) => new { dealer_id = j1.Key, Units = j1.Value, Parc = j2.Value })
                .ToDictionary(x => x.dealer_id, x => (x.Parc > 0 ? x.Units / x.Parc : 0m))
            );

            return res;
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
