﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class TurnoverByType_FB : DashboardObject
    {
        public string Name { get; set; }
        public decimal MTD { get; set; }
        public decimal MTDLY { get; set; }
        public decimal MTDChange { get; set; }
        public decimal MTDChangePerc { get; set; }
        public decimal YTD { get; set; }
        public decimal YTDLY { get; set; }
        public decimal YTDChange { get; set; }
        public decimal YTDChangePerc { get; set; }
    }
}
