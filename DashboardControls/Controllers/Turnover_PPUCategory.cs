﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Controllers
{
    public class Turnover_PPUCategory : Controller
    {
        public Turnover_PPUCategory(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.Turnover_PPUCategory> res = new Dictionary<string, Objects.XS.Turnover_PPUCategory>();

            if (!Data.ContainsKey("Values" + (lastYear ? "LY" : ""))) return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);

            Dictionary<int, Objects.XS.Turnover_PPUCategory> resE = Data["Values" + (lastYear ? "LY" : "")].ToDictionary(x => x.Key, x => new JavaScriptSerializer().Deserialize<Objects.XS.Turnover_PPUCategory>(x.Value));

            res.Add("Values" + (lastYear ? "LY" : ""), new Objects.XS.Turnover_PPUCategory() {
                TO_ROM = resE.Sum(s => s.Value.TO_ROM),
                TO_EXM = resE.Sum(s => s.Value.TO_EXM),
                TO_MEC = resE.Sum(s => s.Value.TO_MEC),
                TO_DAM = resE.Sum(s => s.Value.TO_DAM),
                TO_TOT = resE.Sum(s => s.Value.TO_TOT),
                Parc_ROM = resE.Sum(s => s.Value.Parc_ROM),
                Parc_EXM = resE.Sum(s => s.Value.Parc_EXM),
                Parc_MEC = resE.Sum(s => s.Value.Parc_MEC),
                Parc_DAM = resE.Sum(s => s.Value.Parc_DAM),
                Parc_TOT = resE.Sum(s => s.Value.Parc_TOT),
                ROM = resE.Sum(s => s.Value.Parc_ROM) > 0 ? resE.Sum(s => s.Value.TO_ROM) / resE.Sum(s => s.Value.Parc_ROM) : 0m,
                EXM = resE.Sum(s => s.Value.Parc_EXM) > 0 ? resE.Sum(s => s.Value.TO_EXM) / resE.Sum(s => s.Value.Parc_EXM) : 0m,
                MEC = resE.Sum(s => s.Value.Parc_MEC) > 0 ? resE.Sum(s => s.Value.TO_MEC) / resE.Sum(s => s.Value.Parc_MEC) : 0m,
                DAM = resE.Sum(s => s.Value.Parc_DAM) > 0 ? resE.Sum(s => s.Value.TO_DAM) / resE.Sum(s => s.Value.Parc_DAM) : 0m,
                TOT = resE.Sum(s => s.Value.Parc_TOT) > 0 ? resE.Sum(s => s.Value.TO_TOT) / resE.Sum(s => s.Value.Parc_TOT) : 0m,
            });

            return res.ToDictionary(x => x.Key, x => (DashboardObject) x.Value);
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            Dictionary<string, int> ranks = new Dictionary<string, int>();
            Dictionary<string, int> res = new Dictionary<string, int>();
            Dictionary<int, Objects.XS.Turnover_PPUCategory> resE = Data["Values"].ToDictionary(x => x.Key, x => new JavaScriptSerializer().Deserialize<Objects.XS.Turnover_PPUCategory>(x.Value));
            Dictionary<string, Objects.XS.Turnover_PPUCategory> resS = selectionData.ToDictionary(x => x.Key, x => (Objects.XS.Turnover_PPUCategory) x.Value);

            int count = resE.Count;
            decimal perQuartile = count / 4m;

            foreach (KeyValuePair<int, Objects.XS.Turnover_PPUCategory> v in resE)
            {
                v.Value.ROM = v.Value.Parc_ROM > 0 ? v.Value.TO_ROM / v.Value.Parc_ROM : 0m;
                v.Value.EXM = v.Value.Parc_EXM > 0 ? v.Value.TO_EXM / v.Value.Parc_EXM : 0m;
                v.Value.MEC = v.Value.Parc_MEC > 0 ? v.Value.TO_MEC / v.Value.Parc_MEC : 0m;
                v.Value.DAM = v.Value.Parc_DAM > 0 ? v.Value.TO_DAM / v.Value.Parc_DAM : 0m;
                v.Value.TOT = v.Value.Parc_TOT > 0 ? v.Value.TO_TOT / v.Value.Parc_TOT : 0m;
            }

            ranks.Add("ROM", resE.Where(w => w.Value.ROM > resS["Values"].ROM).Count() + 1);
            ranks.Add("EXM", resE.Where(w => w.Value.EXM > resS["Values"].EXM).Count() + 1);
            ranks.Add("MEC", resE.Where(w => w.Value.MEC > resS["Values"].MEC).Count() + 1);
            ranks.Add("DAM", resE.Where(w => w.Value.DAM > resS["Values"].DAM).Count() + 1);
            ranks.Add("TOT", resE.Where(w => w.Value.TOT > resS["Values"].TOT).Count() + 1);

            res.Add("ROM", int.Parse((Math.Floor(decimal.Parse(ranks["ROM"].ToString()) / perQuartile) + 1).ToString()));
            res.Add("EXM", int.Parse((Math.Floor(decimal.Parse(ranks["EXM"].ToString()) / perQuartile) + 1).ToString()));
            res.Add("MEC", int.Parse((Math.Floor(decimal.Parse(ranks["MEC"].ToString()) / perQuartile) + 1).ToString()));
            res.Add("DAM", int.Parse((Math.Floor(decimal.Parse(ranks["DAM"].ToString()) / perQuartile) + 1).ToString()));
            res.Add("TOT", int.Parse((Math.Floor(decimal.Parse(ranks["TOT"].ToString()) / perQuartile) + 1).ToString()));

            return res;
        }
    }
}
