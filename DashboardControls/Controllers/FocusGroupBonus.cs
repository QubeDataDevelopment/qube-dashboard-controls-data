﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls.Controllers
{
    public class FocusGroupBonus : Controller
    {
        public FocusGroupBonus(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.FocusGroupBonus> res = new Dictionary<string, Objects.XS.FocusGroupBonus>();

            if (!Data.ContainsKey("Values" + (lastYear ? "LY" : ""))) return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);

            List<Objects.XS.FocusGroupBonus> resE = Data["Values" + (lastYear ? "LY" : "")].Select(x => new JavaScriptSerializer().Deserialize<Objects.XS.FocusGroupBonus>(x.Value)).DistinctBy(s => s.dealergroup_id).ToList();

            res.Add("TargetPerc", new Objects.XS.FocusGroupBonus()
            {
                Name = "TargetPerc",
                BrakeDiscs = resE.First().BrakeDiscs_Target,
                Wallboxes = resE.First().Wallboxes_Target,
                PollenFilters = resE.First().PollenFilters_Target,
                BrakePads = resE.First().BrakePads_Target,
                IsTrafficLight = true,
                DataType = "int",
                DisplayType = "percentage"
            });

            res.Add("Actual", new Objects.XS.FocusGroupBonus()
            {
                Name = "Actual",
                BrakeDiscs = resE.Sum(s => s.EXM_Parc) == 0m ? 0m : resE.Sum(s => s.BrakeDiscs_Sold) / resE.Sum(s => s.EXM_Parc),
                Wallboxes = resE.Sum(s => s.EXM_Parc) == 0m ? 0m : resE.Sum(s => s.Wallboxes_Sold) / resE.Sum(s => s.EXM_Parc),
                PollenFilters = resE.Sum(s => s.ROM_Parc) == 0m ? 0m : resE.Sum(s => s.PollenFilters_Sold) / resE.Sum(s => s.ROM_Parc),
                BrakePads = resE.Sum(s => s.EXM_Parc) == 0m ? 0m : resE.Sum(s => s.BrakePads_Sold) / resE.Sum(s => s.EXM_Parc),
                IsTrafficLight = true,
                DataType = "int",
                DisplayType = "percentage"
            });

            res.Add("Target", new Objects.XS.FocusGroupBonus()
            {
                Name = "Target",
                BrakeDiscs = Math.Round(resE.Sum(s => s.BrakeDiscs_Parc) * resE.First().BrakeDiscs_Target),
                Wallboxes = Math.Round(resE.Sum(s => s.Wallboxes_Parc) * resE.First().Wallboxes_Target),
                PollenFilters = Math.Round(resE.Sum(s => s.PollenFilters_Parc) * resE.First().PollenFilters_Target),
                BrakePads = Math.Round(resE.Sum(s => s.BrakePads_Parc) * resE.First().BrakePads_Target),
                DataType = "int",
                DisplayType = "number"
            });

            res.Add("Missing", new Objects.XS.FocusGroupBonus()
            {
                Name = "Missing",
                BrakeDiscs = resE.Sum(s => s.BrakeDiscs_Sold) - res["Target"].BrakeDiscs,
                Wallboxes = resE.Sum(s => s.Wallboxes_Sold) - res["Target"].Wallboxes,
                PollenFilters = resE.Sum(s => s.PollenFilters_Sold) - res["Target"].PollenFilters,
                BrakePads = resE.Sum(s => s.BrakePads_Sold) - res["Target"].BrakePads,
                DataType = "int",
                DisplayType = "number"
            });

            return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
