﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class FocusGroupBonus : DashboardObject
    {
        public string Name { get; set; }
        public decimal AirFilters { get; set; }
        public decimal OilFilters { get; set; }
        public decimal BrakeDiscs { get; set; }
        public decimal BrakePads { get; set; }
        public decimal PollenFilters { get; set; }
        public decimal SparkPlugs { get; set; }
        public decimal FuelFilters { get; set; }
        public decimal WiperBlades { get; set; }
        public decimal VAFilters { get; set; }
        public decimal Batteries { get; set; }
        public decimal Wallboxes { get; set; }
        public bool IsTrafficLight { get; set; }
        public string DataType { get; set; }
        public string DisplayType { get; set; }

        // Used in calculation
        public decimal dealergroup_id { get; set; }
        public decimal AirFilters_Sold { get; set; }
        public decimal AirFilters_Target { get; set; }
        public decimal AirFilters_Parc { get; set; }
        public decimal OilFilters_Sold { get; set; }
        public decimal OilFilters_Target { get; set; }
        public decimal OilFilters_Parc { get; set; }
        public decimal SparkPlugs_Sold { get; set; }
        public decimal SparkPlugs_Target { get; set; }
        public decimal SparkPlugs_Parc { get; set; }
        public decimal BrakePads_Sold { get; set; }
        public decimal BrakePads_Target { get; set; }
        public decimal BrakePads_Parc { get; set; }
        public decimal BrakeDiscs_Sold { get; set; }
        public decimal BrakeDiscs_Target { get; set; }
        public decimal BrakeDiscs_Parc { get; set; }
        public decimal PollenFilters_Sold { get; set; }
        public decimal PollenFilters_Target { get; set; }
        public decimal PollenFilters_Parc { get; set; }
        public decimal FuelFilters_Sold { get; set; }
        public decimal FuelFilters_Target { get; set; }
        public decimal FuelFilters_Parc { get; set; }
        public decimal WiperBlades_Sold { get; set; }
        public decimal WiperBlades_Target { get; set; }
        public decimal WiperBlades_Parc { get; set; }
        public decimal VAFilters_Sold { get; set; }
        public decimal VAFilters_Target { get; set; }
        public decimal VAFilters_Parc { get; set; }
        public decimal Batteries_Sold { get; set; }
        public decimal Batteries_Target { get; set; }
        public decimal Batteries_Parc { get; set; }
        public decimal Wallboxes_Sold { get; set; }
        public decimal Wallboxes_Target { get; set; }
        public decimal Wallboxes_Parc { get; set; }
        public decimal ROM_Parc { get; set; }
        public decimal EXM_Parc { get; set; }

    }
}
