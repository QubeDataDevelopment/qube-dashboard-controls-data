﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls.Controllers
{
    public class BonusSummary : Controller
    {
        public BonusSummary(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.PPM_AT.BonusSummary> res = new Dictionary<string, Objects.PPM_AT.BonusSummary>();

            if (!Data.ContainsKey("Values" + (lastYear ? "LY" : ""))) return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);

            List<Objects.PPM_AT.BonusSummary_Process> resE = Data["Values" + (lastYear ? "LY" : "")].Select(x => new JavaScriptSerializer().Deserialize<Objects.PPM_AT.BonusSummary_Process>(x.Value)).DistinctBy(s => s.dealergroup_id).ToList();

            res.Add("Q1", new Objects.PPM_AT.BonusSummary()
            {
                Name = "Q1",
                Actual_PartsBasket = resE.SumNullable(s => s.Bonus_Q1_PartsBasket),
                Actual_AccessoriesBasket = resE.SumNullable(s => s.Bonus_Q1_AccessoriesBasket),
                Actual_LifeCycleBasket = resE.SumNullable(s => s.Bonus_Q1_LifeCycleBasket),
                Actual_WinterTyresBasket = resE.SumNullable(s => s.Bonus_Q1_WinterTyresBasket),
                Actual_Total = (resE.SumNullable(s => s.Bonus_Q1_PartsBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q1_PartsBasket))) + (resE.SumNullable(s => s.Bonus_Q1_AccessoriesBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q1_AccessoriesBasket))) + (resE.SumNullable(s => s.Bonus_Q1_LifeCycleBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q1_LifeCycleBasket))) + (resE.SumNullable(s => s.Bonus_Q1_WinterTyresBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q1_WinterTyresBasket))),
            });

            res.Add("Q2", new Objects.PPM_AT.BonusSummary()
            {
                Name = "Q2",
                Actual_PartsBasket = resE.SumNullable(s => s.Bonus_Q2_PartsBasket),
                Actual_AccessoriesBasket = resE.SumNullable(s => s.Bonus_Q2_AccessoriesBasket),
                Actual_LifeCycleBasket = resE.SumNullable(s => s.Bonus_Q2_LifeCycleBasket),
                Actual_WinterTyresBasket = resE.SumNullable(s => s.Bonus_Q2_WinterTyresBasket),
                Actual_Total = (resE.SumNullable(s => s.Bonus_Q2_PartsBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q2_PartsBasket))) + (resE.SumNullable(s => s.Bonus_Q2_AccessoriesBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q2_AccessoriesBasket))) + (resE.SumNullable(s => s.Bonus_Q2_LifeCycleBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q2_LifeCycleBasket))) + (resE.SumNullable(s => s.Bonus_Q2_WinterTyresBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q2_WinterTyresBasket))),
            });

            res.Add("Q3", new Objects.PPM_AT.BonusSummary()
            {
                Name = "Q3",
                Actual_PartsBasket = resE.SumNullable(s => s.Bonus_Q3_PartsBasket),
                Actual_AccessoriesBasket = resE.SumNullable(s => s.Bonus_Q3_AccessoriesBasket),
                Actual_LifeCycleBasket = resE.SumNullable(s => s.Bonus_Q3_LifeCycleBasket),
                Actual_WinterTyresBasket = resE.SumNullable(s => s.Bonus_Q3_WinterTyresBasket),
                Actual_Total = (resE.SumNullable(s => s.Bonus_Q3_PartsBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q3_PartsBasket))) + (resE.SumNullable(s => s.Bonus_Q3_AccessoriesBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q3_AccessoriesBasket))) + (resE.SumNullable(s => s.Bonus_Q3_LifeCycleBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q3_LifeCycleBasket))) + (resE.SumNullable(s => s.Bonus_Q3_WinterTyresBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q3_WinterTyresBasket))),
            });

            res.Add("Q4", new Objects.PPM_AT.BonusSummary()
            {
                Name = "Q4",
                Actual_PartsBasket = resE.SumNullable(s => s.Bonus_Q4_PartsBasket),
                Actual_AccessoriesBasket = resE.SumNullable(s => s.Bonus_Q4_AccessoriesBasket),
                Actual_LifeCycleBasket = resE.SumNullable(s => s.Bonus_Q4_LifeCycleBasket),
                Actual_WinterTyresBasket = resE.SumNullable(s => s.Bonus_Q4_WinterTyresBasket),
                Actual_Total = (resE.SumNullable(s => s.Bonus_Q4_PartsBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q4_PartsBasket))) + (resE.SumNullable(s => s.Bonus_Q4_AccessoriesBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q4_AccessoriesBasket))) + (resE.SumNullable(s => s.Bonus_Q4_LifeCycleBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q4_LifeCycleBasket))) + (resE.SumNullable(s => s.Bonus_Q4_WinterTyresBasket) == null ? 0m : (decimal?)Decimal.Round((decimal)resE.SumNullable(s => s.Bonus_Q4_WinterTyresBasket))),
            });

            res.Add("YTD", new Objects.PPM_AT.BonusSummary()
            {
                Name = "YTD",
                Actual_PartsBasket = res.Any(w => w.Value.Actual_PartsBasket != null) ? res.Where(w => w.Value.Actual_PartsBasket != null).Sum(s => s.Value.Actual_PartsBasket) : null,
                Actual_AccessoriesBasket = res.Any(w => w.Value.Actual_AccessoriesBasket != null) ? res.Where(w => w.Value.Actual_AccessoriesBasket != null).Sum(s => s.Value.Actual_AccessoriesBasket) : null,
                Actual_LifeCycleBasket = res.Any(w => w.Value.Actual_LifeCycleBasket != null) ? res.Where(w => w.Value.Actual_LifeCycleBasket != null).Sum(s => s.Value.Actual_LifeCycleBasket) : null,
                Actual_WinterTyresBasket = res.Any(w => w.Value.Actual_WinterTyresBasket != null) ? res.Where(w => w.Value.Actual_WinterTyresBasket != null).Last().Value.Actual_WinterTyresBasket : null,
                Actual_Total = decimal.Parse((
                    Math.Round((double) (res.Any(w => w.Value.Actual_PartsBasket != null) ? res.Where(w => w.Value.Actual_PartsBasket != null).Sum(s => s.Value.Actual_PartsBasket) : 0m), 0) +
                    Math.Round((double) (res.Any(w => w.Value.Actual_AccessoriesBasket != null) ? res.Where(w => w.Value.Actual_AccessoriesBasket != null).Sum(s => s.Value.Actual_AccessoriesBasket) : 0m), 0) +
                    Math.Round((double) (res.Any(w => w.Value.Actual_LifeCycleBasket != null) ? res.Where(w => w.Value.Actual_LifeCycleBasket != null).Sum(s => s.Value.Actual_LifeCycleBasket) : 0m), 0) +
                    Math.Round((double) (res.Any(w => w.Value.Actual_WinterTyresBasket != null) ? res.Where(w => w.Value.Actual_WinterTyresBasket != null).Last().Value.Actual_WinterTyresBasket : 0m), 0)
                ).ToString()),
            });


            return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
