﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.PPM_AT
{
    public class BonusSummary : DashboardObject
    {
        public string Name { get; set; }
        public decimal? Actual_PartsBasket { get; set; }
        public decimal? Actual_AccessoriesBasket { get; set; }
        public decimal? Actual_LifeCycleBasket { get; set; }
        public decimal? Actual_WinterTyresBasket { get; set; }
        public decimal? Actual_Total { get; set; }
        
    }
}
