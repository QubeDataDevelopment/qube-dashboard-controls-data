﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls.Models.XS
{
    public class FocusGroupBonus : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>{
            { "TargetPerc", null },
            { "Actual", null },
            { "Target", null },
            { "Missing", null }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>()
        {
            { "TargetPerc", 1 },
            { "Actual", 2 },
            { "Target", 3 },
            { "Missing", 4 },
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>
        {
            { "English", new List<string>() { "Target Units", "Seasonal Achievement", "Target Units (whole quarter)", "Missing Units  (whole quarter)" } },
            { "German", new List<string>() { "Ziel Einheiten", "Saisonalisiert Erreicht", "Quartalsziel (Stückzahl)", "Fehlmenge zu Ziel" } },
            { "French", new List<string>() { "Objectif", "Actuel", "Objectif", "Manquant" } },
        };

        public FocusGroupBonus(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            string sql =
                @"SELECT
                    d.id dealer_id,
                    coalesce(t.dealergroup_ID, 0) dealergroup_ID,
                    coalesce(cast(t.BrakeDiscs_sold as int), 0) BrakeDiscs_sold,
                    coalesce(t.BrakeDiscs_target, 0) BrakeDiscs_target,
                    coalesce(t.BrakeDiscs_quarterparc, 0) BrakeDiscs_parc, 
                    coalesce(cast(t.Wallbox_sold as int), 0) Wallbox_sold,
                    coalesce(t.Wallbox_target, 0) Wallbox_target,
                    coalesce(t.Wallbox_quarterparc, 0) Wallbox_parc,
                    coalesce(cast(t.PollenFilters_sold as int), 0) PollenFilters_sold,
                    coalesce(t.PollenFilters_target, 0) PollenFilters_target,
                    coalesce(t.PollenFilters_quarterparc, 0) PollenFilters_parc,
                    coalesce(cast(t.BrakePads_sold as int), 0) BrakePads_sold,
                    coalesce(t.BrakePads_target, 0) BrakePads_target,
                    coalesce(t.BrakePads_quarterparc, 0) BrakePads_parc,                    
                    coalesce(t.ytdparcexm, 0) ytdparcexm,
                    coalesce(t.ytdparcrom, 0) ytdparcrom
                from v_LiveDealers d
                left join (
                    select
                        *
                    from (
                        select
                            dealergroup_ID,
                            groupname + '_' + col col,
                            value,
                            ytdparcexm,
                            ytdparcrom
                        from (
                            select
                                fg.dealergroup_id,
                                replace(replace(groupname, ' ', ''), '/', '') groupname,
                                quarterparc,
                                cast(unitssold as money) sold,
                                cast(bc.target_pc as money) target,
                                ytdparcexm,
                                ytdparcrom
                            from FG_PerformanceDetail fg
                            inner join FG_MasterControl mc on mc.id = fg.focusgroup_id
                            inner join FG_PerformanceSummary ps on ps.id = fg.performancesummary_ID
                            inner join FG_BonusControl bc on bc.PB_bonuscontrol_id = ps.PB_bonuscontrol_id and bc.FG_master_id = fg.focusgroup_ID
                            left join FG_QuarterTargets qt ON qt.quarter_id=ps.quarter_id AND qt.year_id = ps.year_id AND qt.focusgroup_id = fg.focusgroup_ID and qt.dealergroup_id = fg.dealergroup_ID
                            where
                            ps.quarter_id = @Quarter and ps.year_id = @Year and fg.dealergroup_id IN (SELECT dealergroup_id FROM dbo.PB_Targets WHERE targetvalue > 0 AND control_id = bc.PB_bonuscontrol_id)
                        ) d
                        unpivot(
                            value for col in (sold, target, quarterparc)
                        ) up
                    ) q
                    pivot (
                        sum(value) for col in (BrakeDiscs_quarterparc,BrakeDiscs_sold,BrakeDiscs_target,BrakePads_quarterparc,BrakePads_sold,BrakePads_target,PollenFilters_quarterparc,PollenFilters_sold,PollenFilters_target,Wallbox_quarterparc,Wallbox_sold,Wallbox_target)
                    ) u
                ) t on t.dealergroup_ID = d.dealergroup_id";

            DateTime maxPurchaseDate = MSSQLHelper.ExecuteScalar<DateTime>("Select dbo.MaxPurchaseDate()", null);

            return new Dictionary<string, Dictionary<int, string>>() {
                {
                    "Values",
                    MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                        new SqlParameter("@Year", SqlDbType.Int) { Value = (maxPurchaseDate > DateTime.Parse("2019-12-31") && maxPurchaseDate < DateTime.Parse("2020-04-01") ? 2019 : int.Parse(Period.FinancialYear.Substring(0,4))) },
                        new SqlParameter("@Quarter", SqlDbType.Int) { Value = (maxPurchaseDate > DateTime.Parse("2019-12-31") && maxPurchaseDate < DateTime.Parse("2020-04-01") ? 5 : Period.FinancialQuarter) }
                    }).AsEnumerable().Select(s => new {
                        dealer_id = s.Field<int>("dealer_id"),
                        values = new Objects.XS.FocusGroupBonus()
                        {
                            dealergroup_id = s.Field<int>("dealergroup_id"),
                            BrakeDiscs_Sold = s.Field<int>("BrakeDiscs_sold"),
                            BrakeDiscs_Target = s.Field<decimal>("BrakeDiscs_target"),
                            BrakeDiscs_Parc = s.Field<decimal>("BrakeDiscs_parc"),
                            PollenFilters_Sold = s.Field<int>("PollenFilters_sold"),
                            PollenFilters_Target = s.Field<decimal>("PollenFilters_target"),
                            PollenFilters_Parc = s.Field<decimal>("PollenFilters_parc"),
                            Wallboxes_Sold = s.Field<int>("Wallbox_sold"),
                            Wallboxes_Target = s.Field<decimal>("Wallbox_target"),
                            Wallboxes_Parc = s.Field<decimal>("Wallbox_parc"),
                            BrakePads_Sold = s.Field<int>("BrakePads_sold"),
                            BrakePads_Target = s.Field<decimal>("BrakePads_target"),
                            BrakePads_Parc = s.Field<decimal>("BrakePads_parc"),
                            EXM_Parc = s.Field<decimal>("ytdparcexm"),
                            ROM_Parc = s.Field<decimal>("ytdparcrom")
                        }
                    }).ToDictionary(x => x.dealer_id, x => new JavaScriptSerializer().Serialize(x.values))
                }
            };
        }
    }
}
