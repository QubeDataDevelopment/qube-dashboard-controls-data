﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.XS
{
    public class EquipmentRate : DashboardModel<decimal>
    {
        public override Dictionary<string, decimal> Template => new Dictionary<string, decimal>() { { "Score", 0m } };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() { { "Score", 1 } };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>();

        public EquipmentRate(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string UnitSql = "select\n"
               + "	d.id dealer_id,\n"
               + "	coalesce(units, 0) units\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id,\n"
               + "		SUM(real_units) as units\n"
               + "	from (\n"
               + "		select\n"
               + "			dealer_id, real_units\n"
               + "		from RealBusiness\n"
               + "		where dataperiod_id between @From and @To\n"
               + "			and level2_code in (" + GetL2Codes() + ")\n"
               + "	) parts_d\n"
               + "	group by dealer_id\n"
               + ") u on u.dealer_id = d.id";

            string ParcSql = "select\n"
               + "	d.id dealer_id,\n"
               + "	coalesce(parc, 0) parc\n"
               + "from v_LiveDealers d\n"
               + "left join (\n"
               + "	select\n"
               + "		dealer_id, dbo.dividetwonumbers(sum(parc_value),12) * (1+(@To-@From)) as parc\n"
               + "	from v_CarParcTenYear t1\n"
               + "	where finyear = @FinYear\n"
               + "		and parc_year between " + GetParcYearFrom() + " and 10\n"
               + "	group by dealer_id\n"
               + ") parc on parc.dealer_id = d.id";

            res.Add("Units", MSSQLHelper.GetDictionary<int, int>(UnitSql,
                new SqlParameter[]
                {
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                }
            ).ToDictionary(x => x.Key, x => x.Value.ToString()));

            res.Add("Parc", MSSQLHelper.GetDictionary<int, decimal>(ParcSql,
                new SqlParameter[]
                {
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                    new SqlParameter("@FinYear", SqlDbType.VarChar) { Value = Period.FinancialYear },
                }
            ).ToDictionary(x => x.Key, x => x.Value.ToString()));

            return res;
        }

        private string GetL2Codes()
        {
            switch (Measure.Enum)
            {
                case DashboardMeasure.EquipmentRate_OilFilters:
                    return "'E12','V12'";
                case DashboardMeasure.EquipmentRate_AirFilters:
                    return "'E11','V11'";
                case DashboardMeasure.EquipmentRate_PollenFilters:
                    return "'OR6','PR6','VR6'";
                case DashboardMeasure.EquipmentRate_FuelFilters:
                    return "'E0B','V0B'";
                case DashboardMeasure.EquipmentRate_SparkPlugs:
                    return "'F31','F36','F32','V31'";
                case DashboardMeasure.EquipmentRate_WiperBlades:
                    return "'F80','F83','F8A','V80'";
                case DashboardMeasure.EquipmentRate_BrakePads:
                    return "'L62','V62'";
                case DashboardMeasure.EquipmentRate_BrakeDiscs:
                    return "'L23','V23'";
                case DashboardMeasure.EquipmentRate_TimingBelts:
                    return "'E30','E31','V9A','B9A'";
                case DashboardMeasure.EquipmentRate_Batteries:
                    return "'91A'";
                default:
                    throw new ArgumentOutOfRangeException("Invalid measure");
            }
        }

        private string GetParcYearFrom()
        {
            switch (Measure.Enum)
            {
                case DashboardMeasure.EquipmentRate_OilFilters:
                case DashboardMeasure.EquipmentRate_AirFilters:
                case DashboardMeasure.EquipmentRate_PollenFilters:
                case DashboardMeasure.EquipmentRate_FuelFilters:
                case DashboardMeasure.EquipmentRate_SparkPlugs:
                case DashboardMeasure.EquipmentRate_WiperBlades:
                    return "1";
                case DashboardMeasure.EquipmentRate_BrakePads:
                case DashboardMeasure.EquipmentRate_BrakeDiscs:
                case DashboardMeasure.EquipmentRate_TimingBelts:
                case DashboardMeasure.EquipmentRate_Batteries:
                    return "3";
                default:
                    throw new ArgumentOutOfRangeException("Invalid measure");
            }
        }
    }
}
