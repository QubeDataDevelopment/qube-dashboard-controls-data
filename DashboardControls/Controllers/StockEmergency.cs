﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Controllers
{
    public class StockEmergency : Controller
    {
        public StockEmergency(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override object Execute(bool lastYear = false)
        {
            Dictionary<string, decimal> res = new Dictionary<string, decimal>();

            if (!Data.ContainsKey("S_Orders")) return res;

            decimal SOrders = Data["S_Orders"].Sum(s => decimal.Parse(s.Value));
            decimal Orders = Data["Orders"].Sum(s => decimal.Parse(s.Value));

            res.Add("Score" + (lastYear ? "LY" : ""), (Orders > 0 ? SOrders / Orders : 0m));

            return res;
        }

        public override object ExecuteDealer(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, decimal>> res = new Dictionary<string, Dictionary<int, decimal>>();

            res.Add("S_Orders" + (lastYear ? "LY" : ""), Data["S_Orders"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)));
            res.Add("Orders" + (lastYear ? "LY" : ""), Data["Orders"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)));
            res.Add("Score" + (lastYear ? "LY" : ""), Data["S_Orders"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value))
                .Join(Data["Orders"].ToDictionary(x => x.Key, x => decimal.Parse(x.Value)), j1 => j1.Key, j2 => j2.Key, (j1, j2) => new { dealer_id = j1.Key, SOrders = j1.Value, Orders = j2.Value })
                .ToDictionary(x => x.dealer_id, x => (x.Orders > 0 ? x.SOrders / x.Orders : 0m))
            );

            return res;
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
