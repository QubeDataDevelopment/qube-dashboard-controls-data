﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Serialization;
using System.Text;
using System.Threading.Tasks;
using QubeDashboardControls.Objects.XS;
using QubeUtils;

namespace QubeDashboardControls.Models.XS
{
    public class TurnoverByType_RB : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>() { { "ROM", new Objects.XS.TurnoverByType_RB() }, { "EXM", new Objects.XS.TurnoverByType_RB() }, { "MEC", new Objects.XS.TurnoverByType_RB() }, { "DAM", new Objects.XS.TurnoverByType_RB() }, { "ACC", new Objects.XS.TurnoverByType_RB() }, { "TOT", new Objects.XS.TurnoverByType_RB() } };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() { { "ROM", 1 }, { "EXM", 2 }, { "MEC", 3 }, { "DAM", 4 }, { "ACC", 5 }, { "TOT", 6 }};
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>()
        {
            { "English", new List<string>() { "Routine Maintenance", "Extended Maintenance", "Mechanical Repair", "Damage", "Accessories", "Total*" } },
            { "German", new List<string>() { "Wartung", "Verschleiß", "Mechanik", "Karosserie", "Zubehör", "Gesamt*" } },
            { "French", new List<string>() { "Maintenance", "Usure", "Mécanique", "Carrosserie", "Accessoires", "Total*" } },
        };

        public TurnoverByType_RB(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            string sql = "select\n"
                       + "	d.id dealer_id\n"
                       + "	,coalesce(r.Level4_Code, '') Level4_Code\n"
                       + "	,coalesce(YTD, 0) YTD\n"
                       + "	,coalesce(YTDLY, 0) YTDLY\n"
                       + "	,coalesce(commitment, 0) Commitment\n"
                       + "from v_LiveDealers d\n"
                       + "left join (\n"
                       + "	SELECT\n"
                       + "		dealer_id\n"
                       + "		,Level4_Code\n"
                       + "		,SUM(case when dataperiod_id between @From and @To then real_value end) YTD\n"
                       + "		,SUM(case when dataperiod_id between @From - 12 and @To - 12 then real_value end) YTDLY\n"
                       + "	FROM RealBusiness\n"
                       + "	WHERE (dataperiod_id BETWEEN @From AND @To or dataperiod_id BETWEEN @From - 12 and @To - 12)\n"
                       + "		and Level4_Code in ('ROM', 'EXM', 'MEC', 'DAM', 'ACC')\n"
                       + "	group by dealer_id, Level4_Code\n"
                       + ") r on r.dealer_id = d.id\n"
                       + "left join (\n"
                       + "	select\n"
                       + "		dealer_id,\n"
                       + "		Level4_Code,\n"
                       + "        SUM(commitment_month) commitment\n"
                       + "	from dealercommitment\n"
                       + "	where dataperiod_id between @From and @To\n"
                       + "		and commitment_type = 'RB'\n"
                       + "	group by dealer_id, level4_code\n"
                       + ") c on c.dealer_id = d.id and c.level4_code = r.level4_code";

            DataTable data = MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                new SqlParameter("@FromDate", SqlDbType.Date) { Value = Period.FYearStartDate },
                new SqlParameter("@ToDate", SqlDbType.Date) { Value = Period.EndDate },
            });

            return
            data.AsEnumerable().Select(s => new {
                DealerId = s.Field<int>("dealer_id"),
                Category = s.Field<string>("Level4_Code"),
                YTD = s.Field<decimal>("YTD"),
                YTDLY = s.Field<decimal>("YTDLY"),
                YTDTarget = s.Field<decimal>("Commitment")
            }).GroupBy(g => g.Category)
            .Where(w => !string.IsNullOrEmpty(w.Key))
            .ToDictionary(
                x => x.Key.Trim(),
                x => x.GroupBy(g => g.DealerId)
                    .ToDictionary(
                        x2 => x2.Key,
                        x2 => new JavaScriptSerializer().Serialize(
                            new Objects.XS.TurnoverByType_RB
                            {
                                YTD = x2.Sum(s => s.YTD),
                                YTDLY = x2.Sum(s => s.YTDLY),
                                YTDTarget = x2.Sum(s => s.YTDTarget)
                            })
                        ));
        }
    }
}
