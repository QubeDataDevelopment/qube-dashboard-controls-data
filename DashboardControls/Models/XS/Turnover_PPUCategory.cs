﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Models.XS
{
    public class Turnover_PPUCategory : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>(){
            { "ROM", new Objects.XS.Turnover_PPUCategory() },
            { "EXM", new Objects.XS.Turnover_PPUCategory() },
            { "MEC", new Objects.XS.Turnover_PPUCategory() },
            { "DAM", new Objects.XS.Turnover_PPUCategory() },
            { "TOT", new Objects.XS.Turnover_PPUCategory() }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() {
            { "Values", 1 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>> {
            { "English", new List<string>() { "Routine Maintenance", "Extended Maintenance", "Mechanical Repair", "Damage", "Total" } },
            { "German", new List<string>() { "Wartung", "Verschleiß", "Mechanik", "Karosserie", "Gesamt" } },
            { "French", new List<string>() { "Maintenance", "Usure", "Mécanique", "Carrosserie", "Total" } },
        };
        private List<string> Measures => new List<string>() { "ROM", "EXM", "MEC", "DAM", "TOT" };

        public Turnover_PPUCategory(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string TOSql = "SELECT \n"
                           + "	d.Id dealer_id,\n"
                           + "	SUM(case when level4_code = 'ROM' then total_this else 0 end) ROM,\n"
                           + "	SUM(case when level4_code = 'EXM' then total_this else 0 end) EXM,\n"
                           + "	SUM(case when level4_code = 'MEC' then total_this else 0 end) MEC,\n"
                           + "	SUM(case when level4_code = 'DAM' then total_this else 0 end) DAM,\n"
                           + "	coalesce(sum(total_this), 0) TOT\n"
                           + "from v_LiveDealers d\n"
                           + "left join (\n"
                           + "	select\n"
                           + "		dealer_id, \n"
                           + "		level4_code,\n"
                           + "		SUM(real_value) as total_this\n"
                           + "	from  RealBusiness  \n"
                           + "	where dataperiod_id between @From and @To\n"
                           + "		and level4_code in ('ROM','EXM','MEC','DAM')\n"
                           + "	group by dealer_id, level4_code\n"
                           + ") tuo on tuo.dealer_id = d.id\n"
                           + "group by d.id";

            string ParcSql = "select\n"
                            + "	d.id dealer_id,\n"
                            + "	sum(case when parc_year between 1 and 10 then parc else 0 end) ROM,\n"
                            + "	sum(case when parc_year between 3 and 10 then parc else 0 end) EXM,\n"
                            + "	sum(case when parc_year between 4 and 10 then parc else 0 end) MEC,\n"
                            + "	sum(case when parc_year between 1 and 6 then parc else 0 end) DAM,\n"
                            + "	sum(case when parc_year between 1 and 10 then parc else 0 end) TOT\n"
                            + "from v_LiveDealers d\n"
                            + "left join (\n"
                            + "	select\n"
                            + "		dealer_id, \n"
                            + "		parc_year,\n"
                            + "		dbo.dividetwonumbers(sum(parc_value),12) * (1+(@To-@From)) as parc\n"
                            + "	from v_CarParcTenYear t1\n"
                            + "	where finyear = @FinYear\n"
                            + "	group by dealer_id, parc_year\n"
                            + ") parc on parc.dealer_id = d.id\n"
                            + "group by d.id";

            Dictionary<int, Objects.XS.Turnover_PPUCategory> resO;

            resO = MSSQLHelper.GetDataTable(TOSql, new SqlParameter[] {
                new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id }
            }).AsEnumerable().Join(
                MSSQLHelper.GetDataTable(ParcSql, new SqlParameter[]
                {
                    new SqlParameter("@FinYear", SqlDbType.VarChar) { Value = Period.FinancialYear },
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id }
                }).AsEnumerable(),
                t1 => t1.Field<int>("dealer_id"),
                t2 => t2.Field<int>("dealer_id"),
                (t1, t2) => new { t1, t2 }
            ).GroupBy(g => g.t1.Field<int>("dealer_id"))
            .Select(s => new
            {
                dealer_id = s.Key,
                values = new Objects.XS.Turnover_PPUCategory()
                {
                    TO_ROM = s.Sum(su => su.t1.Field<decimal>("ROM")),
                    TO_EXM = s.Sum(su => su.t1.Field<decimal>("EXM")),
                    TO_MEC = s.Sum(su => su.t1.Field<decimal>("MEC")),
                    TO_DAM = s.Sum(su => su.t1.Field<decimal>("DAM")),
                    TO_TOT = s.Sum(su => su.t1.Field<decimal>("TOT")),
                    Parc_ROM = s.Sum(su => su.t2.Field<decimal>("ROM")),
                    Parc_EXM = s.Sum(su => su.t2.Field<decimal>("EXM")),
                    Parc_MEC = s.Sum(su => su.t2.Field<decimal>("MEC")),
                    Parc_DAM = s.Sum(su => su.t2.Field<decimal>("DAM")),
                    Parc_TOT = s.Sum(su => su.t2.Field<decimal>("TOT"))
                }
            }
            ).ToDictionary(x => x.dealer_id, x => x.values);

            res.Add("Values" + (lastYear ? "LY" : ""), resO.ToDictionary(x => x.Key, x => new JavaScriptSerializer().Serialize(x.Value)));

            return res;
        }
    }
}
