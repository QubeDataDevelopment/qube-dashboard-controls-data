﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class Turnover_PPUCategory : DashboardObject
    {
        public decimal TO_ROM { get; set; }
        public decimal TO_EXM { get; set; }
        public decimal TO_MEC { get; set; }
        public decimal TO_DAM { get; set; }
        public decimal TO_TOT { get; set; }
        public decimal Parc_ROM { get; set; }
        public decimal Parc_EXM { get; set; }
        public decimal Parc_MEC { get; set; }
        public decimal Parc_DAM { get; set; }
        public decimal Parc_TOT { get; set; }

        public decimal ROM { get; set; }
        public decimal EXM { get; set; }
        public decimal MEC { get; set; }
        public decimal DAM { get; set; }
        public decimal TOT { get; set; }
    }
}
