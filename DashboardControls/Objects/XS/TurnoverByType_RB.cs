﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class TurnoverByType_RB : DashboardObject
    {
        public string Name { get; set; }
        public decimal YTD { get; set; }
        public decimal YTDLY { get; set; }
        public decimal YTDChange { get; set; }
        public decimal YTDChangePerc { get; set; }
        public decimal YTDTarget { get; set; }
        public decimal YTDTargetChange { get; set; }
        public decimal YTDTargetChangePerc { get; set; }
    }
}
