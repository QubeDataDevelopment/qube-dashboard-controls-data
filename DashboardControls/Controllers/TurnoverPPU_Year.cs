﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Controllers
{
    public class TurnoverPPU_Year : Controller
    {
        public TurnoverPPU_Year(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.Turnover_PPUYear> res = new Dictionary<string, Objects.XS.Turnover_PPUYear>();

            if (!Data.ContainsKey("Values" + (lastYear ? "LY" : ""))) return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);

            Dictionary<int, Objects.XS.Turnover_PPUYear> resE = Data["Values" + (lastYear ? "LY" : "")].ToDictionary(x => x.Key, x => new JavaScriptSerializer().Deserialize<Objects.XS.Turnover_PPUYear>(x.Value));

            res.Add("Values" + (lastYear ? "LY" : ""), new Objects.XS.Turnover_PPUYear()
            {
                TO = resE.Sum(s => s.Value.TO),
                Parc = resE.Sum(s => s.Value.Parc),
                YTD = resE.Sum(s => s.Value.Parc) > 0 ? resE.Sum(s => s.Value.TO) / resE.Sum(s => s.Value.Parc) : 0m
            });

            return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            Dictionary<string, int> ranks = new Dictionary<string, int>();
            Dictionary<string, int> res = new Dictionary<string, int>();
            Dictionary<int, Objects.XS.Turnover_PPUYear> resE = Data["Values"].ToDictionary(x => x.Key, x => new JavaScriptSerializer().Deserialize<Objects.XS.Turnover_PPUYear>(x.Value));
            Dictionary<string, Objects.XS.Turnover_PPUYear> resS = selectionData.ToDictionary(x => x.Key, x => (Objects.XS.Turnover_PPUYear)x.Value);

            int count = resE.Count;
            decimal perQuartile = count / 4m;

            foreach (KeyValuePair<int, Objects.XS.Turnover_PPUYear> v in resE)
                v.Value.YTD = v.Value.Parc > 0 ? v.Value.TO / v.Value.Parc : 0m;

            ranks.Add("YTD", resE.Where(w => w.Value.YTD > resS["Values"].YTD).Count() + 1);
            ranks.Add("YTDLY", resE.Where(w => w.Value.YTD > resS["ValuesLY"].YTD).Count() + 1);
            ranks.Add("YTDZone", resE.Where(w => w.Value.YTD > resS["ValuesZone"].YTD).Count() + 1);
            ranks.Add("YTDNat", resE.Where(w => w.Value.YTD > resS["ValuesNat"].YTD).Count() + 1);

            res.Add("YTD", int.Parse((Math.Floor(decimal.Parse(ranks["YTD"].ToString()) / perQuartile) + 1).ToString()));
            res.Add("YTDLY", int.Parse((Math.Floor(decimal.Parse(ranks["YTDLY"].ToString()) / perQuartile) + 1).ToString()));
            res.Add("YTDZone", int.Parse((Math.Floor(decimal.Parse(ranks["YTDZone"].ToString()) / perQuartile) + 1).ToString()));
            res.Add("YTDNat", int.Parse((Math.Floor(decimal.Parse(ranks["YTDNat"].ToString()) / perQuartile) + 1).ToString()));

            return res;
        }
    }
}
