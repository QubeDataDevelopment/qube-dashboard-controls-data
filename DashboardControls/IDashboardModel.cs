﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public interface IDashboardModel<T>
    {
        Dictionary<string, Dictionary<int, string>> Values { get; set; }
        Dictionary<string, Dictionary<int, string>> ValuesLY { get; set; }
        Dictionary<string, Dictionary<int, string>> NationalValues { get; set; }
        Dictionary<string, Dictionary<int, string>> SelectionValues { get; set; }
        Dictionary<string, Dictionary<int, string>> SelectionValuesLY { get; set; }
        Dictionary<string, Dictionary<int, string>> ZoneValues { get; set; }
        Dictionary<string, T> Template { get; }
        Dictionary<string, int> Weights { get; }
        Dictionary<string, List<string>> Text { get; }
    }
}
