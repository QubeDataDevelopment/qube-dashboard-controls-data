﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QubeUtils;

namespace QubeDashboardControls
{
    public class PieChart : DashboardControl<decimal>
    {
        public PieChart(DashboardMeasure measure, string selectionLevel, string selectionId, DataPeriod period = null) : base(measure, selectionLevel, selectionId, period) { }

        protected override bool LastYear => false;
        protected override PeriodSetting PeriodSettings => new PeriodSetting();
        protected override bool AllowValidation => true;

        protected override Dictionary<string, decimal> CalculateData(ControlLevel level = ControlLevel.Default, bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (level == ControlLevel.National ? Model.NationalValues : (level == ControlLevel.Zone ? Model.ZoneValues : (lastYear ? Model.SelectionValuesLY : Model.SelectionValues)));

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return (Dictionary<string, decimal>) res.Execute(lastYear);
        }

        protected override Dictionary<string, Dictionary<int, decimal>> CalculateDealerData(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (lastYear ? Model.ValuesLY : Model.Values);

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return (Dictionary<string, Dictionary<int, decimal>>) res.ExecuteDealer(lastYear);
        }

        protected override Dictionary<string, int> GetQuartiles()
        {
            throw new NotImplementedException();
        }
    }
}
