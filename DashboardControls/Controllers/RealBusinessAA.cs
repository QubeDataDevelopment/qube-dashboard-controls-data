﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls.Controllers
{
    public class RealBusinessAA : Controller
    {
        public RealBusinessAA(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.RealBusinessAA> res = new Dictionary<string, Objects.XS.RealBusinessAA>();

            if (!Data.ContainsKey("Values" + (lastYear ? "LY" : ""))) return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);

            Dictionary<int, Objects.XS.RealBusinessAA> resE = Data["Values" + (lastYear ? "LY" : "")].ToDictionary(x => x.Key, x => new JavaScriptSerializer().Deserialize<Objects.XS.RealBusinessAA>(x.Value));

            res.Add("Values" + (lastYear ? "LY" : ""), new Objects.XS.RealBusinessAA() {
                Month1 = resE.SumNullable(s => s.Value.Month1),
                Month2 = resE.SumNullable(s => s.Value.Month2), 
                Month3 = resE.SumNullable(s => s.Value.Month3), 
                Month4 = resE.SumNullable(s => s.Value.Month4), 
                Month5 = resE.SumNullable(s => s.Value.Month5), 
                Month6 = resE.SumNullable(s => s.Value.Month6), 
                Month7 = resE.SumNullable(s => s.Value.Month7), 
                Month8 = resE.SumNullable(s => s.Value.Month8), 
                Month9 = resE.SumNullable(s => s.Value.Month9), 
                Month10 = resE.SumNullable(s => s.Value.Month10), 
                Month11 = resE.SumNullable(s => s.Value.Month11), 
                Month12 = resE.SumNullable(s => s.Value.Month12), 
                Month13 = resE.SumNullable(s => s.Value.Month13)
            });

            return res.ToDictionary(x => x.Key, x => (DashboardObject) x.Value);
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
