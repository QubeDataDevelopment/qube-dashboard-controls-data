﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Models.XS
{
    public class RealBusinessAA : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>() {
            { "Month1", new Objects.XS.Turnover_PPUMonth() },
            { "Month2", new Objects.XS.Turnover_PPUMonth() },
            { "Month3", new Objects.XS.Turnover_PPUMonth() },
            { "Month4", new Objects.XS.Turnover_PPUMonth() },
            { "Month5", new Objects.XS.Turnover_PPUMonth() },
            { "Month6", new Objects.XS.Turnover_PPUMonth() },
            { "Month7", new Objects.XS.Turnover_PPUMonth() },
            { "Month8", new Objects.XS.Turnover_PPUMonth() },
            { "Month9", new Objects.XS.Turnover_PPUMonth() },
            { "Month10", new Objects.XS.Turnover_PPUMonth() },
            { "Month11", new Objects.XS.Turnover_PPUMonth() },
            { "Month12", new Objects.XS.Turnover_PPUMonth() },
            { "Month13", new Objects.XS.Turnover_PPUMonth() }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() {
            { "Month1", 1 }, { "Month2", 2 }, { "Month3", 3 }, { "Month4", 4 }, { "Month5", 5 }, { "Month6", 6 }, { "Month7", 7 }, { "Month8", 8 }, { "Month9", 9 }, { "Month10", 10 }, { "Month11", 11 }, { "Month12", 12 }, { "Month13", 13 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>> {
            { "English", new List<string>() { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" } },
            { "German", new List<string>() {  "JAN", "FEB", "MÄR", "APR", "MAI", "JUN", "JUL", "AUG", "SEP", "OKT", "NOV", "DEZ" } },
            { "French", new List< string>() { "JANV", "FÉV", "MARS", "AVR", "MAI", "JUIN", "JUIL", "AOÛT", "SEPT", "OCT", "NOV", "DÉC" } },
        };

        private List<string> Measures => new List<string>() { "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR", "APR2" };

        public RealBusinessAA(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            if (lastYear)
                Period = new DataPeriod(Period.Id - 12);

            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string TOSql = @"
                select
	                d.id as dealer_id,
	                sum(case when (dataperiod_id-@PeriodId) = -12 then total_this else null end) Month1,
	                sum(case when (dataperiod_id-@PeriodId) = -11 then total_this else null end) Month2,
	                sum(case when (dataperiod_id-@PeriodId) = -10 then total_this else null end) Month3,
	                sum(case when (dataperiod_id-@PeriodId) = -9 then total_this else null end) Month4,
	                sum(case when (dataperiod_id-@PeriodId) = -8 then total_this else null end) Month5,
	                sum(case when (dataperiod_id-@PeriodId) = -7 then total_this else null end) Month6,
	                sum(case when (dataperiod_id-@PeriodId) = -6 then total_this else null end) Month7,
	                sum(case when (dataperiod_id-@PeriodId) = -5 then total_this else null end) Month8,
	                sum(case when (dataperiod_id-@PeriodId) = -4 then total_this else null end) Month9,
	                sum(case when (dataperiod_id-@PeriodId) = -3 then total_this else null end) Month10,
	                sum(case when (dataperiod_id-@PeriodId) = -2 then total_this else null end) Month11,
	                sum(case when (dataperiod_id-@PeriodId) = -1 then total_this else null end) Month12,
	                sum(case when (dataperiod_id-@PeriodId) = 0 then total_this else null end) Month13
                from v_LiveDealers d
                left join (
	                select
		                dealer_id,
		                dataperiod_id,
		                SUM(real_value) as total_this
	                from RealBusiness s
	                inner join dataperiods dp on dp.id = s.dataperiod_id
	                where dataperiod_id between @PeriodId-12 and @PeriodId
		                and level4_code in (" + GetLevel4Codes(Measure.Enum) + @")
	                group by dealer_id, dataperiod_id
                ) ppu on ppu.dealer_id = d.id
                group by d.id";

            Dictionary<int, Objects.XS.RealBusinessAA> resO;

            DataTable TO = MSSQLHelper.GetDataTable(TOSql, new SqlParameter[] {
                        new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
                    });

            resO = TO.AsEnumerable()
                    .Select(s => new
                    {
                        dealer_id = s.Field<int>("dealer_id"),
                        values = new Objects.XS.RealBusinessAA()
                        {
                            Month1 = s.Field<decimal?>("Month1"),
                            Month2 = s.Field<decimal?>("Month2"),
                            Month3 = s.Field<decimal?>("Month3"),
                            Month4 = s.Field<decimal?>("Month4"),
                            Month5 = s.Field<decimal?>("Month5"),
                            Month6 = s.Field<decimal?>("Month6"),
                            Month7 = s.Field<decimal?>("Month7"),
                            Month8 = s.Field<decimal?>("Month8"),
                            Month9 = s.Field<decimal?>("Month9"),
                            Month10 = s.Field<decimal?>("Month10"),
                            Month11 = s.Field<decimal?>("Month11"),
                            Month12 = s.Field<decimal?>("Month12"),
                            Month13 = s.Field<decimal?>("Month13"),
                        }
                    }
                    ).ToDictionary(x => x.dealer_id, x => x.values);

            res.Add("Values" + (lastYear ? "LY" : ""), resO.ToDictionary(x => x.Key, x => new JavaScriptSerializer().Serialize(x.Value)));

            return res;
        }

        private string GetLevel4Codes(DashboardMeasure measure)
        {
            switch (measure)
            {
                case DashboardMeasure.RealBusinessAA_RoutineMaintenance:
                    return "'ROM'";
                case DashboardMeasure.RealBusinessAA_ExtendedMaintenance:
                    return "'EXM'";
                case DashboardMeasure.RealBusinessAA_MechanicalRepair:
                    return "'MEC'";
                case DashboardMeasure.RealBusinessAA_Damage:
                    return "'DAM'";
                case DashboardMeasure.RealBusinessAA_Accessories:
                    return "'ACC'";
                case DashboardMeasure.RealBusinessAA_Total:
                    return "'ROM','EXM','MEC','DAM','ACC'";
                default:
                    throw new ArgumentOutOfRangeException("Invalid measure");
            }
        }
    }
}
