﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class TotalBonusByQuarter : DashboardObject
    {
        public string Name { get; set; }
        public int dealergroup_id { get; set; }
        public decimal? PB_Bonus { get; set; }
        public decimal? FG_Bonus { get; set; }
        public decimal? AV_Bonus { get; set; }
        public decimal? Total { get; set; }
    }
}
