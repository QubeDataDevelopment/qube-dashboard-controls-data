﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public class LineGraph : DashboardControl<decimal?>
    {
        public LineGraph(DashboardMeasure measure, string selectionLevel, string selectionId, DataPeriod period = null) : base(measure, selectionLevel, selectionId, period) { }

        protected override bool LastYear => false;

        protected override bool AllowValidation => true;

        protected override PeriodSetting PeriodSettings => new PeriodSetting();

        private List<string> Measures => new List<string>() { "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "JAN", "FEB", "MAR" };

        protected override Dictionary<string, decimal?> CalculateData(ControlLevel level = ControlLevel.Default, bool lastYear = false)
        {
            Dictionary<string, decimal?> res = new Dictionary<string, decimal?>();

            Dictionary<string, Dictionary<int, string>> data = (level == ControlLevel.National ? Model.NationalValues : (level == ControlLevel.Zone ? Model.ZoneValues : Model.SelectionValues));

            foreach (string measure in Measures)
            {
                if (!data.ContainsKey("TO_" + measure)) continue;

                decimal? TO = data["TO_" + measure].Sum(s => decimal.Parse(s.Value));
                decimal? Parc = data["Parc_" + measure].Sum(s => decimal.Parse(s.Value));

                res.Add(measure, (Parc > 0 ? TO / Parc : 0m));
            }

            return res;
        }

        protected override Dictionary<string, Dictionary<int, decimal?>> CalculateDealerData(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, decimal?>> res = new Dictionary<string, Dictionary<int, decimal?>>();

            Dictionary<string, Dictionary<int, string>> data = Model.Values;

            foreach (string measure in Measures)
            {
                res.Add("TO_" + measure, data["TO_" + measure].ToDictionary(x => x.Key, x => (decimal?)decimal.Parse(x.Value)));
                res.Add("Parc_" + measure, data["Parc_" + measure].ToDictionary(x => x.Key, x => (decimal?)decimal.Parse(x.Value)));
                res.Add(measure, data["TO_" + measure].ToDictionary(x => x.Key, x => (decimal?)decimal.Parse(x.Value))
                    .Join(data["Parc_" + measure].ToDictionary(x => x.Key, x => (decimal?)decimal.Parse(x.Value)), j1 => j1.Key, j2 => j2.Key, (j1, j2) => new { dealer_id = j1.Key, TO = j1.Value, Parc = j2.Value })
                    .ToDictionary(x => x.dealer_id, x => (x.Parc > 0 ? x.TO / x.Parc : 0m))
                );
            }

            return res;
        }
        protected override Dictionary<string, int> GetQuartiles()
        {
            throw new NotImplementedException();
        }
    }
}
