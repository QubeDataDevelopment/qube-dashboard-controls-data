﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls.Models.PPM_AT
{
    public class BonusSummary : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>{
            { "Q1", null },
            { "Q2", null },
            { "Q3", null },
            { "Q4", null },
            { "YTD", null }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>()
        {
            { "Q1", 1 },
            { "Q2", 2 },
            { "Q3", 3 },
            { "Q4", 4 },
            { "YTD", 5 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>> {
            { "English", new List<string> { "Apr-Jun", "Jul-Sep", "Oct-Dec", "Jan-Mar", "YTD" } },
            { "German", new List<string> { "Apr-Jun", "Jul-Sep", "Okt-Dez", "Jan-Mär", "YTD" } },
            { "French", new List<string> { "Avr-Juin", "Juil-Sept", "Oct-Déc", "Jan-Mars", "YTD" } }
        };

        public BonusSummary(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            

            string sql =
				@"SELECT
					dp.id PeriodId,
					dp.FinancialYear,
					dp.FinancialQuarter,
					dp.FinancialMonth,
					dp.StartDate,
					dp.EndDate,
					bi.productgroup,
					bi.control_id,
					bi.bonustype,
					bi.quarter_id,
					bi.basketname,
					bi.CumulativeBonus,
					dg.DealerGroup_ID
				INTO #Periods
				from DataPeriods dp
				left join (
					select i.*, c.bonustype, c.quarter_id, c.basketname, c.CumulativeBonus, c.period_from, c.period_to from BonusBasketItems i
					INNER join (select id, bonustype, quarter_id, basketname, CumulativeBonus, period_from, period_to from BonusBasketControl WHERE period_from BETWEEN @StartPeriod and @FYEndPeriod) c on c.id = i.control_id
				) bi ON (dp.FinancialQuarter = bi.quarter_id OR bi.CumulativeBonus = 1) AND dp.id BETWEEN bi.period_from and bi.period_to
				LEFT join dbo.CurrentDealerGroupSelectionTableForBonus('M', '6', 6) dg on 1=1
				WHERE dp.id BETWEEN @StartPeriod AND @EndPeriod

				DELETE FROM #Periods WHERE basketname = 'Accessories NCS Basket' AND PeriodId > (SELECT MAX(dataperiod_id) FROM dbo.VehicleSales_Summary)

				SELECT 
					DISTINCT
					PeriodId,
					FinancialYear,
					FinancialQuarter,
					FinancialMonth,
					StartDate,
					EndDate,
					control_id,
					bonustype,
					quarter_id,
					basketname,
					CumulativeBonus,
					DealerGroup_ID
				INTO #DistinctPeriods
				FROM #Periods

				DECLARE @MaxDate DATE = (SELECT MAX(Purchase_date) FROM dbo.Web_ValidDealerPurchases)

				SELECT bc.id control_id, p.DealerGroup_Id, FLOOR(p.Parc) Parc INTO #parc FROM dbo.BonusBasketControl bc INNER JOIN dbo.DataPeriods dpf ON dpf.id = bc.period_from INNER JOIN dbo.DataPeriods dpt ON dpt.id = bc.period_to CROSS APPLY dbo.GetWorkingDayParcByDealerGroupFromDate(dpf.StartDate, CASE WHEN @MaxDate < dpt.EndDate THEN @MaxDate ELSE dpt.EndDate end) p

				SELECT
					d.id dealer_id,
					d.DealerGroup_Id,
					Bonus_Q1_PartsBasket, Bonus_Q2_PartsBasket, Bonus_Q3_PartsBasket, Bonus_Q4_PartsBasket,
					Bonus_Q1_AccessoriesNCSBasket, Bonus_Q2_AccessoriesNCSBasket, Bonus_Q3_AccessoriesNCSBasket, Bonus_Q4_AccessoriesNCSBasket,
					Bonus_Q1_AccessoriesUIOBasket, Bonus_Q2_AccessoriesUIOBasket, Bonus_Q3_AccessoriesUIOBasket, Bonus_Q4_AccessoriesUIOBasket,
					Bonus_Q1_WinterTyresBasket, Bonus_Q2_WinterTyresBasket, Bonus_Q3_WinterTyresBasket, Bonus_Q4_WinterTyresBasket
				from v_LiveDealers d
				left join (
					select
						p.DealerGroup_Id,
						Bonus_Q1_PartsBasket, Bonus_Q2_PartsBasket, Bonus_Q3_PartsBasket, Bonus_Q4_PartsBasket,
						Bonus_Q1_AccessoriesNCSBasket, Bonus_Q2_AccessoriesNCSBasket, Bonus_Q3_AccessoriesNCSBasket, Bonus_Q4_AccessoriesNCSBasket,
						Bonus_Q1_AccessoriesUIOBasket, Bonus_Q2_AccessoriesUIOBasket, Bonus_Q3_AccessoriesUIOBasket, Bonus_Q4_AccessoriesUIOBasket,
						Bonus_Q1_WinterTyresBasket, Bonus_Q2_WinterTyresBasket, Bonus_Q3_WinterTyresBasket, Bonus_Q4_WinterTyresBasket
					from (
						select
							DealerGroup_Id,
							col + '_Q' + cast(FinancialQuarter as varchar(1)) + '_' + REPLACE(u.basketname, ' ', '') col,
							value
						from (
							select
								q.DealerGroup_Id,
								q.FinancialQuarter,
								cast(q.BonusCost * (b.band_bonuspc / 100.0) as money) Bonus,
								q.basketname
							from (
								SELECT
									q.DealerGroup_ID,
									q.FinancialQuarter,
									CASE WHEN q.basketname = 'Accessories NCS Basket' THEN dbo.DivideTwoNumbersAsFloat(q.BonusCost, ISNULL(vs.vehicles_sold, 0)) WHEN q.basketname = 'Accessories UIO Basket' THEN dbo.DivideTwoNumbersAsFloat(q.BonusCost, ISNULL(p.Parc, 0)) else q.BonusMeasure END BonusMeasure,
									q.BonusCost,
									q.basketname,
									q.control_id
								FROM (
									SELECT
										DISTINCT
										q.DealerGroup_Id,
										q.FinancialQuarter,
										SUM(CASE WHEN q.bonustype = 'units' THEN ISNULL(p.Quantity, 0) ELSE ISNULL(p.DealerCost, 0) end) OVER(PARTITION BY q.DealerGroup_ID, (CASE WHEN q.CumulativeBonus = 1 THEN NULL ELSE q.FinancialQuarter end), q.basketname ORDER BY q.FinancialQuarter) BonusMeasure,
										SUM(ISNULL(p.DealerCost, 0)) OVER(PARTITION BY q.DealerGroup_ID, (CASE WHEN q.CumulativeBonus = 1 THEN NULL ELSE q.FinancialQuarter end), q.basketname ORDER BY q.FinancialQuarter) BonusCost,
										q.basketname,
										q.control_id
									FROM #Periods q
									LEFT JOIN dbo.Web_ValidDealerPurchases p ON p.DataPeriod_Id = q.PeriodId AND p.Level2_Code = q.productgroup AND p.DealerGroup_Id = q.DealerGroup_ID
								) q
								LEFT JOIN (SELECT d.dealergroup_id, p.FinancialQuarter, p.control_id, SUM(vs.vehicles_sold) vehicles_sold FROM dbo.VehicleSales_Summary vs INNER JOIN dbo.v_LiveDealers d ON d.id = vs.dealer_id INNER JOIN dbo.v_LiveDealerGroups dg ON dg.id = d.dealergroup_id INNER JOIN #DistinctPeriods p ON p.DealerGroup_ID = d.dealergroup_id AND p.PeriodId = vs.dataperiod_id WHERE vs.retaildate <= @MaxDate GROUP BY d.dealergroup_id, p.FinancialQuarter, p.control_id) vs ON vs.dealergroup_id = q.DealerGroup_ID AND vs.FinancialQuarter = q.FinancialQuarter AND vs.control_id = q.control_id
								LEFT JOIN #parc p ON p.control_id = q.control_id AND p.DealerGroup_Id = q.DealerGroup_ID
								WHERE q.DealerGroup_ID <> 0
							) q
							inner join dbo.BonusBasketPaymentBands b on b.control_id = q.control_id and q.BonusMeasure between b.band_min and b.band_max
						) q
						unpivot (
							value for col in (Bonus)
						) u
					) q
					pivot (
						sum(value) for col in (
							Bonus_Q1_PartsBasket, Bonus_Q2_PartsBasket, Bonus_Q3_PartsBasket, Bonus_Q4_PartsBasket,
							Bonus_Q1_AccessoriesNCSBasket, Bonus_Q2_AccessoriesNCSBasket, Bonus_Q3_AccessoriesNCSBasket, Bonus_Q4_AccessoriesNCSBasket,
							Bonus_Q1_AccessoriesUIOBasket, Bonus_Q2_AccessoriesUIOBasket, Bonus_Q3_AccessoriesUIOBasket, Bonus_Q4_AccessoriesUIOBasket,
							Bonus_Q1_WinterTyresBasket, Bonus_Q2_WinterTyresBasket, Bonus_Q3_WinterTyresBasket, Bonus_Q4_WinterTyresBasket
						)
					) p
				) q on q.DealerGroup_Id = d.dealergroup_id
				order by dealergroup_id

				DROP TABLE #DistinctPeriods
				DROP TABLE #Periods
				DROP TABLE #parc";

            return new Dictionary<string, Dictionary<int, string>>() {
                {
                    "Values",
                    MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                        new SqlParameter("@StartPeriod", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                        new SqlParameter("@EndPeriod", SqlDbType.Int) { Value = Period.Id },
                        new SqlParameter("@FYEndPeriod", SqlDbType.Int) { Value = Period.FYearEndPeriod }
                    }).AsEnumerable().Select(s => new {
                        dealer_id = s.Field<int>("dealer_id"),
                        values = new Objects.PPM_AT.BonusSummary_Process()
                        {
                            dealergroup_id = s.Field<int>("dealergroup_id"),
                            Bonus_Q1_PartsBasket = s.Field<decimal?>("Bonus_Q1_PartsBasket"),
                            Bonus_Q1_AccessoriesBasket = s.Field<decimal?>("Bonus_Q1_AccessoriesNCSBasket"),
                            Bonus_Q1_LifeCycleBasket = s.Field<decimal?>("Bonus_Q1_AccessoriesUIOBasket"),
                            Bonus_Q1_WinterTyresBasket = s.Field<decimal?>("Bonus_Q1_WinterTyresBasket"),
                            Bonus_Q2_PartsBasket = s.Field<decimal?>("Bonus_Q2_PartsBasket"),
                            Bonus_Q2_AccessoriesBasket = s.Field<decimal?>("Bonus_Q2_AccessoriesNCSBasket"),
                            Bonus_Q2_LifeCycleBasket = s.Field<decimal?>("Bonus_Q2_AccessoriesUIOBasket"),
                            Bonus_Q2_WinterTyresBasket = s.Field<decimal?>("Bonus_Q2_WinterTyresBasket"),
                            Bonus_Q3_PartsBasket = s.Field<decimal?>("Bonus_Q3_PartsBasket"),
                            Bonus_Q3_AccessoriesBasket = s.Field<decimal?>("Bonus_Q3_AccessoriesNCSBasket"),
                            Bonus_Q3_LifeCycleBasket = s.Field<decimal?>("Bonus_Q3_AccessoriesUIOBasket"),
                            Bonus_Q3_WinterTyresBasket = s.Field<decimal?>("Bonus_Q3_WinterTyresBasket"),
                            Bonus_Q4_PartsBasket = s.Field<decimal?>("Bonus_Q4_PartsBasket"),
                            Bonus_Q4_AccessoriesBasket = s.Field<decimal?>("Bonus_Q4_AccessoriesNCSBasket"),
                            Bonus_Q4_LifeCycleBasket = s.Field<decimal?>("Bonus_Q4_AccessoriesUIOBasket"),
                            Bonus_Q4_WinterTyresBasket = s.Field<decimal?>("Bonus_Q4_WinterTyresBasket")
                        }
                    }).ToDictionary(x => x.dealer_id, x => new JavaScriptSerializer().Serialize(x.values))
                }
            };
        }
    }
}
