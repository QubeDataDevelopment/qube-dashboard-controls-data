﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Controllers
{
    public class TurnoverByTypeFB : Controller
    {
        public TurnoverByTypeFB(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override object Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.TurnoverByType_FB> res = new Dictionary<string, Objects.XS.TurnoverByType_FB>();

            Dictionary<string, Dictionary<int, Objects.XS.TurnoverByType_FB>> resH = new Dictionary<string, Dictionary<int, Objects.XS.TurnoverByType_FB>>();

            resH = Data.ToDictionary(x => x.Key, x => x.Value.ToDictionary(x2 => x2.Key, x2 => new JavaScriptSerializer().Deserialize<Objects.XS.TurnoverByType_FB>(x2.Value)));

            foreach (KeyValuePair<string, Dictionary<int, Objects.XS.TurnoverByType_FB>> g in resH)
                res.Add(g.Key + (lastYear ? "LY" : ""), new Objects.XS.TurnoverByType_FB() { Name = g.Key, MTD = g.Value.Sum(s => s.Value.MTD), MTDLY = g.Value.Sum(s => s.Value.MTDLY), YTD = g.Value.Sum(s => s.Value.YTD), YTDLY = g.Value.Sum(s => s.Value.YTDLY) });

            res.Add("TOTE" + (lastYear ? "LY" : ""), new Objects.XS.TurnoverByType_FB()
            {
                MTD = res.Where(w => w.Key != "WRW").Sum(s => s.Value.MTD),
                MTDLY = res.Where(w => w.Key != "WRW").Sum(s => s.Value.MTDLY),
                YTD = res.Where(w => w.Key != "WRW").Sum(s => s.Value.YTD),
                YTDLY = res.Where(w => w.Key != "WRW").Sum(s => s.Value.YTDLY),
            });

            res.Add("TOT" + (lastYear ? "LY" : ""), new Objects.XS.TurnoverByType_FB()
            {
                MTD = res.Where(w => w.Key != "TOTE").Sum(s => s.Value.MTD),
                MTDLY = res.Where(w => w.Key != "TOTE").Sum(s => s.Value.MTDLY),
                YTD = res.Where(w => w.Key != "TOTE").Sum(s => s.Value.YTD),
                YTDLY = res.Where(w => w.Key != "TOTE").Sum(s => s.Value.YTDLY),
            });

            foreach (KeyValuePair<string, Objects.XS.TurnoverByType_FB> o in res)
            {
                o.Value.MTDChange = o.Value.MTD - o.Value.MTDLY;
                o.Value.MTDChangePerc = o.Value.MTDChange / (o.Value.MTDLY == 0m ? 100m : o.Value.MTDLY);
                o.Value.YTDChange = o.Value.YTD - o.Value.YTDLY;
                o.Value.YTDChangePerc = o.Value.YTDChange / (o.Value.YTDLY == 0m ? 100m : o.Value.YTDLY);
            }

            return res.ToDictionary(x => x.Key, x => (DashboardObject) x.Value);
        }

        public override object ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
