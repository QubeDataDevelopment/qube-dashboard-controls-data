﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Controllers
{
    public class Turnover_PPUMonth : Controller
    {
        public Turnover_PPUMonth(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.Turnover_PPUMonth> res = new Dictionary<string, Objects.XS.Turnover_PPUMonth>();

            if (!Data.ContainsKey("Values" + (lastYear ? "LY" : ""))) return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);

            Dictionary<int, Objects.XS.Turnover_PPUMonth> resE = Data["Values" + (lastYear ? "LY" : "")].ToDictionary(x => x.Key, x => new JavaScriptSerializer().Deserialize<Objects.XS.Turnover_PPUMonth>(x.Value));

            res.Add("Values" + (lastYear ? "LY" : ""), new Objects.XS.Turnover_PPUMonth() {
                TO_APR = resE.Sum(s => s.Value.TO_APR),
                TO_MAY = resE.Sum(s => s.Value.TO_MAY),
                TO_JUN = resE.Sum(s => s.Value.TO_JUN),
                TO_JUL = resE.Sum(s => s.Value.TO_JUL),
                TO_AUG = resE.Sum(s => s.Value.TO_AUG),
                TO_SEP = resE.Sum(s => s.Value.TO_SEP),
                TO_OCT = resE.Sum(s => s.Value.TO_OCT),
                TO_NOV = resE.Sum(s => s.Value.TO_NOV),
                TO_DEC = resE.Sum(s => s.Value.TO_DEC),
                TO_JAN = resE.Sum(s => s.Value.TO_JAN),
                TO_FEB = resE.Sum(s => s.Value.TO_FEB),
                TO_MAR = resE.Sum(s => s.Value.TO_MAR),
                Parc_APR = resE.Sum(s => s.Value.Parc_APR),
                Parc_MAY = resE.Sum(s => s.Value.Parc_MAY),
                Parc_JUN = resE.Sum(s => s.Value.Parc_JUN),
                Parc_JUL = resE.Sum(s => s.Value.Parc_JUL),
                Parc_AUG = resE.Sum(s => s.Value.Parc_AUG),
                Parc_SEP = resE.Sum(s => s.Value.Parc_SEP),
                Parc_OCT = resE.Sum(s => s.Value.Parc_OCT),
                Parc_NOV = resE.Sum(s => s.Value.Parc_NOV),
                Parc_DEC = resE.Sum(s => s.Value.Parc_DEC),
                Parc_JAN = resE.Sum(s => s.Value.Parc_JAN),
                Parc_FEB = resE.Sum(s => s.Value.Parc_FEB),
                Parc_MAR = resE.Sum(s => s.Value.Parc_MAR),
                APR = resE.Sum(s => s.Value.Parc_APR) > 0 ? resE.Sum(s => s.Value.TO_APR) / resE.Sum(s => s.Value.Parc_APR) : 0m,
                MAY = resE.Sum(s => s.Value.Parc_MAY) > 0 ? resE.Sum(s => s.Value.TO_MAY) / resE.Sum(s => s.Value.Parc_MAY) : 0m,
                JUN = resE.Sum(s => s.Value.Parc_JUN) > 0 ? resE.Sum(s => s.Value.TO_JUN) / resE.Sum(s => s.Value.Parc_JUN) : 0m,
                JUL = resE.Sum(s => s.Value.Parc_JUL) > 0 ? resE.Sum(s => s.Value.TO_JUL) / resE.Sum(s => s.Value.Parc_JUL) : 0m,
                AUG = resE.Sum(s => s.Value.Parc_AUG) > 0 ? resE.Sum(s => s.Value.TO_AUG) / resE.Sum(s => s.Value.Parc_AUG) : 0m,
                SEP = resE.Sum(s => s.Value.Parc_SEP) > 0 ? resE.Sum(s => s.Value.TO_SEP) / resE.Sum(s => s.Value.Parc_SEP) : 0m,
                OCT = resE.Sum(s => s.Value.Parc_OCT) > 0 ? resE.Sum(s => s.Value.TO_OCT) / resE.Sum(s => s.Value.Parc_OCT) : 0m,
                NOV = resE.Sum(s => s.Value.Parc_NOV) > 0 ? resE.Sum(s => s.Value.TO_NOV) / resE.Sum(s => s.Value.Parc_NOV) : 0m,
                DEC = resE.Sum(s => s.Value.Parc_DEC) > 0 ? resE.Sum(s => s.Value.TO_DEC) / resE.Sum(s => s.Value.Parc_DEC) : 0m,
                JAN = resE.Sum(s => s.Value.Parc_JAN) > 0 ? resE.Sum(s => s.Value.TO_JAN) / resE.Sum(s => s.Value.Parc_JAN) : 0m,
                FEB = resE.Sum(s => s.Value.Parc_FEB) > 0 ? resE.Sum(s => s.Value.TO_FEB) / resE.Sum(s => s.Value.Parc_FEB) : 0m,
                MAR = resE.Sum(s => s.Value.Parc_MAR) > 0 ? resE.Sum(s => s.Value.TO_MAR) / resE.Sum(s => s.Value.Parc_MAR) : 0m,
            });

            return res.ToDictionary(x => x.Key, x => (DashboardObject) x.Value);
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
