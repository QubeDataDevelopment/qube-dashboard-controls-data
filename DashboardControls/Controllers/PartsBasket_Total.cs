﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QubeUtils;

namespace QubeDashboardControls.Controllers
{
    public class PartsBasket_Total : Controller
    {
        public PartsBasket_Total(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, decimal> res = new Dictionary<string, decimal>();
            if (!Data.ContainsKey("dealergroup" + (lastYear ? "LY" : ""))) return res;

            CultureInfo en = new CultureInfo("en-GB");

            List<Objects.XS.GenericDealerGroup> resE =
                Data.SelectMany(sm => sm.Value.Select(s => new { key = sm.Key, dealercode = s.Key, value = s.Value }))
                .GroupBy(g => g.dealercode)
                .ToDictionary(x => x.Key, x =>
                    new Objects.XS.GenericDealerGroup
                    {
                        Dealergroup_id = int.Parse(x.Where(w => w.key == "dealergroup").First().value),
                        Sales = decimal.Parse(x.Where(w => w.key == "sales").First().value, en.NumberFormat),
                        Target = decimal.Parse(x.Where(w => w.key == "target").First().value, en.NumberFormat),
                        Value = decimal.Parse(x.Where(w => w.key == "value").First().value, en.NumberFormat)
                    }
                ).DistinctBy(d => d.Value.Dealergroup_id).Select(s => s.Value).ToList();

            decimal Perf = resE.Sum(s => s.Target) > 0 ? (resE.Sum(s => s.Sales) / resE.Sum(s => s.Target)) * 100 : 0m;
            decimal Value = resE.Sum(s => s.Value);

            res.Add("Performance" + (lastYear ? "LY" : ""), Perf);
            res.Add("Value" + (lastYear ? "LY" : ""), Value);

            return res;
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            return Data.ToDictionary(x => x.Key, x => x.Value.ToDictionary(x2 => x2.Key, x2 => decimal.Parse(x2.Value)));
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
