﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls.Controllers
{
    public class AccessoriesBasket : Controller
    {
        public AccessoriesBasket(DashboardMeasure measure, DataPeriod period, Dictionary<string, Dictionary<int, string>> data) : base(measure, period, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, decimal> res = new Dictionary<string, decimal>();
            if (!Data.ContainsKey("dealergroup" + (lastYear ? "LY" : ""))) return res;

            var resE =
                Data.SelectMany(sm => sm.Value.Select(s => new { key = sm.Key, dealercode = s.Key, value = s.Value }))
                .GroupBy(g => g.dealercode)
                .ToDictionary(x => x.Key, x =>
                    new
                    {
                        Dealergroup_id = int.Parse(x.Where(w => w.key == "dealergroup").First().value),
                        ThisYear = decimal.Parse(x.Where(w => w.key == "ThisYear").First().value),
                        LastYear = decimal.Parse(x.Where(w => w.key == "LastYear").First().value),
                        MaxDate = decimal.Parse(x.Where(w => w.key == "MaxDate").First().value),
                        ThisQuarter = decimal.Parse(x.Where(W => W.key == "ThisQuarter").First().value),
                        ThisYearNCS = decimal.Parse(x.Where(W => W.key == "ThisYearNCS").First().value),
                        Ratio = decimal.Parse(x.Where(W => W.key == "Ratio").First().value)
                    }
                ).DistinctBy(d => d.Value.Dealergroup_id).Select(s => s.Value).ToList();

            decimal ThisYear = resE.Sum(s => s.ThisYear);
            decimal LastYear = resE.Sum(s => s.LastYear);
            decimal ThisQuarter = resE.Sum(s => s.ThisQuarter);
            DateTime MaxDate = resE.Max(s => DateTime.ParseExact(s.MaxDate.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture));
            decimal ThisYearNCS = resE.Sum(s => s.ThisYearNCS);
            decimal Ratio = resE.Sum(s => s.Ratio);

            //string workingDaysSql = "SELECT SUM(WorkingDay_Flag) FROM WorkingDays WHERE FullDate BETWEEN @StartDate AND @EndDate";

            //int workingDaysFull = MSSQLHelper.ExecuteScalar<int>(workingDaysSql, new SqlParameter[] {
            //                            new SqlParameter("@StartDate", SqlDbType.Date) { Value = Period.FYearStartDate },
            //                            new SqlParameter("@EndDate", SqlDbType.Date) { Value = Period.FYearEndDate }
            //                        });
            //int workingDaysSoFar = MSSQLHelper.ExecuteScalar<int>(workingDaysSql, new SqlParameter[] {
            //                            new SqlParameter("@StartDate", SqlDbType.Date) { Value = Period.FYearStartDate },
            //                            new SqlParameter("@EndDate", SqlDbType.Date) { Value = MaxDate }
            //                        });

            decimal bonusPerc = MSSQLHelper.ExecuteScalar<decimal>("select isnull((select band_bonuspc from BonusBasketPaymentBands where @Ratio between band_min and band_max and control_id = (select top 1 id from bonusbasketcontrol where @PeriodId between period_from and period_to and basketname = 'Accessories NCS Basket')), 0)", new SqlParameter[] {
                                    new SqlParameter("@Ratio", SqlDbType.Money) { Value = Ratio },
                                    new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
                                });
            decimal nextBandMin = MSSQLHelper.ExecuteScalar<decimal>("select isnull(min(band_min), 0) from BonusBasketPaymentBands where control_id = (select top 1 id from bonusbasketcontrol where @PeriodId between period_from and period_to and basketname = 'Accessories NCS Basket') and band_min > @Ratio", new SqlParameter[] {
                                        new SqlParameter("@Ratio", SqlDbType.Money) { Value = Ratio },
                                        new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
                                    });

            decimal needed = nextBandMin > 0 ? nextBandMin - Ratio : 0m;

            res.Add("Performance" + (lastYear ? "LY" : ""), bonusPerc);
            res.Add("Value" + (lastYear ? "LY" : ""), needed > 0 ? needed : 0m);
            res.Add("YTD" + (lastYear ? "LY" : ""), ThisQuarter);

            return res;
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {          
            return Data.ToDictionary(x => x.Key, x => x.Value.ToDictionary(x2 => x2.Key, x2 => decimal.Parse(x2.Value)));
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
