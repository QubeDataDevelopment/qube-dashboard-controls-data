﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public class PeriodSetting
    {
        public bool LastYear { get; set; }
        public bool Zone { get; set; }
        public bool National { get; set; }
    }
}
