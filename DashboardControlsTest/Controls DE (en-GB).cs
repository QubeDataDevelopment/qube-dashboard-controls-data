﻿using System;
using System.Globalization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QubeDashboardControls;
using QubeUtils;

namespace DashboardControlsTest
{
    [TestClass]
    public class ControlsDE_enGB
    {
        [TestMethod]
        public void RealBusiness()
        {
            DataPeriod period = new DataPeriod(152);

            DataPill pill1 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "5");
            DataPill pill2 = new DataPill(DashboardMeasure.RealBusiness_ExtendedMaintenance, "M", "5");
            DataPill pill3 = new DataPill(DashboardMeasure.RealBusiness_MechanicalRepair, "M", "5");
            DataPill pill4 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "5");
            DataPill pill5 = new DataPill(DashboardMeasure.RealBusiness_Damage, "M", "5");
            DataPill pill6 = new DataPill(DashboardMeasure.RealBusiness_Total, "M", "5");
        }

        [TestMethod]
        public void PPU_Month()
        {
            LineGraph ppuMonth = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "5");
        }

        [TestMethod]
        public void PPU_Category()
        {
            BarGraph ppuCat = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "5");
        }

        [TestMethod]
        public void StockVsEmergency()
        {
            DataPeriod period = new DataPeriod(153);

            Dial dial1 = new Dial(DashboardMeasure.StockEmergency_RoutineMaintenance, "M", "5");
            Dial dial2 = new Dial(DashboardMeasure.StockEmergency_ExtendedMaintenance, "M", "5");
            Dial dial3 = new Dial(DashboardMeasure.StockEmergency_MechanicalRepair, "M", "5");
            Dial dial4 = new Dial(DashboardMeasure.StockEmergency_Damage, "M", "5");
            Dial dial5 = new Dial(DashboardMeasure.StockEmergency_Accessories, "M", "5");
            Dial dial6 = new Dial(DashboardMeasure.StockEmergency_Total, "M", "5");
        }

        [TestMethod]
        public void TurnoverByType_FB()
        {
            DataPeriod period = new DataPeriod(157);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "5", period);
        }

        [TestMethod]
        public void TurnoverByType_RB()
        {
            DataPeriod period = new DataPeriod(154);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "5");
        }

        [TestMethod]
        public void TurnoverByYear()
        {
            DataPeriod period = new DataPeriod(154);

            BarGraph barGraph1 = new BarGraph(DashboardMeasure.Turnover_PPUYear, "R", "1", period);
        }

        [TestMethod]
        public void PartsBasket_Seasonal()
        {
            PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Seasonal, "M", "5", new DataPeriod(153));
        }

        [TestMethod]
        public void PartsBasket_Total()
        {
            //PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Total, "M", "5", new DataPeriod(157));

            Dial dial1 = new Dial(DashboardMeasure.PartsBasket_Total, "M", "5", new DataPeriod(153));
        }

        [TestMethod]
        public void PartsBasket()
        {
            DataPeriod period = new DataPeriod(156);

            PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Seasonal, "M", "5", period);
            PieChart pieChart2 = new PieChart(DashboardMeasure.PartsBasket_Total, "M", "5", period);
        }

        [TestMethod]
        public void RealBusinessAA()
        {
            DataPeriod period = new DataPeriod(157);

            BarGraph lineGraph1 = new BarGraph(DashboardMeasure.RealBusinessAA_Accessories, "M", "5", period);
            BarGraph lineGraph2 = new BarGraph(DashboardMeasure.RealBusinessAA_Damage, "M", "5", period);
            BarGraph lineGraph3 = new BarGraph(DashboardMeasure.RealBusinessAA_ExtendedMaintenance, "M", "5", period);
            BarGraph lineGraph4 = new BarGraph(DashboardMeasure.RealBusinessAA_MechanicalRepair, "M", "5", period);
            BarGraph lineGraph5 = new BarGraph(DashboardMeasure.RealBusinessAA_RoutineMaintenance, "M", "5", period);
            BarGraph lineGraph6 = new BarGraph(DashboardMeasure.RealBusinessAA_Total, "M", "5", period);
        }

        [TestMethod]
        public void AccessoriesBySales()
        {
            BarGraph barGraph1 = new BarGraph(DashboardMeasure.AccessoriesBySales, "M", "5", new DataPeriod(156));
        }

        [TestMethod]
        public void TurnoverByType()
        {
            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "5", new DataPeriod(170));
            DataGrid grid2 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "5");
        }

        [TestMethod]
        public void Total_MainDashboard()
        {
            DataPeriod period = new DataPeriod(157);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "5", period);
            DataGrid grid2 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "5", period);

            BarGraph barGraph1 = new BarGraph(DashboardMeasure.RealBusinessAA_Accessories, "M", "5", period);
            BarGraph barGraph2 = new BarGraph(DashboardMeasure.RealBusinessAA_Damage, "M", "5", period);
            BarGraph barGraph3 = new BarGraph(DashboardMeasure.RealBusinessAA_ExtendedMaintenance, "M", "5", period);
            BarGraph barGraph4 = new BarGraph(DashboardMeasure.RealBusinessAA_MechanicalRepair, "M", "5", period);
            BarGraph barGraph5 = new BarGraph(DashboardMeasure.RealBusinessAA_RoutineMaintenance, "M", "5", period);
            BarGraph barGraph6 = new BarGraph(DashboardMeasure.RealBusinessAA_Total, "M", "5", period);

            PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Seasonal, "M", "5", period);
            PieChart pieChart2 = new PieChart(DashboardMeasure.PartsBasket_Total, "M", "5", period);

            DataGrid grid3 = new DataGrid(DashboardMeasure.FocusGroupBonus, "M", "5", period);
            DataGrid grid4 = new DataGrid(DashboardMeasure.TotalBonusByQuarter, "M", "5", period);

            BarGraph barGraph7 = new BarGraph(DashboardMeasure.Turnover_PPUYear, "M", "5", period);
            BarGraph barGraph8 = new BarGraph(DashboardMeasure.AccessoriesBySales, "M", "5", period);

            DashboardControl<decimal> gridAV = new DashboardControl<decimal>(DashboardMeasure.VolumeBonus, "M", "5", period);
        }

        [TestMethod]
        public void EquipmentRate()
        {
            Dial dial7 = new Dial(DashboardMeasure.EquipmentRate_OilFilters, "M", "5");
            Dial dial8 = new Dial(DashboardMeasure.EquipmentRate_AirFilters, "M", "5");
            Dial dial9 = new Dial(DashboardMeasure.EquipmentRate_PollenFilters, "M", "5");
            Dial dial10 = new Dial(DashboardMeasure.EquipmentRate_FuelFilters, "M", "5");
            Dial dial11 = new Dial(DashboardMeasure.EquipmentRate_SparkPlugs, "M", "5");
            Dial dial12 = new Dial(DashboardMeasure.EquipmentRate_WiperBlades, "M", "5");
            Dial dial13 = new Dial(DashboardMeasure.EquipmentRate_BrakePads, "M", "5");
            Dial dial14 = new Dial(DashboardMeasure.EquipmentRate_BrakeDiscs, "M", "5");
            Dial dial15 = new Dial(DashboardMeasure.EquipmentRate_TimingBelts, "M", "5");
            Dial dial16 = new Dial(DashboardMeasure.EquipmentRate_Batteries, "M", "5");
        }

        //[TestMethod]
        //public void BadMeasure()
        //{
        //    DataPill pill1 = new DataPill("BAD", "M", "5");
        //}

        [TestMethod]
        public void Total()
        {
            DataPill pill1 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "5");
            DataPill pill2 = new DataPill(DashboardMeasure.RealBusiness_ExtendedMaintenance, "M", "5");
            DataPill pill3 = new DataPill(DashboardMeasure.RealBusiness_MechanicalRepair, "M", "5");
            DataPill pill4 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "5");
            DataPill pill5 = new DataPill(DashboardMeasure.RealBusiness_Damage, "M", "5");
            DataPill pill6 = new DataPill(DashboardMeasure.RealBusiness_Total, "M", "5");

            Dial dial1 = new Dial(DashboardMeasure.StockEmergency_RoutineMaintenance, "M", "5");
            Dial dial2 = new Dial(DashboardMeasure.StockEmergency_ExtendedMaintenance, "M", "5");
            Dial dial3 = new Dial(DashboardMeasure.StockEmergency_MechanicalRepair, "M", "5");
            Dial dial4 = new Dial(DashboardMeasure.StockEmergency_Damage, "M", "5");
            Dial dial5 = new Dial(DashboardMeasure.StockEmergency_Accessories, "M", "5");
            Dial dial6 = new Dial(DashboardMeasure.StockEmergency_Total, "M", "5");

            LineGraph ppuMonth = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "5");
            BarGraph ppuCat = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "5");

            Dial dial7 = new Dial(DashboardMeasure.EquipmentRate_OilFilters, "M", "5");
            Dial dial8 = new Dial(DashboardMeasure.EquipmentRate_AirFilters, "M", "5");
            Dial dial9 = new Dial(DashboardMeasure.EquipmentRate_PollenFilters, "M", "5");
            Dial dial10 = new Dial(DashboardMeasure.EquipmentRate_FuelFilters, "M", "5");
            Dial dial11 = new Dial(DashboardMeasure.EquipmentRate_SparkPlugs, "M", "5");
            Dial dial12 = new Dial(DashboardMeasure.EquipmentRate_WiperBlades, "M", "5");
            Dial dial13 = new Dial(DashboardMeasure.EquipmentRate_BrakePads, "M", "5");
            Dial dial14 = new Dial(DashboardMeasure.EquipmentRate_BrakeDiscs, "M", "5");
            Dial dial15 = new Dial(DashboardMeasure.EquipmentRate_TimingBelts, "M", "5");
            Dial dial16 = new Dial(DashboardMeasure.EquipmentRate_Batteries, "M", "5");

        }

        [TestMethod]
        public void PPUMonth_Match_PPUCat()
        {
            DataPeriod period = new DataPeriod(151);

            LineGraph ppuCat = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "5", period);
            BarGraph ppuMonth = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "5", period);

            //Must change this when change period id
            Assert.AreEqual(ppuCat.NatValues["JUL"], ppuMonth.NatValues["TOT"]);
        }

        [TestMethod]
        public void FocusGroupBonus()
        {
            DataGrid gridFG = new DataGrid(DashboardMeasure.FocusGroupBonus, "M", "5", new DataPeriod(205));

            Assert.IsNotNull(gridFG);
        }

        [TestMethod]
        public void TotalBonusByQuarter()
        {
            DataGrid gridTB = new DataGrid(DashboardMeasure.TotalBonusByQuarter, "M", "5");
        }

        [TestMethod]
        public void VolumeBonus()
        {
            DashboardControl<decimal> gridAV = new DashboardControl<decimal>(DashboardMeasure.VolumeBonus, "M", "5", new DataPeriod(153));
        }
    }
}
