﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.XS
{
    public class RealBusiness : DashboardModel<decimal>
    {
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() { { "YTD", 1 }, { "Change", 2 }, { "Objective", 3 } };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>();
        public override Dictionary<string, decimal> Template => new Dictionary<string, decimal>()
        {
            { "YTD", 0m }, { "Target", 0m }, { "Objective", 0m }, { "Change", 0m }
        };

        public RealBusiness(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string YTDSql = "SELECT \n"
               + "	d.Id dealer_id,\n"
               + "	coalesce(real_this, 0) real_this\n"
               + "from v_LiveDealers d\n"
               + "LEFT JOIN (\n"
               + "	select \n"
               + "		dealer_id,\n"
               + "		sum(real_value) real_this\n"
               + "	from realbusiness \n"
               + "	where dataperiod_id between @From and @To \n"
               + "		and level4_code in (" + GetL4Codes() + ") \n"
               + "	group by dealer_id\n"
               + ") rb ON d.id = rb.dealer_id";

            string TargetSql = "select\n"
               + "		d.id,\n"
               + "		coalesce(commitment, 0) as commitment\n"
               + "	from v_LiveDealers d\n"
               + "	left join (\n"
               + "		select\n"
               + "			dealer_id,\n"
               + "			SUM(commitment_month) commitment\n"
               + "		from dealercommitment\n"
               + "		where dataperiod_id between @From and @To\n"
               + "			and level4_code in (" + GetL4Codes() + ")\n"
               + "			and commitment_type = 'RB'	\n"
               + "		group by dealer_id\n"
               + "	) dc on dc.dealer_id = d.id";

            res.Add("YTD", MSSQLHelper.GetDictionary<int,decimal>(YTDSql,
                new SqlParameter[]
                {
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                }
            ).ToDictionary(x => x.Key, x => x.Value.ToString()));

            res.Add("PYTD", MSSQLHelper.GetDictionary<int,decimal>(YTDSql,
                new SqlParameter[]
                {
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod-12 },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id-12 },
                }
            ).ToDictionary(x => x.Key, x => x.Value.ToString()));

            res.Add("Target", MSSQLHelper.GetDictionary<int,decimal>(TargetSql,
                new SqlParameter[]
                {
                    new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                    new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                }
            ).ToDictionary(x => x.Key, x => x.Value.ToString()));

            return res;
        }

        private string GetL4Codes()
        {
            //Store this in the DB somewhere??
            switch (Measure.Enum)
            {
                case DashboardMeasure.RealBusiness_RoutineMaintenance:
                    return "'ROM'";
                case DashboardMeasure.RealBusiness_ExtendedMaintenance:
                    return "'EXM'";
                case DashboardMeasure.RealBusiness_MechanicalRepair:
                    return "'MEC'";
                case DashboardMeasure.RealBusiness_Damage:
                    return "'DAM'";
                case DashboardMeasure.RealBusiness_Accessories:
                    return "'ACC'";
                case DashboardMeasure.RealBusiness_Total:
                    return "'ACC','DAM','EXM','MEC','ROM'";
                default:
                    throw new ArgumentOutOfRangeException("Invalid measure");
            }
        }
    }
}
