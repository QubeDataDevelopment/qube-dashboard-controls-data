﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.XS
{
    public class PartsBasket_Seasonal : DashboardModel<decimal>
    {
        public override Dictionary<string, decimal> Template => new Dictionary<string, decimal>() {
            { "Performance", 0m }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>()
        {
            { "Performance", 1 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>();

        public PartsBasket_Seasonal(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            string sql = @"select
	                        d.id dealer_id,
	                        coalesce(d.dealergroup_id, 0) dealergroup_id,
	                        coalesce(q.sales_this, 0) sales_this,
	                        coalesce(q.target_actualised, 0) target_actualised
	                        --,coalesce(q.neededfornextband, 0) neededfornextband
                        from v_LiveDealers d
                        left join (
	                        select
		                        pb.dealergroup_id,
		                        sales_this,
		                        target_actualised
		                        --,neededfornextband
	                        from PB_Performance_DealerGroup pb
	                        inner join PB_BonusControl bc on bc.calendaryear = @Year and bc.quarter_id = @Quarter and bc.id = pb.basketcontrol_id and pb.period_from between bc.period_from and bc.period_to
	                        inner join dbo.CurrentDealerGroupSelectionTableForBonus('M', '" + Settings.GetString("MarketID") + @"', " + Settings.GetString("MarketID") + @") dg on dg.DealerGroup_ID = pb.dealergroup_id
                            WHERE pb.target_real > 0
                        ) q on q.dealergroup_id = d.dealergroup_id";

            DateTime maxPurchaseDate = MSSQLHelper.ExecuteScalar<DateTime>("Select dbo.MaxPurchaseDate()", null);

            EnumerableRowCollection<DataRow> data = MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                new SqlParameter("@Year", SqlDbType.Int) { Value = (maxPurchaseDate > DateTime.Parse("2019-12-31") && maxPurchaseDate < DateTime.Parse("2020-04-01") ? 2019 : int.Parse(Period.FinancialYear.Substring(0,4))) },
                new SqlParameter("@Quarter", SqlDbType.Int) { Value = (maxPurchaseDate > DateTime.Parse("2019-12-31") && maxPurchaseDate < DateTime.Parse("2020-04-01") ? 5 : Period.FinancialQuarter) }
            }).AsEnumerable();

            res.Add("dealergroup", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<int>("dealergroup_id").ToString()));
            res.Add("sales", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("sales_this").ToString()));
            res.Add("target", data.ToDictionary(x => x.Field<int>("dealer_id"), x => x.Field<decimal>("target_actualised").ToString()));

            return res;
        }
    }
}
