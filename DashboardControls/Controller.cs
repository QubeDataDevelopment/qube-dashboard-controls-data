﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public abstract class Controller : IController
    {
        public DashboardMeasure Measure { get; protected set; }
        public DataPeriod Period { get; protected set; }
        public Dictionary<string, Dictionary<int, string>> Data { get; protected set; }

        public Controller(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data)
        {
            this.Measure = measure;
            this.Data = data;
        }

        public Controller(DashboardMeasure measure, DataPeriod period, Dictionary<string, Dictionary<int, string>> data)
        {
            this.Measure = measure;
            this.Period = period;
            this.Data = data;
        }

        public abstract dynamic Execute(bool lastYear = false);
        public abstract dynamic ExecuteDealer(bool lastYear = false);
        public abstract Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData);
    }
}
