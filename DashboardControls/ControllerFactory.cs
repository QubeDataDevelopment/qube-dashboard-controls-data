﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public static class ControllerFactory
    {
        public static Controller Get(DashboardMeasure measure, DataPeriod period, Dictionary<string, Dictionary<int, string>> data)
        {
            switch(measure)
            {
                case DashboardMeasure.RealBusiness_RoutineMaintenance:
                case DashboardMeasure.RealBusiness_ExtendedMaintenance:
                case DashboardMeasure.RealBusiness_MechanicalRepair:
                case DashboardMeasure.RealBusiness_Damage:
                case DashboardMeasure.RealBusiness_Accessories:
                case DashboardMeasure.RealBusiness_Total:
                    return new Controllers.RealBusiness(measure, data);
                case DashboardMeasure.StockEmergency_RoutineMaintenance:
                case DashboardMeasure.StockEmergency_ExtendedMaintenance:
                case DashboardMeasure.StockEmergency_MechanicalRepair:
                case DashboardMeasure.StockEmergency_Damage:
                case DashboardMeasure.StockEmergency_Accessories:
                case DashboardMeasure.StockEmergency_Total:
                    return new Controllers.StockEmergency(measure, data);
                case DashboardMeasure.Turnover_PPUCategory:
                    return new Controllers.Turnover_PPUCategory(measure, data);
                case DashboardMeasure.Turnover_PPUMonth:
                    return new Controllers.Turnover_PPUMonth(measure, data);
                case DashboardMeasure.EquipmentRate_OilFilters:
                case DashboardMeasure.EquipmentRate_AirFilters:
                case DashboardMeasure.EquipmentRate_PollenFilters:
                case DashboardMeasure.EquipmentRate_FuelFilters:
                case DashboardMeasure.EquipmentRate_SparkPlugs:
                case DashboardMeasure.EquipmentRate_WiperBlades:
                case DashboardMeasure.EquipmentRate_BrakePads:
                case DashboardMeasure.EquipmentRate_BrakeDiscs:
                case DashboardMeasure.EquipmentRate_TimingBelts:
                case DashboardMeasure.EquipmentRate_Batteries:
                    return new Controllers.EquipmentRate(measure, data);
                case DashboardMeasure.TurnoverByType_FB:
                    return new Controllers.TurnoverByTypeFB(measure, data);
                case DashboardMeasure.TurnoverByType_RB:
                    return new Controllers.TurnoverByTypeRB(measure, data);
                case DashboardMeasure.Turnover_PPUYear:
                    return new Controllers.TurnoverPPU_Year(measure, data);
                case DashboardMeasure.AccessoriesBySales:
                    return new Controllers.AccessoriesBySales(measure, data);
                case DashboardMeasure.PartsBasket_Seasonal:
                    return new Controllers.PartsBasket_Seasonal(measure, data);
                case DashboardMeasure.PartsBasket_Total:
                    return new Controllers.PartsBasket_Total(measure, data);
                case DashboardMeasure.RealBusinessAA_Accessories:
                case DashboardMeasure.RealBusinessAA_Damage:
                case DashboardMeasure.RealBusinessAA_ExtendedMaintenance:
                case DashboardMeasure.RealBusinessAA_MechanicalRepair:
                case DashboardMeasure.RealBusinessAA_RoutineMaintenance:
                case DashboardMeasure.RealBusinessAA_Total:
                    return new Controllers.RealBusinessAA(measure, data);
                case DashboardMeasure.FocusGroupBonus:
                    return new Controllers.FocusGroupBonus(measure, data);
                case DashboardMeasure.TotalBonusByQuarter:
                    return new Controllers.TotalBonusByQuarter(measure, data);
                case DashboardMeasure.VolumeBonus:
                    return new Controllers.VolumeBonus(measure, data);
                case DashboardMeasure.PartsBasket:
                    return new Controllers.PartsBasket(measure, period, data);
                case DashboardMeasure.AccessoriesBasket:
                    return new Controllers.AccessoriesBasket(measure, period, data);
                case DashboardMeasure.LifeCycleBasket:
                    return new Controllers.LifeCycleBasket(measure, period, data);
                case DashboardMeasure.WinterTyresBasket:
                    return new Controllers.WinterTyresBasket(measure, period, data);
                case DashboardMeasure.BonusSummary:
                    return new Controllers.BonusSummary(measure, data);
                default:
                    throw new ArgumentOutOfRangeException("Measure invalid!");
            }
        }
    }
}
