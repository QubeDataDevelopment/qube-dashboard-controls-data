﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QubeUtils;

namespace QubeDashboardControls.Controllers
{
    public class VolumeBonus : Controller
    {
        public VolumeBonus(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, decimal> res = new Dictionary<string, decimal>();
            if (!Data.ContainsKey("dealergroup" + (lastYear ? "LY" : ""))) return res;

            CultureInfo en = new CultureInfo("en-GB");

            List<Objects.XS.GenericDealerGroup> resE =
                Data.SelectMany(sm => sm.Value.Select(s => new { key = sm.Key, dealercode = s.Key, value = s.Value }))
                .GroupBy(g => g.dealercode)
                .ToDictionary(x => x.Key, x =>
                    new Objects.XS.GenericDealerGroup
                    {
                        Dealergroup_id = x.Any(a => a.key == "dealergroup") ? int.Parse(x.Where(w => w.key == "dealergroup").First().value) : 0,
                        Sales = x.Any(a => a.key == "sales") ? decimal.Parse(x.Where(w => w.key == "sales").First().value, en.NumberFormat) : 0m,
                        Target = x.Any(a => a.key == "bonus") ? decimal.Parse(x.Where(w => w.key == "bonus").First().value, en.NumberFormat) : 0m,
                    }
                ).DistinctBy(d => d.Value.Dealergroup_id).Select(s => s.Value).ToList();

            res.Add("Sales", resE.Count > 0 ? resE.Sum(s => s.Sales) : 0m);
            res.Add("Bonus", resE.Count > 0 ? resE.Average(a => a.Target) : 0m);

            return res;
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            return Data.ToDictionary(x => x.Key, x => x.Value.ToDictionary(x2 => x2.Key, x2 => decimal.Parse(x2.Value)));
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
