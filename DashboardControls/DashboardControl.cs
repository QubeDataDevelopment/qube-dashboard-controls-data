﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QubeUtils;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.Threading;
using System.Globalization;

namespace QubeDashboardControls
{
    public class DashboardControl<T> : IDashboardControl<T>
    {
        protected Measure Measure { get; set; }
        protected string SelectionLevel { get; set; }
        protected string SelectionId { get; set; }
        protected DataPeriod Period { get; set; }

        protected int DealerCount { get; set; }
        protected List<int> NationalDealerIds { get; set; }
        protected List<int> SelectionDealerIds { get; set; }
        protected string Zone { get; set; }
        protected List<int> ZoneDealerIds { get; set; }
        protected bool HasData { get; set; }

        protected virtual bool LastYear => false;
        protected virtual bool AllowValidation => true;
        protected virtual PeriodSetting PeriodSettings => new PeriodSetting();

        protected DashboardModel<T> Model { get; set; }

        public Dictionary<string, int> Weights { get; private set; }
        public Dictionary<string, List<string>> Text { get; private set; }

        public Dictionary<string, T> Values { get; private set; }
        public Dictionary<string, T> ValuesLY { get; private set; }
        public Dictionary<string, int> Quartiles { get; private set; }
        public Dictionary<string, T> ZoneValues { get; private set; }
        public Dictionary<string, T> NatValues { get; private set; }
        public Dictionary<string, T> Template { get; private set; }

        public DashboardControl(DashboardMeasure measure, string selectionLevel, string selectionId, DataPeriod period = null)
        {
            CultureInfo gb = new CultureInfo("en-GB");
            CultureInfo oc = Thread.CurrentThread.CurrentCulture;

            Thread.CurrentThread.CurrentCulture = gb;

            this.Measure = MeasureFactory.Factory(measure);
            this.SelectionLevel = selectionLevel;
            this.SelectionId = selectionId;
            this.Period = GetDataPeriod(period);
            if(Measure.HasQuartiles) this.DealerCount = MSSQLHelper.ExecuteScalar<int>("select count(1) from dbo.CurrentDealerSelectionTable('M', '" + Settings.GetString("MarketID") + "')", null);
            this.NationalDealerIds = GetDealerIds(ControlLevel.National);
            this.SelectionDealerIds = GetDealerIds();

            if (SelectionLevel == (Settings.GetBool("IsVCR") ? "C" : "D"))
            {
                this.Zone = MSSQLHelper.ExecuteScalar<string>(Settings.GetBool("IsVCR") ? "select isnull((select Zone from CentreMaster where dealercode = '" + SelectionId + "'), 0)" : "select isnull((select Zone from Dealers where Id = '" + SelectionId + "'), 0)", null);
                this.ZoneDealerIds = GetDealerIds(ControlLevel.Zone);
            }
            else if(SelectionLevel == "G")
            {
                // TODO: VCR Groups
                this.Zone = MSSQLHelper.ExecuteScalar<string>(Settings.GetBool("IsVCR") ? "" : "select isnull((select Zone from DealerGroups where Id = '" + SelectionId + "'), 0)", null);
                this.ZoneDealerIds = GetDealerIds(ControlLevel.Zone);
            }
            else
                ZoneDealerIds = new List<int>();

            this.HasData = _HasData();

            this.Model = GetDashboardModel();

            this.Weights = Model.Weights;
            this.Text = Model.Text;

            //Generate dealer data if not there
            if (!HasData)
            {
                DeleteData();

                if (typeof(T).Namespace == "System") {
                    SaveDealerData(CalculateDealerData());
                    if (LastYear) SaveDealerData(CalculateDealerData(true));
                }
                else {
                    SaveDealerData(Model.Values);
                    if (LastYear) SaveDealerData(Model.ValuesLY);
                }

                CleanupRefresh();
            }

            Model.SelectionValues = GetSelectionData();
            if(LastYear) Model.SelectionValuesLY = GetSelectionData(ControlLevel.Default, true);
            Model.ZoneValues = GetSelectionData(ControlLevel.Zone);
            Model.NationalValues = GetSelectionData(ControlLevel.National);

            Values = CalculateData();
            if(LastYear) ValuesLY = CalculateData(ControlLevel.Default, true);
            if (!string.IsNullOrEmpty(Zone))
            {
                ZoneValues = CalculateData(ControlLevel.Zone);
            }
            else if(new List<string>() { "R", "Z", "G" }.Contains(SelectionLevel))
            {
                ZoneValues = Values.ToDictionary(x => x.Key, x => x.Value);
            }
            NatValues = CalculateData(ControlLevel.National);
            if (ZoneValues == null && selectionLevel == "M")
                ZoneValues = NatValues;

            if(PeriodSettings.LastYear)
                foreach (KeyValuePair<string, T> v in ValuesLY)
                    Values.Add(v.Key, v.Value);

            if (PeriodSettings.Zone)
                foreach (KeyValuePair<string, T> v in ZoneValues)
                    Values.Add(v.Key + "Zone", v.Value);

            if (PeriodSettings.National)
                foreach (KeyValuePair<string, T> v in NatValues)
                    Values.Add(v.Key + "Nat", v.Value);

            if (Measure.HasQuartiles) Quartiles = GetQuartiles();

            Template = Model.Template;

            ValidateValues();

            Thread.CurrentThread.CurrentCulture = oc;
        }

        protected virtual Dictionary<string, T> CalculateData(ControlLevel level = ControlLevel.Default, bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (level == ControlLevel.National ? Model.NationalValues : (level == ControlLevel.Zone ? Model.ZoneValues : (lastYear ? Model.SelectionValuesLY : Model.SelectionValues)));

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return (Dictionary<string, T>)res.Execute(lastYear);
        }

        protected virtual Dictionary<string, Dictionary<int, T>> CalculateDealerData(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (lastYear ? Model.ValuesLY : Model.Values);

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return (Dictionary<string, Dictionary<int, T>>)res.ExecuteDealer(lastYear);
        }

        protected virtual Dictionary<string, int> GetQuartiles()
        {
            Dictionary<string, int> res = new Dictionary<string, int>();

            foreach (KeyValuePair<string, T> d in Values)
            {
                res.Add(d.Key, GetQuartile(d.Value, GetRank(d.Key, d.Value), d.Key, DealerCount));
            }

            return res;
        }

        private Dictionary<string, Dictionary<int, string>> GetSelectionData(ControlLevel level = ControlLevel.Default, bool lastYear = false)
        {
            List<int> DealerIds = level == ControlLevel.National ? NationalDealerIds :(level == ControlLevel.Default ? SelectionDealerIds : ZoneDealerIds);
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            if (DealerIds.Count == 0) return res;

            foreach (KeyValuePair<string, Dictionary<int, string>> d in (lastYear ? Model.ValuesLY : Model.Values))
            {
                res.Add(d.Key, d.Value.Where(w => DealerIds.Contains(w.Key)).Select(s => s).ToDictionary(x => x.Key, x => x.Value));
            }

            return res;
        }

        private List<int> GetDealerIds(ControlLevel level = ControlLevel.Default)
        {
            return MSSQLHelper.GetList<int>("select dealer_id from dbo.CurrentDealerSelectionTable(@SelectionLevel, @SelectionId)",
                    new SqlParameter[]
                    {
                        new SqlParameter("@SelectionLevel", SqlDbType.VarChar) { Value = level == ControlLevel.National ? "M" : (level == ControlLevel.Zone ? "Z" : SelectionLevel) },
                        new SqlParameter("@SelectionId", SqlDbType.VarChar) { Value = level == ControlLevel.National ? Settings.GetString("MarketID") :(level == ControlLevel.Zone ? Zone : SelectionId) },
                    }
                );
        }

        private void CleanupRefresh()
        {
            string sql = "With dups as\n"
               + "   (select Dealer_id, ScoreKey, row_number() over(partition by Dealer_Id,ScoreKey order by Dealer_Id,ScoreKey, Score DESC ) rownumber \n"
               + "   from DashboardRanking where DashboardMeasureId = @MeasureId and PeriodId = @PeriodId)\n"
               + "delete from dups where not rownumber = 1";

            MSSQLHelper.ExecuteSQL(sql, new SqlParameter[] {
                    new SqlParameter("@MeasureId", SqlDbType.Int) { Value = Measure.Id },
                    new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
                });
        }

        //Data must be already ordered
        private void SaveDealerData(Dictionary<string, Dictionary<int, T>> allData)
        {
            string connStr = ConfigurationManager.ConnectionStrings[Settings.GetString("ConnectionStringName")].ToString();
            string sql = "INSERT INTO DashboardRanking(DashboardMeasureId,PeriodId,Dealer_Id,ScoreKey,Score,Ranking,Quartile,LastUpdate) VALUES(" + Measure.Id + ", " + Period.Id + ",{0},'{1}','{2}',{3},{4},'{5}'); ";
            int maxRank = 0;
            int wRank = 1;
            int sameRank = 0;
            T previousScore = default(T);

            Parallel.ForEach(allData, (data) =>
            {
               StringBuilder sb = new StringBuilder();

               if (data.Value.Count == 0) return;

               maxRank = data.Value.Count();
               wRank = 1;
               sameRank = 0;
               previousScore = default(T);

               foreach (KeyValuePair<int, T> row in data.Value.OrderByDescending(o => o.Value).ToDictionary(x => x.Key, x => x.Value))
               {
                   if (row.Value.Equals(previousScore))
                       sameRank = sameRank == 0 ? wRank - 1 : sameRank;
                   else
                       sameRank = 0;

                   int rank = row.Value.Equals(previousScore) ? sameRank : wRank;

                   sb.Append(string.Format(sql, row.Key, data.Key, row.Value, rank, GetQuartile(row.Value, rank, data.Key, maxRank), DateTime.Now.ToString("yyyyMMdd HH:mm:ss")));

                   wRank++;

                   previousScore = row.Value;

               }

               if(sb.Length > 0)
                MSSQLHelper.ExecuteSQL(sb.ToString(), null);
           });
        }

        private void SaveDealerData(Dictionary<string, Dictionary<int, string>> allData)
        {
            string connStr = ConfigurationManager.ConnectionStrings[Settings.GetString("ConnectionStringName")].ToString();
            string sql = "INSERT INTO DashboardRanking(DashboardMeasureId,PeriodId,Dealer_Id,ScoreKey,Score,Ranking,Quartile,LastUpdate) VALUES(" + Measure.Id + ", " + Period.Id + ",{0},'{1}','{2}',{3},{4},'{5}'); ";
            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, Dictionary<int, string>> data in allData)

                foreach (KeyValuePair<int, string> row in data.Value)
                    sb.Append(string.Format(sql, row.Key, data.Key, row.Value, 0, 0, DateTime.Now.ToString("yyyyMMdd HH:mm:ss")));

            if (sb.Length > 0)
                MSSQLHelper.ExecuteSQL(sb.ToString(), null);
        }

        protected int GetRank(string key, T value) =>
            MSSQLHelper.ExecuteScalar<int>("SELECT COUNT(*)+1 FROM DashboardRanking WHERE DashboardMeasureId = @Id AND PeriodId = @PeriodId AND ScoreKey = @Key AND Score > @Value",
                new SqlParameter[] {
                    new SqlParameter("@Id", SqlDbType.Int) { Value = Measure.Id },
                    new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id },
                    new SqlParameter("@Key", SqlDbType.VarChar) { Value = key },
                    new SqlParameter("@Value", SqlDbType.VarChar) { Value = value.ToString() },
                    }
                );

        protected int GetQuartile(T value, int rank, string key, int maxRank = -1)
        {
            if (maxRank == -1)
                maxRank = MSSQLHelper.ExecuteScalar<int>("SELECT count(1) FROM DashboardRanking WHERE DashboardMeasureId = @Id AND PeriodId = @PeriodId AND ScoreKey = @Key",
                    new SqlParameter[]
                    {
                        new SqlParameter("@Id", SqlDbType.Int) { Value = Measure.Id },
                        new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id },
                        new SqlParameter("@Key", SqlDbType.VarChar) { Value = key },
                    });

            decimal perQuart = maxRank / 4m;
            decimal wRank = 0;
            int quartile = 0;

            if (perQuart == 0) return 1;

            while (wRank.CompareTo(rank) <= -1 || wRank == 0)
            {
                wRank += perQuart;
                quartile++;
            }

            return quartile;
        }

        private bool _HasData()
        {
            DateTime LastUpdated = MSSQLHelper.ExecuteScalar<DateTime>("SELECT ISNULL(MIN(LastUpdate), CAST('1900-01-01' AS DATETIME)) FROM DashboardRanking WHERE DashboardMeasureId = @Id AND PeriodId = @PeriodId",
                new SqlParameter[] {
                    new SqlParameter("@Id", SqlDbType.Int) { Value = Measure.Id },
                    new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
                });

            DateTime LastRefresh = MSSQLHelper.ExecuteScalar<DateTime>("select LastWebRefresh from system", null);

            //if (LastUpdated >= LastRefresh || LastUpdated.Date >= Period.EndDateActual.AddDays(14).Date)
            if(LastUpdated >= LastRefresh)
                return true;

            return false;
        }

        private void DeleteData() =>
            MSSQLHelper.ExecuteSQL(
                "DELETE FROM DashboardRanking WHERE DashboardMeasureId = @Id AND PeriodId = @PeriodId",
                new SqlParameter[] {
                    new SqlParameter("@Id", SqlDbType.Int) { Value = Measure.Id },
                    new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id },
                });

        private DashboardModel<T> GetDashboardModel()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Type type;

            // Changed to check XS first, then try site models path if doesnt exists. Will throw error if doesnt exist in models path either!!
            if(assembly.GetTypes().Any(t => t.Namespace == "QubeDashboardControls.Models.XS" && t.Name == (Measure.IsGroup ? Measure.Name.Split('_')[0] : Measure.Name)))
            { 
                type = assembly.GetTypes().First(t => t.Namespace == "QubeDashboardControls.Models.XS" && t.Name == (Measure.IsGroup ? Measure.Name.Split('_')[0] : Measure.Name));
            }
            else
            {
                type = assembly.GetTypes().First(t => t.Namespace == "QubeDashboardControls.Models." + Settings.GetString("ModelsPath") && t.Name == (Measure.IsGroup ? Measure.Name.Split('_')[0] : Measure.Name));
            }

            return (DashboardModel<T>) Activator.CreateInstance(type, new object[] { Measure, Period, HasData, LastYear });
        }

        private void ValidateValues()
        {
            if (!AllowValidation) return;

            List<string> toRemove = Values.Select(s => s.Key).ToList();

            foreach (KeyValuePair<string, T> tmp in Model.Template)
            {
                if (Values == null) Values = new Dictionary<string, T>();

                if (!Values.ContainsKey(tmp.Key))
                {
                    Values.Add(tmp.Key, tmp.Value);

                    if (Quartiles == null) Quartiles = new Dictionary<string, int>();

                    if (!Quartiles.ContainsKey(tmp.Key))
                        Quartiles.Add(tmp.Key, 4);
                }

                if (NatValues == null)NatValues = new Dictionary<string, T>();

                if (!NatValues.ContainsKey(tmp.Key))
                    NatValues.Add(tmp.Key, tmp.Value);

                if (ZoneValues == null) ZoneValues = new Dictionary<string, T>();

                if (!ZoneValues.ContainsKey(tmp.Key))
                    ZoneValues.Add(tmp.Key, tmp.Value);

                toRemove.Remove(tmp.Key);
            }

            foreach(string r in toRemove)
            {
                if (Values != null) Values.Remove(r);
                if (NatValues != null) NatValues.Remove(r);
                if (ZoneValues != null) ZoneValues.Remove(r);
                if (Quartiles != null) Quartiles.Remove(r);
            }
        }

        private DataPeriod GetDataPeriod(DataPeriod period)
        {
            if (period == null)
                return new DataPeriod(MSSQLHelper.ExecuteScalar<int>("select max(id) from dataperiods where periodstatus = 'C'", null));

            return period;
        }
    }

    public enum ControlLevel
    {
        Default,
        National,
        Zone
    }

    public enum DashboardMeasure {
        RealBusiness_RoutineMaintenance = 1,
        RealBusiness_ExtendedMaintenance = 2,
        RealBusiness_MechanicalRepair = 3,
        RealBusiness_Damage = 4,
        RealBusiness_Accessories = 5,
        RealBusiness_Total = 6,
        StockEmergency_RoutineMaintenance = 7,
        StockEmergency_ExtendedMaintenance = 8,
        StockEmergency_MechanicalRepair = 9,
        StockEmergency_Damage = 10,
        StockEmergency_Accessories = 11,
        StockEmergency_Total = 12,
        Turnover_PPUMonth = 13,
        Turnover_PPUCategory = 14,
        EquipmentRate_OilFilters = 15,
        EquipmentRate_AirFilters = 16,
        EquipmentRate_PollenFilters = 17,
        EquipmentRate_FuelFilters = 18,
        EquipmentRate_SparkPlugs = 19,
        EquipmentRate_WiperBlades = 20,
        EquipmentRate_BrakePads = 21,
        EquipmentRate_BrakeDiscs = 22,
        EquipmentRate_TimingBelts = 23,
        EquipmentRate_Batteries = 24,
        TurnoverByType_FB = 25,
        TurnoverByType_RB = 26,
        RealBusinessAA_RoutineMaintenance = 27,
        RealBusinessAA_ExtendedMaintenance = 28,
        RealBusinessAA_MechanicalRepair = 29,
        RealBusinessAA_Damage = 30,
        RealBusinessAA_Accessories = 31,
        RealBusinessAA_Total = 32,
        PartsBasket_Seasonal = 33,
        PartsBasket_Total = 34,
        FocusGroupBonus = 35,
        TotalBonusByQuarter = 36,
        Turnover_PPUYear = 37,
        AccessoriesBySales = 38,
        VolumeBonus = 39,
        PartsBasket = 40,
        AccessoriesBasket = 41,
        LifeCycleBasket = 42,
        WinterTyresBasket = 43,
        BonusSummary = 44
    }
}