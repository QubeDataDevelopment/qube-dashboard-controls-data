﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public class BarGraph : DashboardControl<DashboardObject>
    {
        public BarGraph(DashboardMeasure measure, string selectionLevel, string selectionId, DataPeriod period = null) : base(measure, selectionLevel, selectionId, period) { }

        private List<DashboardMeasure> lastYearMeasures = new List<DashboardMeasure>() { DashboardMeasure.AccessoriesBySales, DashboardMeasure.Turnover_PPUYear };
        protected override bool LastYear { get => (lastYearMeasures.Contains(Measure.Enum) || Measure.Name.Split('_')[0] == "RealBusinessAA" ? true : false); }

        protected override PeriodSetting PeriodSettings {
            get {
                if (Measure.Name.Split('_')[0] == "RealBusinessAA")
                    return new PeriodSetting { LastYear = true };

                switch (Measure.Enum) {
                    case DashboardMeasure.AccessoriesBySales:
                    case DashboardMeasure.Turnover_PPUYear:
                        return new PeriodSetting { LastYear = true, Zone = true, National = true };
                    case DashboardMeasure.Turnover_PPUCategory:
                        return new PeriodSetting { National = true };
                    default:
                        return new PeriodSetting();
                }
            }
        }

        protected override bool AllowValidation {
            get {
                if (Measure.Name.Split('_')[0] == "RealBusinessAA")
                    return false;

                switch (Measure.Enum)
                {
                    case DashboardMeasure.AccessoriesBySales:
                    case DashboardMeasure.Turnover_PPUYear:
                    case DashboardMeasure.Turnover_PPUCategory:
                        return false;
                    default:
                        return true;
                }
            }
        }

        protected override Dictionary<string, DashboardObject> CalculateData(ControlLevel level = ControlLevel.Default, bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (level == ControlLevel.National ? Model.NationalValues : (level == ControlLevel.Zone ? Model.ZoneValues : (lastYear ? Model.SelectionValuesLY : Model.SelectionValues)));

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return (Dictionary<string, DashboardObject>) res.Execute(lastYear);
        }

        protected override Dictionary<string, Dictionary<int, DashboardObject>> CalculateDealerData(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (lastYear ? Model.ValuesLY : Model.Values);

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return (Dictionary<string, Dictionary<int, DashboardObject>>) res.ExecuteDealer(lastYear);
        }

        protected override Dictionary<string, int> GetQuartiles()
        {
            Dictionary<string, Dictionary<int, string>> data = Model.NationalValues;

            Controller res = ControllerFactory.Get(Measure.Enum, Period, data);

            return res.ExecuteQuartiles(Values.ToDictionary(x => x.Key, x => (object) x.Value));
        }
    }
}
