﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class GenericDealerGroup : DashboardObject
    {
        public int Dealergroup_id { get; set; }
        public decimal Sales { get; set; }
        public decimal Target { get; set; }
        public decimal Value { get; set; }
    }
}
