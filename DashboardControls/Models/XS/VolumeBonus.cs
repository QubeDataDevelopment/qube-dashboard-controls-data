﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Models.XS
{
    public class VolumeBonus : DashboardModel<decimal>
    {
        public override Dictionary<string, decimal> Template => new Dictionary<string, decimal> {
            { "Sales", 0m }, { "Bonus", 0m }
        };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>
        {
            { "Sales", 1 }, { "Bonus", 2 }
        };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>> {
            {"English", new List<string> { "Sales", "Band" } },
            {"German", new List<string> { "Sales", "Band" } },
            {"French", new List<string> { "Sales", "Band" } }
        };

        public VolumeBonus(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            string sql =
                @"select
	                d.id dealer_id,
	                coalesce(d.dealergroup_id, 0) dealergroup_id,
	                coalesce(col, '') col,
	                coalesce(value, 0) value
                from v_LiveDealers d
                left join (
	                select
		                dealergroup_id,
		                left(col, charindex('_', col)-1) col,
		                value
	                FROM (
                        SELECT qv.*
                        FROM QV_Performance qv
                        INNER JOIN dbo.PB_Bonuscontrol bc ON bc.calendaryear = qv.year_id AND bc.quarter_id = qv.quarter_id
		                LEFT JOIN dbo.PB_Targets pbt ON pbt.control_id = bc.id  AND pbt.dealergroup_id = qv.dealergroup_id 
                        WHERE qv.year_id = @Year and qv.quarter_id = @Quarter and
                            (
				                (qv.year_id < 2019 AND qv.quarter_id IN (1,2,3,4))
				                OR
				                (qv.year_id = 2019 AND ((qv.quarter_id = 4 AND pbt.dealergroup_id IS NOT NULL) OR qv.quarter_id IN (1,2,3)))
				                OR
				                (qv.year_id >= 2020 AND pbt.dealergroup_id IS NOT null)
			                )
                    )q
	                unpivot(
		                value for col in (sales_this, bonus_pc_today)
	                ) p
                ) t on t.dealergroup_id = d.dealergroup_id";

            DateTime maxPurchaseDate = MSSQLHelper.ExecuteScalar<DateTime>("Select dbo.MaxPurchaseDate()", null);

            EnumerableRowCollection<DataRow> data = MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                new SqlParameter("@Year", SqlDbType.Int) { Value = (maxPurchaseDate > DateTime.Parse("2019-12-31") && maxPurchaseDate < DateTime.Parse("2020-04-01") ? 2019 : int.Parse(Period.FinancialYear.Substring(0,4)))},
                new SqlParameter("@Quarter", SqlDbType.Int) { Value = (maxPurchaseDate > DateTime.Parse("2019-12-31") && maxPurchaseDate < DateTime.Parse("2020-04-01") ? 5 : Period.FinancialQuarter) }
            }).AsEnumerable();

            Dictionary<string, Dictionary<int, string>> res = data
                .GroupBy(g => g.Field<string>("col"))
                .Where(w => !string.IsNullOrEmpty(w.Key))
                .ToDictionary(
                    x => x.Key, 
                    x => x.GroupBy(g => g.Field<int>("dealer_id"))
                        .ToDictionary(x2 => x2.Key, x2 => x2.Sum(s => s.Field<decimal>("value")).ToString())
            );

            res.Add("dealergroup", data.GroupBy(g => g.Field<int>("dealer_id")).ToDictionary(x => x.Key, x => x.First().Field<int>("dealergroup_id").ToString()));

            return res;
        }
    }
}
