﻿using System;
using System.Globalization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QubeDashboardControls;
using QubeUtils;

namespace DashboardControlsTest
{
    [TestClass]
    public class ControlsDE_deDE
    {
        [TestMethod]
        public void RealBusiness()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPeriod period = new DataPeriod(152);

            DataPill pill1 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "5");
            DataPill pill2 = new DataPill(DashboardMeasure.RealBusiness_ExtendedMaintenance, "M", "5");
            DataPill pill3 = new DataPill(DashboardMeasure.RealBusiness_MechanicalRepair, "M", "5");
            DataPill pill4 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "5");
            DataPill pill5 = new DataPill(DashboardMeasure.RealBusiness_Damage, "M", "5");
            DataPill pill6 = new DataPill(DashboardMeasure.RealBusiness_Total, "M", "5");
        }

        [TestMethod]
        public void PPU_Month()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            LineGraph ppuMonth = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "5");
        }

        [TestMethod]
        public void PPU_Category()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            BarGraph ppuCat = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "5");
        }

        [TestMethod]
        public void StockVsEmergency()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPeriod period = new DataPeriod(153);

            Dial dial1 = new Dial(DashboardMeasure.StockEmergency_RoutineMaintenance, "M", "5");
            Dial dial2 = new Dial(DashboardMeasure.StockEmergency_ExtendedMaintenance, "M", "5");
            Dial dial3 = new Dial(DashboardMeasure.StockEmergency_MechanicalRepair, "M", "5");
            Dial dial4 = new Dial(DashboardMeasure.StockEmergency_Damage, "M", "5");
            Dial dial5 = new Dial(DashboardMeasure.StockEmergency_Accessories, "M", "5");
            Dial dial6 = new Dial(DashboardMeasure.StockEmergency_Total, "M", "5");
        }

        [TestMethod]
        public void TurnoverByType_FB()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPeriod period = new DataPeriod(155);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "5");
        }

        [TestMethod]
        public void TurnoverByType_RB()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPeriod period = new DataPeriod(154);

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "5");
        }

        [TestMethod]
        public void TurnoverByYear()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPeriod period = new DataPeriod(154);

            BarGraph barGraph1 = new BarGraph(DashboardMeasure.Turnover_PPUYear, "M", "5");
        }

        [TestMethod]
        public void PartsBasket_Seasonal()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Seasonal, "M", "5", new DataPeriod(155));
        }

        [TestMethod]
        public void PartsBasket_Total()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Total, "M", "5");
        }

        [TestMethod]
        public void PartsBasket()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPeriod period = new DataPeriod(0);

            PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Seasonal, "M", "5", period);
            PieChart pieChart2 = new PieChart(DashboardMeasure.PartsBasket_Total, "M", "5", period);
        }

        [TestMethod]
        public void RealBusinessAA()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            BarGraph lineGraph1 = new BarGraph(DashboardMeasure.RealBusinessAA_Accessories, "M", "5");
            BarGraph lineGraph2 = new BarGraph(DashboardMeasure.RealBusinessAA_Damage, "M", "5");
            BarGraph lineGraph3 = new BarGraph(DashboardMeasure.RealBusinessAA_ExtendedMaintenance, "M", "5");
            BarGraph lineGraph4 = new BarGraph(DashboardMeasure.RealBusinessAA_MechanicalRepair, "M", "5");
            BarGraph lineGraph5 = new BarGraph(DashboardMeasure.RealBusinessAA_RoutineMaintenance, "M", "5");
            BarGraph lineGraph6 = new BarGraph(DashboardMeasure.RealBusinessAA_Total, "M", "5");
        }

        [TestMethod]
        public void AccessoriesBySales()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            BarGraph barGraph1 = new BarGraph(DashboardMeasure.AccessoriesBySales, "M", "5");
        }

        [TestMethod]
        public void TurnoverByType()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "5");
            DataGrid grid2 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "5");
        }

        [TestMethod]
        public void Total_MainDashboard()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataGrid grid1 = new DataGrid(DashboardMeasure.TurnoverByType_FB, "M", "5");
            DataGrid grid2 = new DataGrid(DashboardMeasure.TurnoverByType_RB, "M", "5");

            BarGraph barGraph1 = new BarGraph(DashboardMeasure.RealBusinessAA_Accessories, "M", "5");
            BarGraph barGraph2 = new BarGraph(DashboardMeasure.RealBusinessAA_Damage, "M", "5");
            BarGraph barGraph3 = new BarGraph(DashboardMeasure.RealBusinessAA_ExtendedMaintenance, "M", "5");
            BarGraph barGraph4 = new BarGraph(DashboardMeasure.RealBusinessAA_MechanicalRepair, "M", "5");
            BarGraph barGraph5 = new BarGraph(DashboardMeasure.RealBusinessAA_RoutineMaintenance, "M", "5");
            BarGraph barGraph6 = new BarGraph(DashboardMeasure.RealBusinessAA_Total, "M", "5");

            PieChart pieChart1 = new PieChart(DashboardMeasure.PartsBasket_Seasonal, "M", "5");
            PieChart pieChart2 = new PieChart(DashboardMeasure.PartsBasket_Total, "M", "5");

            DataGrid grid3 = new DataGrid(DashboardMeasure.FocusGroupBonus, "M", "5");
            DataGrid grid4 = new DataGrid(DashboardMeasure.TotalBonusByQuarter, "M", "5");

            BarGraph barGraph7 = new BarGraph(DashboardMeasure.Turnover_PPUYear, "M", "5");
            BarGraph barGraph8 = new BarGraph(DashboardMeasure.AccessoriesBySales, "M", "5");
        }

        [TestMethod]
        public void EquipmentRate()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            Dial dial7 = new Dial(DashboardMeasure.EquipmentRate_OilFilters, "M", "5");
            Dial dial8 = new Dial(DashboardMeasure.EquipmentRate_AirFilters, "M", "5");
            Dial dial9 = new Dial(DashboardMeasure.EquipmentRate_PollenFilters, "M", "5");
            Dial dial10 = new Dial(DashboardMeasure.EquipmentRate_FuelFilters, "M", "5");
            Dial dial11 = new Dial(DashboardMeasure.EquipmentRate_SparkPlugs, "M", "5");
            Dial dial12 = new Dial(DashboardMeasure.EquipmentRate_WiperBlades, "M", "5");
            Dial dial13 = new Dial(DashboardMeasure.EquipmentRate_BrakePads, "M", "5");
            Dial dial14 = new Dial(DashboardMeasure.EquipmentRate_BrakeDiscs, "M", "5");
            Dial dial15 = new Dial(DashboardMeasure.EquipmentRate_TimingBelts, "M", "5");
            Dial dial16 = new Dial(DashboardMeasure.EquipmentRate_Batteries, "M", "5");
        }

        //[TestMethod]
        //public void BadMeasure()
        //{
        //    DataPill pill1 = new DataPill("BAD", "M", "5");
        //}

        [TestMethod]
        public void Total()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPill pill1 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "6");
            //DataPill pill2 = new DataPill(DashboardMeasure.RealBusiness_ExtendedMaintenance, "M", "6");
            //DataPill pill3 = new DataPill(DashboardMeasure.RealBusiness_MechanicalRepair, "M", "6");
            //DataPill pill4 = new DataPill(DashboardMeasure.RealBusiness_RoutineMaintenance, "M", "6");
            //DataPill pill5 = new DataPill(DashboardMeasure.RealBusiness_Damage, "M", "6");
            //DataPill pill6 = new DataPill(DashboardMeasure.RealBusiness_Total, "M", "6");

            Dial dial1 = new Dial(DashboardMeasure.StockEmergency_RoutineMaintenance, "M", "6");
            //Dial dial2 = new Dial(DashboardMeasure.StockEmergency_ExtendedMaintenance, "M", "6");
            //Dial dial3 = new Dial(DashboardMeasure.StockEmergency_MechanicalRepair, "M", "6");
            //Dial dial4 = new Dial(DashboardMeasure.StockEmergency_Damage, "M", "6");
            //Dial dial5 = new Dial(DashboardMeasure.StockEmergency_Accessories, "M", "6");
            //Dial dial6 = new Dial(DashboardMeasure.StockEmergency_Total, "M", "6");

            LineGraph ppuMonth = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "6");
            BarGraph ppuCat = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "6");

            Dial dial7 = new Dial(DashboardMeasure.EquipmentRate_OilFilters, "M", "6");
            //Dial dial8 = new Dial(DashboardMeasure.EquipmentRate_AirFilters, "M", "6");
            //Dial dial9 = new Dial(DashboardMeasure.EquipmentRate_PollenFilters, "M", "6");
            //Dial dial10 = new Dial(DashboardMeasure.EquipmentRate_FuelFilters, "M", "6");
            //Dial dial11 = new Dial(DashboardMeasure.EquipmentRate_SparkPlugs, "M", "6");
            //Dial dial12 = new Dial(DashboardMeasure.EquipmentRate_WiperBlades, "M", "6");
            //Dial dial13 = new Dial(DashboardMeasure.EquipmentRate_BrakePads, "M", "6");
            //Dial dial14 = new Dial(DashboardMeasure.EquipmentRate_BrakeDiscs, "M", "6");
            //Dial dial15 = new Dial(DashboardMeasure.EquipmentRate_TimingBelts, "M", "6");
            //Dial dial16 = new Dial(DashboardMeasure.EquipmentRate_Batteries, "M", "6");
        }

        [TestMethod]
        public void PPUMonth_Match_PPUCat()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataPeriod period = new DataPeriod(151);

            LineGraph ppuCat = new LineGraph(DashboardMeasure.Turnover_PPUMonth, "M", "5", period);
            BarGraph ppuMonth = new BarGraph(DashboardMeasure.Turnover_PPUCategory, "M", "5", period);

            //Must change this when change period id
            Assert.AreEqual(ppuCat.NatValues["JUL"], ppuMonth.NatValues["TOT"]);
        }

        [TestMethod]
        public void FocusGroupBonus()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataGrid gridFG = new DataGrid(DashboardMeasure.FocusGroupBonus, "G", "18");
        }

        [TestMethod]
        public void TotalBonusByQuarter()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DataGrid gridTB = new DataGrid(DashboardMeasure.TotalBonusByQuarter, "M", "5");
        }

        [TestMethod]
        public void VolumeBonus()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");

            DashboardControl<decimal> gridAV = new DashboardControl<decimal>(DashboardMeasure.VolumeBonus, "M", "5");
        }
    }
}
