﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using QubeUtils;

namespace QubeDashboardControls
{
    public class DataPill : DashboardControl<decimal>
    {
        public DataPill(DashboardMeasure measure, string selectionLevel, string selectionId, DataPeriod period = null) : base(measure, selectionLevel, selectionId, period) { }

        protected override bool LastYear => false;
        protected override PeriodSetting PeriodSettings => new PeriodSetting();
        protected override bool AllowValidation => true;

        protected override Dictionary<string, decimal> CalculateData(ControlLevel level = ControlLevel.Default, bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (level == ControlLevel.National ? Model.NationalValues : (level == ControlLevel.Zone ? Model.ZoneValues : (lastYear ? Model.SelectionValuesLY : Model.SelectionValues)));

            Controller con = ControllerFactory.Get(Measure.Enum, Period, data);

            Dictionary<string, decimal> res = (Dictionary<string, decimal>)con.Execute(lastYear);

            return res;
        }

        protected override Dictionary<string, Dictionary<int, decimal>> CalculateDealerData(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> data = (lastYear ? Model.ValuesLY : Model.Values);

            Controller con = ControllerFactory.Get(Measure.Enum, Period, data);

            Dictionary<string, Dictionary<int, decimal>> res =  (Dictionary<string, Dictionary<int, decimal>>)con.ExecuteDealer(lastYear);

            return res;
        }

        protected override Dictionary<string, int> GetQuartiles()
        {
            throw new NotImplementedException();
        }
    }
}
