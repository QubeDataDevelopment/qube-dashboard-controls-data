﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Serialization;
using System.Text;
using System.Threading.Tasks;
using QubeDashboardControls.Objects.XS;
using QubeUtils;

namespace QubeDashboardControls.Models.XS
{
    public class TurnoverByType_FB : DashboardModel<DashboardObject>
    {
        public override Dictionary<string, DashboardObject> Template => new Dictionary<string, DashboardObject>() { { "ROM", new Objects.XS.TurnoverByType_FB() }, { "EXM", new Objects.XS.TurnoverByType_FB() }, { "MEC", new Objects.XS.TurnoverByType_FB() }, { "DAM", new Objects.XS.TurnoverByType_FB() }, { "ACC", new Objects.XS.TurnoverByType_FB() }, { "TOTE", new Objects.XS.TurnoverByType_FB() }, { "WRW", new Objects.XS.TurnoverByType_FB() }, { "TOT", new Objects.XS.TurnoverByType_FB() } };
        public override Dictionary<string, int> Weights => new Dictionary<string, int>() { { "ROM", 1 }, { "EXM", 2 }, { "MEC", 3 }, { "DAM", 4 }, { "ACC", 5 }, { "TOTE", 6 }, { "WRW", 7 }, { "TOT", 8 } };
        public override Dictionary<string, List<string>> Text => new Dictionary<string, List<string>>()
        {
            { "English", new List<string>(){ "Routine Maintenance", "Extended Maintenance", "Mechanical Repair", "Damage", "Accessories", "Total*", "Winter Tyres", "Total" } },
            { "German", new List<string>(){ "Wartung", "Verschleiß", "Mechanik", "Karosserie", "Zubehör", "Gesamt*", "Winterreifen", "Gesamt" } },
            { "French", new List<string>(){ "Maintenance", "Usure", "Mécanique", "Carrosserie", "Accessoires", "Total*", "Pneus hiver", "Total" } },
        };

        public TurnoverByType_FB(Measure measure, DataPeriod period, bool hasData, bool lastYear) : base(measure, period, hasData, lastYear) { }

        protected override Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false)
        {
            DateTime maxPurchaseDate = MSSQLHelper.ExecuteScalar<DateTime>("SELECT dbo.MaxPurchaseDate()", null);
            string sql = "select\n"
                       + "	d.id dealer_id\n"
                       + "	,coalesce(Level4_Code, '') Level4_Code\n"
                       + "	,coalesce(MTD, 0) MTD\n"
                       + "	,coalesce(MTDLY, 0) MTDLY\n"
                       + "	,coalesce(YTD, 0) YTD\n"
                       + "	,coalesce(YTDLY, 0) YTDLY\n"
                       + "from v_LiveDealers d\n"
                       + "left join (\n"
                       + "	SELECT\n"
                       + "		dealer_id\n"
                       + "		,Level4_Code\n"
                       + "		,SUM(case when dataperiod_id between @To and @To then DealerCost end) MTD\n"
                       + "		,SUM(case when dataperiod_id between @To - 12 and @To - 12 then DealerCost end) MTDLY\n"
                       + "		,SUM(case when dataperiod_id between @From and @To then DealerCost end) YTD\n"
                       + "		,SUM(case when dataperiod_id between @From - 12 and @To - 12 then DealerCost end) YTDLY\n"
                       + "	FROM Web_ValidDealerPurchases\n"
                       + "	WHERE (dataperiod_id BETWEEN @From AND @To or dataperiod_id BETWEEN @From - 12 and @To - 12)\n"
                       + "		AND (purchase_date BETWEEN @FromDate and @ToDate or purchase_date BETWEEN dateadd(year, -1, @FromDate) and dateadd(year, -1, @ToDate))\n"
                       + "		and Level4_Code in ('ROM', 'EXM', 'MEC', 'DAM', 'ACC', 'WRW', 'OTH')\n"
                       + "	group by dealer_id, Level4_Code\n"
                       + ") r on r.dealer_id = d.id";

            DataTable data = MSSQLHelper.GetDataTable(sql, new SqlParameter[] {
                new SqlParameter("@From", SqlDbType.Int) { Value = Period.FYearStartPeriod },
                new SqlParameter("@To", SqlDbType.Int) { Value = Period.Id },
                new SqlParameter("@FromDate", SqlDbType.Date) { Value = Period.FYearStartDate },
                new SqlParameter("@ToDate", SqlDbType.Date) { Value = maxPurchaseDate }
            });

            return
            data.AsEnumerable().Select(s => new {
                DealerId = s.Field<int>("dealer_id"),
                Category = s.Field<string>("Level4_Code"),
                MTD = s.Field<decimal>("MTD"),
                MTDLY = s.Field<decimal>("MTDLY"),
                YTD = s.Field<decimal>("YTD"),
                YTDLY = s.Field<decimal>("YTDLY"),
            }).GroupBy(g => g.Category)
            .Where(w => !string.IsNullOrEmpty(w.Key))
            .ToDictionary(
                x => x.Key.Trim(),
                x => x.GroupBy(g => g.DealerId)
                    .ToDictionary(
                        x2 => x2.Key,
                        x2 => new JavaScriptSerializer().Serialize(
                            new Objects.XS.TurnoverByType_FB {
                                MTD = x2.Sum(s => s.MTD),
                                MTDLY = x2.Sum(s => s.MTDLY),
                                YTD = x2.Sum(s => s.YTD),
                                YTDLY = x2.Sum(s => s.YTDLY),
                            })
                        ));
        }
    }
}
