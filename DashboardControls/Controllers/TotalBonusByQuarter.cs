﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using QubeUtils;

namespace QubeDashboardControls.Controllers
{
    public class TotalBonusByQuarter : Controller
    {
        public TotalBonusByQuarter(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override dynamic Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.TotalBonusByQuarter> res = new Dictionary<string, Objects.XS.TotalBonusByQuarter>();

            //Q1
            if (Data.ContainsKey("Q1" + (lastYear ? "LY" : "")))
            {
                List<Objects.XS.TotalBonusByQuarter> resQ1 = Data["Q1" + (lastYear ? "LY" : "")].Select(x => new JavaScriptSerializer().Deserialize<Objects.XS.TotalBonusByQuarter>(x.Value)).DistinctBy(s => s.dealergroup_id).ToList();
                res.Add("Q1", new Objects.XS.TotalBonusByQuarter
                {
                    Name = "Q1",
                    PB_Bonus = resQ1.SumNullable(s => s.PB_Bonus),
                    FG_Bonus = resQ1.SumNullable(s => s.FG_Bonus),
                    AV_Bonus = resQ1.SumNullable(s => s.AV_Bonus),
                    Total = resQ1.SumNullable(s => s.PB_Bonus) + resQ1.SumNullable(s => s.FG_Bonus) + resQ1.SumNullable(s => s.AV_Bonus)
                });
            }

            //Q2
            if (Data.ContainsKey("Q2" + (lastYear ? "LY" : "")))
            {
                List<Objects.XS.TotalBonusByQuarter> resQ2 = Data["Q2" + (lastYear ? "LY" : "")].Select(x => new JavaScriptSerializer().Deserialize<Objects.XS.TotalBonusByQuarter>(x.Value)).DistinctBy(s => s.dealergroup_id).ToList();
                res.Add("Q2", new Objects.XS.TotalBonusByQuarter
                {
                    Name = "Q2",
                    PB_Bonus = resQ2.SumNullable(s => s.PB_Bonus),
                    FG_Bonus = resQ2.SumNullable(s => s.FG_Bonus),
                    AV_Bonus = resQ2.SumNullable(s => s.AV_Bonus),
                    Total = resQ2.SumNullable(s => s.PB_Bonus) + resQ2.SumNullable(s => s.FG_Bonus) + resQ2.SumNullable(s => s.AV_Bonus)
                });
            }

            //Q3
            if (Data.ContainsKey("Q3" + (lastYear ? "LY" : "")))
            {
                List<Objects.XS.TotalBonusByQuarter> resQ3 = Data["Q3" + (lastYear ? "LY" : "")].Select(x => new JavaScriptSerializer().Deserialize<Objects.XS.TotalBonusByQuarter>(x.Value)).DistinctBy(s => s.dealergroup_id).ToList();
                res.Add("Q3", new Objects.XS.TotalBonusByQuarter
                {
                    Name = "Q3",
                    PB_Bonus = resQ3.SumNullable(s => s.PB_Bonus),
                    FG_Bonus = resQ3.SumNullable(s => s.FG_Bonus),
                    AV_Bonus = resQ3.SumNullable(s => s.AV_Bonus),
                    Total = resQ3.SumNullable(s => s.PB_Bonus) + resQ3.SumNullable(s => s.FG_Bonus) + resQ3.SumNullable(s => s.AV_Bonus)
                });
            }

            //Q4
            if (Data.ContainsKey("Q4" + (lastYear ? "LY" : "")))
            {
                List<Objects.XS.TotalBonusByQuarter> resQ4 = Data["Q4" + (lastYear ? "LY" : "")].Select(x => new JavaScriptSerializer().Deserialize<Objects.XS.TotalBonusByQuarter>(x.Value)).DistinctBy(s => s.dealergroup_id).ToList();
                res.Add("Q4", new Objects.XS.TotalBonusByQuarter
                {
                    Name = "Q4",
                    PB_Bonus = resQ4.SumNullable(s => s.PB_Bonus),
                    FG_Bonus = resQ4.SumNullable(s => s.FG_Bonus),
                    AV_Bonus = resQ4.SumNullable(s => s.AV_Bonus),
                    Total = resQ4.SumNullable(s => s.PB_Bonus) + resQ4.SumNullable(s => s.FG_Bonus) + resQ4.SumNullable(s => s.AV_Bonus)
                });
            }

            //if (MSSQLHelper.ExecuteScalar<DateTime>("SELECT dbo.MaxPurchaseDate", null).Year == 2020 && MSSQLHelper.ExecuteScalar<DateTime>("SELECT dbo.MaxPurchaseDate", null).Month < 4)
            //{
                //if (Data.ContainsKey("Q5" + (lastYear ? "LY" : "")))
                //{
                //    List<Objects.XS.TotalBonusByQuarter> resQ5 = Data["Q5" + (lastYear ? "LY" : "")].Select(x => new JavaScriptSerializer().Deserialize<Objects.XS.TotalBonusByQuarter>(x.Value)).DistinctBy(s => s.dealergroup_id).ToList();
                //    res.Add("Q5", new Objects.XS.TotalBonusByQuarter
                //    {
                //        Name = "Q5",
                //        PB_Bonus = resQ5.SumNullable(s => s.PB_Bonus),
                //        FG_Bonus = resQ5.SumNullable(s => s.FG_Bonus),
                //        AV_Bonus = resQ5.SumNullable(s => s.AV_Bonus),
                //        Total = resQ5.SumNullable(s => s.PB_Bonus) + resQ5.SumNullable(s => s.FG_Bonus) + resQ5.SumNullable(s => s.AV_Bonus)
                //    });
                //}
            //}

            

            //YTD
            res.Add("YTD", new Objects.XS.TotalBonusByQuarter
            {
                Name = "YTD",
                PB_Bonus = res.SumNullable(s => s.Value.PB_Bonus),
                FG_Bonus = res.SumNullable(s => s.Value.FG_Bonus),
                AV_Bonus = res.SumNullable(s => s.Value.AV_Bonus),
                Total = res.SumNullable(s => s.Value.Total)
            });

            return res.ToDictionary(x => x.Key, x => (DashboardObject)x.Value);
        }

        public override dynamic ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
