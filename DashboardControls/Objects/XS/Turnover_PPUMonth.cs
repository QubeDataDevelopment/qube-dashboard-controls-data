﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class Turnover_PPUMonth : DashboardObject
    {
        public decimal TO_APR { get; set; }
        public decimal TO_MAY { get; set; }
        public decimal TO_JUN { get; set; }
        public decimal TO_JUL { get; set; }
        public decimal TO_AUG { get; set; }
        public decimal TO_SEP { get; set; }
        public decimal TO_OCT { get; set; }
        public decimal TO_NOV { get; set; }
        public decimal TO_DEC { get; set; }
        public decimal TO_JAN { get; set; }
        public decimal TO_FEB { get; set; }
        public decimal TO_MAR { get; set; }
        public decimal Parc_APR { get; set; }
        public decimal Parc_MAY { get; set; }
        public decimal Parc_JUN { get; set; }
        public decimal Parc_JUL { get; set; }
        public decimal Parc_AUG { get; set; }
        public decimal Parc_SEP { get; set; }
        public decimal Parc_OCT { get; set; }
        public decimal Parc_NOV { get; set; }
        public decimal Parc_DEC { get; set; }
        public decimal Parc_JAN { get; set; }
        public decimal Parc_FEB { get; set; }
        public decimal Parc_MAR { get; set; }

        public decimal APR { get; set; }
        public decimal MAY { get; set; }
        public decimal JUN { get; set; }
        public decimal JUL { get; set; }
        public decimal AUG { get; set; }
        public decimal SEP { get; set; }
        public decimal OCT { get; set; }
        public decimal NOV { get; set; }
        public decimal DEC { get; set; }
        public decimal JAN { get; set; }
        public decimal FEB { get; set; }
        public decimal MAR { get; set; }
    }
}
