﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public interface IController
    {
        DashboardMeasure Measure { get; }
        Dictionary<string, Dictionary<int, string>> Data { get; }
    }
}
