﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.PPM_AT
{
    internal class BonusSummary_Process : DashboardObject
    {
        public int dealergroup_id { get; set; }
        public decimal? Bonus_Q1_PartsBasket { get; set; }
        
        public decimal? Bonus_Q1_AccessoriesBasket { get; set; }
        
        public decimal? Bonus_Q1_LifeCycleBasket { get; set; }
        
        public decimal? Bonus_Q1_WinterTyresBasket { get; set; }
        
        public decimal? Bonus_Q2_PartsBasket { get; set; }
        
        public decimal? Bonus_Q2_AccessoriesBasket { get; set; }
        
        public decimal? Bonus_Q2_LifeCycleBasket { get; set; }
        
        public decimal? Bonus_Q2_WinterTyresBasket { get; set; }
        
        public decimal? Bonus_Q3_PartsBasket { get; set; }
        
        public decimal? Bonus_Q3_AccessoriesBasket { get; set; }
        
        public decimal? Bonus_Q3_LifeCycleBasket { get; set; }
        
        public decimal? Bonus_Q3_WinterTyresBasket { get; set; }
        
        public decimal? Bonus_Q4_PartsBasket { get; set; }
        
        public decimal? Bonus_Q4_AccessoriesBasket { get; set; }
        
        public decimal? Bonus_Q4_LifeCycleBasket { get; set; }
        
        public decimal? Bonus_Q4_WinterTyresBasket { get; set; }
        
    }
}
