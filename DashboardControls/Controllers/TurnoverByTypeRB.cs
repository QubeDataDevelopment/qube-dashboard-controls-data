﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace QubeDashboardControls.Controllers
{
    public class TurnoverByTypeRB : Controller
    {
        public TurnoverByTypeRB(DashboardMeasure measure, Dictionary<string, Dictionary<int, string>> data) : base(measure, data) { }

        public override object Execute(bool lastYear = false)
        {
            Dictionary<string, Objects.XS.TurnoverByType_RB> res = new Dictionary<string, Objects.XS.TurnoverByType_RB>();

            Dictionary<string, Dictionary<int, Objects.XS.TurnoverByType_RB>> resH = new Dictionary<string, Dictionary<int, Objects.XS.TurnoverByType_RB>>();

            resH = Data.ToDictionary(x => x.Key, x => x.Value.ToDictionary(x2 => x2.Key, x2 => new JavaScriptSerializer().Deserialize<Objects.XS.TurnoverByType_RB>(x2.Value)));

            foreach (KeyValuePair<string, Dictionary<int, Objects.XS.TurnoverByType_RB>> g in resH)
                res.Add(g.Key + (lastYear ? "LY" : ""), new Objects.XS.TurnoverByType_RB() { Name = g.Key, YTD = g.Value.Sum(s => s.Value.YTD), YTDLY = g.Value.Sum(s => s.Value.YTDLY), YTDTarget = g.Value.Sum(s => s.Value.YTDTarget) });

            res.Add("TOTE" + (lastYear ? "LY" : ""), new Objects.XS.TurnoverByType_RB()
            {
                YTD = res.Where(w => w.Key != "WRW").Sum(s => s.Value.YTD),
                YTDLY = res.Where(w => w.Key != "WRW").Sum(s => s.Value.YTDLY),
                YTDTarget = res.Where(w => w.Key != "WRW").Sum(s => s.Value.YTDTarget)
            });

            res.Add("TOT" + (lastYear ? "LY" : ""), new Objects.XS.TurnoverByType_RB()
            {
                YTD = res.Where(w => w.Key != "TOTE").Sum(s => s.Value.YTD),
                YTDLY = res.Where(w => w.Key != "TOTE").Sum(s => s.Value.YTDLY),
                YTDTarget = res.Where(w => w.Key != "TOTE").Sum(s => s.Value.YTDTarget)
            });

            foreach (KeyValuePair<string, Objects.XS.TurnoverByType_RB> o in res)
            {
                o.Value.YTDChange = o.Value.YTD - o.Value.YTDLY;
                o.Value.YTDChangePerc = o.Value.YTDChange / (o.Value.YTDLY == 0m ? 1m : o.Value.YTDLY);
                o.Value.YTDTargetChange = o.Value.YTD - o.Value.YTDTarget;
                o.Value.YTDTargetChangePerc = o.Value.YTDTargetChange / (o.Value.YTDTarget == 0m ? 1m : o.Value.YTDTarget);
            }

            return res.ToDictionary(x => x.Key, x => (DashboardObject) x.Value);
        }

        public override object ExecuteDealer(bool lastYear = false)
        {
            throw new NotImplementedException();
        }

        public override Dictionary<string, int> ExecuteQuartiles(Dictionary<string, object> selectionData)
        {
            throw new NotImplementedException();
        }
    }
}
