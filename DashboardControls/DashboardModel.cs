﻿using QubeUtils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls
{
    public abstract class DashboardModel<T> : IDashboardModel<T>
    {
        public Measure Measure { get; set; }
        public DataPeriod Period { get; set; }
        public bool HasData { get; set; }
        public bool LastYear { get; set; }

        public abstract Dictionary<string, T> Template { get; }
        public abstract Dictionary<string, int> Weights { get; }
        public abstract Dictionary<string, List<string>> Text { get; }
        public Dictionary<string, Dictionary<int, string>> Values { get; set; }
        public Dictionary<string, Dictionary<int, string>> ValuesLY { get; set; }
        public Dictionary<string, Dictionary<int, string>> NationalValues { get; set; }
        public Dictionary<string, Dictionary<int, string>> SelectionValues { get; set; }
        public Dictionary<string, Dictionary<int, string>> SelectionValuesLY { get; set; }
        public Dictionary<string, Dictionary<int, string>> ZoneValues { get; set; }

        public DashboardModel(Measure measure, DataPeriod period, bool hasData, bool lastYear) {
            this.Measure = measure;
            this.Period = period;
            this.HasData = hasData;
            this.LastYear = lastYear;

            if (!HasData)
            {
                Values = GetValues();
                if (LastYear) ValuesLY = GetValues(true);
            }
            else
            {
                Values = GetData();
                if (LastYear) ValuesLY = GetData(true);
            }
        }

        // Last year must suffix keys with "LY"
        protected abstract Dictionary<string, Dictionary<int, string>> GetValues(bool lastYear = false);

        private Dictionary<string, Dictionary<int, string>> GetData(bool lastYear = false)
        {
            Dictionary<string, Dictionary<int, string>> res = new Dictionary<string, Dictionary<int, string>>();

            DataTable data = MSSQLHelper.GetDataTable("select Dealer_Id, ScoreKey, Score from DashboardRanking where DashboardMeasureId = @MeasureId and PeriodId = @PeriodId",
                new SqlParameter[]
                {
                    new SqlParameter("@MeasureId", SqlDbType.Int) { Value = Measure.Id },
                    new SqlParameter("@PeriodId", SqlDbType.Int) { Value = Period.Id }
                }
            );

            foreach(IGrouping<string, DataRow> row in data.AsEnumerable().Where(w => (w.Field<string>("ScoreKey").Right(2) == "LY") == lastYear).GroupBy(g => g.Field<string>("ScoreKey")))
            {
                res.Add(row.Key, row.ToDictionary(d => d.Field<int>("Dealer_Id"), d => d.Field<string>("Score")));
            }

            return res;
        }
    }
}
