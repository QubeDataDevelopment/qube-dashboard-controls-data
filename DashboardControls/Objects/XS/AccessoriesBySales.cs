﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QubeDashboardControls.Objects.XS
{
    public class AccessoriesBySales : DashboardObject
    {
        public decimal Acc { get; set; }
        public int Veh { get; set; }
        public decimal YTD { get; set; }
    }
}
